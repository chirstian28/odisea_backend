# -*- coding: utf-8 -*-
from rest_framework import serializers
from backend.components.tasks.models import HistoricTask

class HistoricTaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = HistoricTask
        fields = ("id", "licenser", "taskid", "creation_date_time")
