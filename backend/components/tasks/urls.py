from backend.components.tasks import views
from rest_framework import routers
from django.urls import path

router = routers.DefaultRouter()

router.register(r'historic', views.HistoricTaskViewSet)

urlpatterns = router.urls
