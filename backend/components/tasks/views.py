# -*- coding: utf-8 -*-

from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets
from backend.components.tasks.models import HistoricTask
from backend.components.tasks.serializers import HistoricTaskSerializer

class HistoricTaskViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = HistoricTask.objects.all()
    serializer_class = HistoricTaskSerializer
    permission_classes = (IsAuthenticated,)

