# -*- coding: utf-8 -*-
from django.db import models
import datetime
        
class TaskClass(models.Model):

    def getTaskID(self):
        date = datetime.datetime.now().timestamp()
        taskid = self.__class__.__name__.ljust(30)
        taskid += str(date).zfill(15)
        taskid += str(self.id).zfill(20)
        return taskid

    class Meta:
        abstract = True
        app_label = 'tasks'

class HistoricTask(models.Model):
    licenser = models.ForeignKey('license.Licenser', on_delete=models.PROTECT)
    taskid = models.CharField(max_length=70)
    creation_date_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['creation_date_time']
        app_label = 'tasks'
    
