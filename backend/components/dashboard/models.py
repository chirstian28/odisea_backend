from backend.core.models import ResourceHost
from django.db import models
from backend.auth.accounts.models import UserDate
from django.conf import settings

class Dashboard(models.Model):
    
    def getDashboard(account):
        
        dashboard_dict = {}
        dashboard_dict['title'] = f'Bienvenido {account} a {settings.COMERCIAL_APP_NAME}'
        dashboard_dict['description'] = 'El estado actual de la operación es el siguiente.'
        
        panel_dict_list = []
        panel_list = Panel.objects.filter(accounttype_list=account.type)
        
        for panel in panel_list:
            panel_dict = panel.getDict(account)
            if panel_dict is not None:
                panel_dict_list.append(panel_dict)
        dashboard_dict['PANELS'] = panel_dict_list
        return dashboard_dict
    
    getDashboard = staticmethod(getDashboard)
   
    class Meta:
        abstract = True
        app_label = 'dashboard'
        verbose_name = 'Dashboard'
     
class PanelType(models.Model):
    id = models.CharField(max_length=10, primary_key=True)
    name = models.CharField(max_length=60)

    def __str__(self):
        return "%s" % (self.name)

    class Meta:
        ordering = ['id']
        app_label = 'dashboard'

class Panel(UserDate):
    id = models.IntegerField(verbose_name='Código', primary_key=True)
    name = models.CharField(max_length=50, verbose_name='Título')
    url = models.CharField(max_length=100, null=True, blank=True, verbose_name='URL')
    arrayid = models.IntegerField(verbose_name='ID Arreglo', null=True, blank=True)
    type = models.ForeignKey('PanelType', verbose_name='Tipo de Opción', on_delete=models.PROTECT)
    description = models.CharField(max_length=100, verbose_name='Descripción', null=True, blank=True)
    accounttype_list = models.ManyToManyField('accounts.AccountType', verbose_name='Tipos de Cuenta')

    def __str__(self):
        return "%s" % (self.title)
        
    def getDict(self, account):
        panel_dict = None
        
        if account is not None and self.accounttype_list.filter(pk=account.type.id).count() > 0:
            panel_dict = {
                        "key": f"PANEL_{self.id}",
                        "name": self.name,
                        "type": self.type.id,
                    }

        if panel_dict is not None: 
            if self.url is not None:
                panel_dict['url'] = ResourceHost.getBase() + '/' + self.url
            if self.description is not None:
                panel_dict['description'] = self.description

        return panel_dict
    
    class Meta:
        ordering = ['id']
        app_label = 'dashboard'
        verbose_name = 'Panel'
        verbose_name_plural = 'Paneles'
       
