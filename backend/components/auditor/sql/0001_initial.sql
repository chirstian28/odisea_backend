INSERT INTO auditor_auditorlevel (id, name) VALUES (0, 'LOGIN');
INSERT INTO auditor_auditorlevel (id, name) VALUES (1, 'LOGOUT');
INSERT INTO auditor_auditorlevel (id, name) VALUES (2, 'ACCESO');
INSERT INTO auditor_auditorlevel (id, name) VALUES (3, 'CREACION');
INSERT INTO auditor_auditorlevel (id, name) VALUES (4, 'ACTUALIZACION');
INSERT INTO auditor_auditorlevel (id, name) VALUES (5, 'ELIMINACION');

INSERT INTO auditor_auditorparameter (environment, monitor_level_id, mail_monitor_level_id, notification_mail) VALUES ('DEVELOPMENT', 2, 5, 'soporte@er-technology.net');
INSERT INTO auditor_auditorparameter (environment, monitor_level_id, mail_monitor_level_id, notification_mail) VALUES ('PRODUCTION', 3, 4, 'soporte@er-technology.net');
