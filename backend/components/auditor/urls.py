# -*- coding: utf-8 -*-
from backend.components.auditor import views
from rest_framework import routers

router = routers.DefaultRouter()

router.register('auditorevent', views.AuditorEventViewSet, basename='auditorevent')
router.register('auditorparameter', views.AuditorParameterViewSet, basename='auditorparameter')
router.register('auditorlevel', views.AuditorLevelViewSet, basename='auditorlevel')

urlpatterns = router.urls
