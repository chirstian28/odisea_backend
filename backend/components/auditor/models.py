from django.db import models
import datetime
from backend.library.sendmail import send_mail
from backend.core.models import Environment

class AuditorLevel(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)

    def __str__(self):
        return u"%d-%s" % (self.id, self.name)

    class Meta:
        app_label = 'auditor'

class AuditorParameter(models.Model):
    LOGIN = 0
    LOGOUT = 1
    QUERY = 2
    CREATE = 3
    UPDATE = 4
    DELETE = 5
    environment = models.CharField(verbose_name='Entorno', max_length=30, unique=True)
    licenser = models.ForeignKey('license.Licenser', null=True, blank=True, on_delete=models.SET_NULL, verbose_name='Licenciador')
    monitor_level = models.ForeignKey('AuditorLevel', on_delete=models.PROTECT, verbose_name='Nivel a Monitorear')
    mail_monitor_level = models.ForeignKey('AuditorLevel', related_name='mail_monitor_level', null=True, blank=True, on_delete=models.SET_NULL, verbose_name='Nivel para Enviar a Email')
    notification_mail = models.CharField(max_length=100, null=True, blank=True, verbose_name='Email a Notificar')
    
    def __str__(self):
        return u"%s" % (self.environment)

    def sendEmailEvent(self, event):
        email_dict = {}
        email_dict['SUBJECT'] = ''
        email_dict['SUBJECT'] += Environment.getEnvironmentName()
        email_dict['SUBJECT'] += ' - '
        email_dict['SUBJECT'] += 'Notificación de Evento - '
        email_dict['SUBJECT'] += event.level.name
        email_dict['TO_EMAIL'] = [self.notification_mail]

        email_dict['VERBOSE_TITLE'] = 'Notificación de Evento'
        email_dict['DATA'] = {}
        email_dict['DATA']['BODY'] = ''

        email_dict['DATA']['BODY'] += '<b>'
        email_dict['DATA']['BODY'] += event.level.name
        email_dict['DATA']['BODY'] += '</b> <br/>'
        email_dict['DATA']['BODY'] += '%s' % (event.date_time.strftime('%Y-%m-%d %H:%M'))
        email_dict['DATA']['BODY'] += '<br/>'
        email_dict['DATA']['BODY'] += 'Usuario: %s Host: %s, Action: %s' % (event.username, event.remote_address, event.action)
        email_dict['DATA']['BODY'] += '<br/>'
        email_dict['DATA']['BODY'] += 'URL: %s' % (event.urlpath)
        email_dict['DATA']['BODY'] += '<br/>'
        email_dict['DATA']['BODY'] += 'Módulo: %s, Modelo: %s, ID Registro: %s' % (event.module, event.model_name, event.objectid)
        email_dict['DATA']['BODY'] += '<br/>'
        try:
            send_mail(email_dict)
        except:
            pass

    def registerEvent(request, module, model_name, register_object, object_id, event_level, before_register_object=None, action=None ):
        from backend.auth.accounts.models import Account
        try:
            account = Account.getAccount(request.user)
        except Account.DoesNotExist:
            return False
        if account is None:
            return False

        auditor_list = AuditorParameter.objects.filter(environment=Environment.getEnvironmentName()) 
        if auditor_list.count() == 0:
            return False
        elif auditor_list.count() == 1:
            auditor = auditor_list[0]
            if auditor.licenser is not None and auditor.licenser.id != account.licenser.id:
                return False
        else:
            try:
                auditor = auditor_list.get(licenser=account.licenser)
            except AuditorParameter.DoesNotExist:
                try:
                    auditor = auditor_list.get(licenser__isnull=True)
                except AuditorParameter.DoesNotExist:
                    return False

        if auditor.monitor_level.id > event_level:
            return False
        try:
            auditor_level = AuditorLevel.objects.get(pk=event_level)
        except AuditorLevel.DoesNotExist:
            return False  
        
        date_time = datetime.datetime.now()
        
        event = AuditorEvent()
        event.environment = Environment.getEnvironmentName()
        event.username = account.username
        event.licenser = account.licenser
        event.date = date_time.date()
        event.time = date_time.time()
        event.date_time = date_time
        event.remote_address = request.META['REMOTE_ADDR']
        event.module = module.upper()
        event.model_name = model_name.upper()
        if before_register_object is not None:
            event.before_register_object = before_register_object
        if register_object is not None:
            event.actual_register_object = register_object
        event.objectid = object_id
        event.level = auditor_level
        event.urlpath = request.get_full_path()
        event.action = action
        event.save()

        if auditor.mail_monitor_level.id <= event_level:
            auditor.sendEmailEvent(event)

        return True
    
    registerEvent = staticmethod(registerEvent)
        
    class Meta:
        app_label = 'auditor'

class AuditorEvent(models.Model):
    environment = models.CharField(verbose_name='Entorno', max_length=30)
    licenser = models.ForeignKey('license.Licenser', null=True, blank=True, on_delete=models.SET_NULL, verbose_name='Licenciador')
    username = models.CharField(max_length=100, null=True, blank=True, verbose_name='Usuario')
    date = models.DateField(verbose_name='Fecha')
    time = models.TimeField(verbose_name='Hora')
    date_time = models.DateTimeField(verbose_name='Fecha y Hora')
    remote_address = models.CharField(max_length=100, null=True, blank=True, verbose_name='Host')
    module = models.CharField(max_length=100, verbose_name='Modulo')
    model_name = models.CharField(max_length=100, verbose_name='Modelo')
    before_register_object = models.TextField(null=True, blank=True, verbose_name='Objeto Anterior')
    actual_register_object = models.TextField(null=True, blank=True, verbose_name='Objeto Actual')
    objectid = models.CharField(max_length=100,null=True, blank=True, verbose_name='ID Objeto')
    level = models.ForeignKey('AuditorLevel', on_delete=models.PROTECT, verbose_name='Nivel')
    urlpath = models.TextField(null=True, blank=True, verbose_name='URL')
    action = models.CharField(max_length=100, verbose_name='Acción')
    
    def __str__(self):
        return u"%s-%s-%s" % (self.environment, self.username, self.datetime.strftime('%Y-%m-%d'))

    class Meta:
        app_label = 'auditor'
        ordering = ['environment', '-date_time']


