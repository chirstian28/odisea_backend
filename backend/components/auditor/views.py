# -*- coding: utf-8 -*-
from rest_framework import viewsets
from backend.components.auditor.models import AuditorEvent, AuditorLevel, \
                                              AuditorParameter
from backend.components.auditor.serializers import AuditorEventSerializer, \
                                                   AuditorParameterSerializer, \
                                                   AuditorLevelSerializer
from backend.core.permissions import IsAdminOrReadOnly, IsAdminAndReadOnly
from backend.core.views import GenericModelViewSet

class AuditorParameterViewSet(GenericModelViewSet):
    app_name = 'auditor'
    model_name = 'AuditorParameter'
    queryset = AuditorParameter.objects.all()
    serializer_class = AuditorParameterSerializer
    permission_classes = (IsAdminOrReadOnly,)
    filter_licenser = False
    
class AuditorEventViewSet(viewsets.ReadOnlyModelViewSet):
    app_name = 'auditor'
    model_name = 'AuditorEvent'
    queryset = AuditorEvent.objects.all()
    serializer_class = AuditorEventSerializer
    permission_classes = (IsAdminAndReadOnly, )

class AuditorLevelViewSet(GenericModelViewSet):
    app_name = 'auditor'
    model_name = 'AuditorLevel'
    queryset = AuditorLevel.objects.all()
    serializer_class = AuditorLevelSerializer
    permission_classes = (IsAdminOrReadOnly,)
