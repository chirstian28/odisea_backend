from rest_framework import serializers
from backend.components.auditor.models import AuditorLevel, AuditorEvent, \
                                              AuditorParameter

class AuditorLevelSerializer(serializers.ModelSerializer):

    class Meta:
        model = AuditorLevel
        fields = ("id", "name")

class AuditorParameterSerializer(serializers.ModelSerializer):

    class Meta:
        model = AuditorParameter
        fields = ("id", "environment", "monitor_level", "mail_monitor_level", "notification_mail")

class AuditorEventSerializer(serializers.ModelSerializer):

    class Meta:
        model = AuditorEvent
        fields = ("id", "environment", "username", "date", "time", "date_time", "remote_address", 
                  "module", "model_name", "before_register_object", "actual_register_object", 
                  "objectid", "level", "urlpath", "action",
                )


