INSERT INTO pushmessages_messagetype ( id, name ) VALUES ( 'NOT', 'Notificación');
INSERT INTO pushmessages_messagetype ( id, name ) VALUES ( 'UPD', 'Actualización');
INSERT INTO pushmessages_messagetype ( id, name ) VALUES ( 'INS', 'Inserción');
INSERT INTO pushmessages_messagetype ( id, name ) VALUES ( 'DEL', 'Eliminación');

INSERT INTO pushmessages_messageclass ( id, name ) VALUES ( 'GRAPH', 'Gráfica');
INSERT INTO pushmessages_messageclass ( id, name ) VALUES ( 'TABLE', 'Tabla');
INSERT INTO pushmessages_messageclass ( id, name ) VALUES ( 'LOG', 'Texto Log');
INSERT INTO pushmessages_messageclass ( id, name ) VALUES ( 'FORM', 'Formulario');
