# -*- coding: utf-8 -*-
from django.conf import settings
from backend.core.metadata import GenericMetadata

class RegisterInterfaceMetadata(GenericMetadata):

    def determine_metadata(self, request, view):
        meta_data_dict = GenericMetadata.determine_metadata(self, request, view)
        meta_data_dict['savebutton'] = 'Finalizar'
        meta_data_dict['exitbutton'] = None
        meta_data_dict['type'] = 'FORM'
        meta_data_dict['disablesaveconfirm'] = True
        try:
            meta_data_dict['title'] = view.title
        except:
            pass
        return meta_data_dict
    
class SendNewsMessageMetadata(GenericMetadata):

    def determine_metadata(self, request, view):
        meta_data_dict = GenericMetadata.determine_metadata(self, request, view)
        meta_data_dict['savebutton'] = ' Enviar'
        meta_data_dict['exitbutton'] = None
        meta_data_dict['type'] = 'FORM'
        meta_data_dict['disablesaveconfirm'] = True
        meta_data_dict['backurl'] = 'DASHBOARD'
        try:
            meta_data_dict['title'] = view.title
        except:
            pass
        return meta_data_dict
    
    
