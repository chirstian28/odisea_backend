# -*- coding: utf-8 -*-
from django.db import models
from backend.library.utils import FCMsendData, JsonEncode
import json
import datetime

class PushInterfaceType(models.Model):
    name = models.CharField(max_length=40, verbose_name='Nombre', unique=True)

    def __str__(self):
        return self.name

    class Meta:
        app_label = 'pushmessages'
        verbose_name = 'Tipo de Interfaz'
        verbose_name_plural = 'Tipos de Interfaces'

class PushInterface(models.Model):
    interface_type = models.ForeignKey('PushInterfaceType', verbose_name='Tipo de Interfaz', on_delete=models.PROTECT)
    sessionid = models.TextField(verbose_name="ID de Sesión", db_index=True, default='')
    interfaceid = models.TextField(verbose_name="ID de Interfaz", db_index=True, default='', unique=True)
    active = models.BooleanField(default=True)
     
    def __str__(self):
        return self.interfaceid
    
    def sendMessage(self, message_type_id, message_class_id, data):
        return Message.createMessage(self, message_type_id, message_class_id, data)

    class Meta:
        unique_together = [['interface_type', 'sessionid']]
        app_label = 'pushmessages'
        verbose_name = 'Interfaz'
        verbose_name_plural = 'Interfaces'

class MessageType(models.Model):
    id = models.CharField(max_length=20, primary_key=True, verbose_name='ID')
    name = models.CharField(max_length=40, verbose_name='Nombre')

    def __str__(self):
        return self.name

    class Meta:
        app_label = 'pushmessages'
        verbose_name = 'Tipo de Mensaje'
        verbose_name_plural = 'Tipos de Mensajes'

class MessageClass(models.Model):
    id = models.CharField(max_length=20, primary_key=True, verbose_name='ID')
    name = models.CharField(max_length=40, verbose_name='Nombre')

    def __str__(self):
        return self.nam

    class Meta:
        app_label = 'pushmessages'
        verbose_name = 'Clase de Mensaje'
        verbose_name_plural = 'Clases de Mensajes'

class Message(models.Model):
    sessionid = models.TextField(verbose_name="ID de Sesión", db_index=True, default='')
    date_time = models.DateTimeField(verbose_name='Fecha y Hora',auto_now_add=True)
    message_type = models.ForeignKey('MessageType', verbose_name='Tipo de Mensaje', on_delete=models.PROTECT)
    message_class = models.ForeignKey('MessageClass', verbose_name='Clase de Mensaje', on_delete=models.PROTECT)
    interface_type = models.ForeignKey('PushInterfaceType', verbose_name='Tipo de Interfaz', on_delete=models.PROTECT)
    push_interface = models.ForeignKey('PushInterface', verbose_name='Interfaz', on_delete=models.PROTECT)
    data = models.JSONField(verbose_name="Dato")
    sent = models.BooleanField(default=False, verbose_name='Enviado')

    def createMessageMassive(interface_type_name, message_type_id, message_class_id, data):
        response_list = []
        try:
            interface_type = PushInterfaceType.objects.get(name=interface_type_name)
        except PushInterfaceType.DoesNotExist:
            return None
        push_interface_list = PushInterface.objects.filter(interface_type=interface_type, active=True)
        for push_interface in push_interface_list:
            response = Message.createMessage(push_interface, message_type_id, message_class_id, data)
            response_list.append(response)
            
        return response_list

    createMessageMassive = staticmethod(createMessageMassive)
    
    def createMessage(push_interface, message_type_id, message_class_id, data):
        response = None
        try:
            message_type = MessageType.objects.get(pk=message_type_id)
        except MessageType.DoesNotExist:
            return None
            
        try:
            message_class = MessageClass.objects.get(pk=message_class_id)
        except MessageClass.DoesNotExist:
            return None
            
        if push_interface.active:
            
            data_dict = {
                            "message_type": message_type_id,
                            "message_class": message_class_id, 
                            "body": data,
                        } 
            
            message = Message()
            message.date_time = datetime.datetime.now()
            message.sessionid = push_interface.sessionid
            message.message_type = message_type
            message.message_class = message_class
            message.interface_type = push_interface.interface_type
            message.push_interface = push_interface
            message.data = data_dict
            message.sent = False
            message.save()
            
            response = message.sendMessage()

        return response

    createMessage = staticmethod(createMessage)
    
    def sendMessage(self):
        data_dict = { 
                      "to": self.sessionid, 
                      "data": self.data
                }
        data_str = json.dumps(data_dict, cls=JsonEncode)
        response = FCMsendData(data_str)
        
        if response == 200:
            self.sent = True
            self.save()
        
        print (data_dict)
#             self.delete()
        
        return response
    
    def sendTestMessage():
        from backend.auth.accounts.models import Account
        account = Account.objects.get(username='info@er-technology.net')
        interface_type_name = 'SHIFT_TURN'
        message_type_id = 'INS'
        message_class_id = 'TABLE'
        data = ['A03', 1, '16288417']
        Message.createMessage(interface_type_name, message_type_id, message_class_id, data)

    sendTestMessage = staticmethod(sendTestMessage)
    
    class Meta:
        app_label = 'pushmessages'
        verbose_name = 'Mensaje'
        verbose_name_plural = 'Mensajes'
        
class Notification(models.Model):
    account = models.ForeignKey('accounts.Account', verbose_name='Cuenta', on_delete=models.PROTECT)
    date_time = models.DateTimeField(verbose_name='Fecha y Hora',auto_now_add=True)
    source = models.TextField(verbose_name="Fuente")
    data = models.JSONField(verbose_name="Dato")
    sent = models.BooleanField(default=False, verbose_name='Enviado')
     
    def __str__(self):
        return self.data

    def createMessage(account, source, title, message, icon=None, action=None):
        data_dict = {
                        "message_type": 'NOT',
                        "title": title, 
                        "body": message,
                    } 
        if icon is not None:
            data_dict['icon'] = icon
        
        if action is not None:
            data_dict['action'] = action
        
        notification = Notification()
        notification.date_time = datetime.datetime.now()
        notification.account = account
        notification.source = source
        notification.data = data_dict
        notification.sent = False
        notification.save()
        
        response = notification.sendMessage()

        return response

    createMessage = staticmethod(createMessage)
    
    def sendTestMessage():
        from backend.auth.accounts.models import Account
        account = Account.objects.get(username='rmrodriguez@superfinanciera.gov.co')
        Notification.createMessage(account, source='TEST', title='Notificación de Prueba', message='Esta es una notificación de pruebas Olver')
    sendTestMessage = staticmethod(sendTestMessage)
    
    def sendMessage(self):
        from backend.auth.accounts.models import AccountToken
        try:
            account_token = AccountToken.objects.get(account=self.account, active=True, sessionid__isnull=False)
        except AccountToken.DoesNotExist:
            return None
        
        data_dict = { 
                      "to": account_token.sessionid, 
                      "data": self.data
                }
        data_str = json.dumps(data_dict, cls=JsonEncode)
        response = FCMsendData(data_str)
        
        print (data_str)
        
        if response == 200:
            self.sent = True
            self.save()
            
            self.delete()
        
        return response
    
    def sendPendingMessage(account=None):
        notification_list = Notification.objects.filter(sent=False)
        if account is not None:
            notification_list = notification_list.filter(account=account)
        for notification in notification_list:
            notification.sendMessage()

    sendPendingMessage = staticmethod(sendPendingMessage)
    
    class Meta:
        app_label = 'pushmessages'
        verbose_name = 'Notificación'
        verbose_name_plural = 'Notificaciones'
        
class NewsMessage(models.Model):
    source_account = models.ForeignKey('accounts.Account', verbose_name='Cuenta', related_name='source_account', on_delete=models.PROTECT)
    date_time = models.DateTimeField(verbose_name='Fecha y Hora',auto_now_add=True)
    account_type = models.ForeignKey('accounts.AccountType', verbose_name='Tipo de Cuenta', null=True, blank=True, on_delete=models.SET_NULL)
    account_list = models.ManyToManyField('accounts.Account', verbose_name='Cuentas Destino', help_text='large') 
    title = models.CharField(verbose_name="Título",max_length=100, help_text='large')
    message = models.TextField(verbose_name="Mensaje", help_text='xlarge')
    data = models.JSONField(verbose_name="Dato")
    sent = models.BooleanField(default=False, verbose_name='Enviado')
     
    def __str__(self):
        return self.data

    def createMessage(source_account, title, message, account_type=None, account_list=None):
        data_dict = {
                        "message_type": 'NOT',
                        "title": title, 
                        "body": message,
                    } 
        
        news_message = NewsMessage()
        news_message.date_time = datetime.datetime.now()
        news_message.source_account = source_account
        news_message.title = title
        news_message.message = message
        news_message.account_type = account_type
        news_message.data = data_dict
        news_message.sent = False
        news_message.save()
        
        if account_list is not None:
            news_message.account_list = account_list
        
        response = news_message.sendMessage()

        return response

    createMessage = staticmethod(createMessage)
    
    def sendTestMessage():
        from backend.auth.accounts.models import Account, AccountType
        source_account = Account.objects.get(username='info@er-technology.net')
        account_type = AccountType.objects.get(pk='R')
        NewsMessage.createMessage(source_account, title='Noticia Nueva', message='Esta es una Noticia', account_type=account_type)
    
    sendTestMessage = staticmethod(sendTestMessage)
    
    def sendMessage(self):
        from backend.auth.accounts.models import Account, AccountToken
        account_list = None
        general_response = 404
        response_list = []
        if self.account_list is not None and self.account_list.count() > 0:
            account_list = self.account_list.all()
        if account_list is None:
            account_list = Account.objects.filter(is_active=True)
        if self.account_type is not None:
            account_list = account_list.filter(type=self.account_type)
        account_list = account_list.exclude(pk=self.source_account.id)
        account_token_list = AccountToken.objects.filter(account__in=account_list, active=True, sessionid__isnull=False)
        for account_token in account_token_list:
            data_dict = { 
                          "to": account_token.sessionid, 
                          "data": self.data
                    }
            data_str = json.dumps(data_dict, cls=JsonEncode)
            response = FCMsendData(data_str)
            response_list.append(response)
        
            print (data_str)
        
        with_error = False
        i = 0
        while not with_error and i < len(response_list): 
            general_response = response_list[i]
            if response_list[i] != 200:
                with_error = True
            i += 1
        
        if not with_error:
            self.sent = True
            self.save()
            
            self.delete()
            
        return general_response
    
    def sendPendingMessage(account=None):
        notification_list = NewsMessage.objects.filter(sent=False)
        if account is not None:
            notification_list = notification_list.filter(account=account)
        for notification in notification_list:
            notification.sendMessage()

    sendPendingMessage = staticmethod(sendPendingMessage)
    
    class Meta:
        app_label = 'pushmessages'
        verbose_name = 'Noticia'
        verbose_name_plural = 'Noticias'
        
