from backend.components.pushmessages import views
from rest_framework import routers
from django.urls import path

router = routers.DefaultRouter()

router.register(r'pushinterfacetype', views.PushInterfaceTypeViewSet, basename='pushinterfacetype')
router.register(r'pushinterfacetypetable', views.PushInterfaceTypeTableViewSet, basename='pushinterfacetypetable')
router.register(r'pushinterfacetypequery', views.PushInterfaceTypeQueryViewSet, basename='pushinterfacetypequery')
router.register(r'pushinterface', views.PushInterfaceViewSet, basename='pushinterface')
router.register(r'pushinterfacetable', views.PushInterfaceTableViewSet, basename='pushinterfacetable')
router.register(r'pushinterfacequery', views.PushInterfaceQueryViewSet, basename='pushinterfacequery')
router.register(r'notification', views.NotificationViewSet, basename='notification')
router.register(r'notificationtable', views.NotificationTableViewSet, basename='notificationtable')
router.register(r'notificationquery', views.NotificationQueryViewSet, basename='notificationquery')
router.register(r'message', views.MessageViewSet, basename='message')
router.register(r'messagetable', views.MessageTableViewSet, basename='messagetable')
router.register(r'messagequery', views.MessageQueryViewSet, basename='messagequery')

urlpatterns = [
                path('register/', views.RegisterInterfaceViewSet.as_view()),
                path('unregister/', views.UnregisterInterfaceViewSet.as_view()),
                path('sendnewsmessage/', views.SendNewsMessageViewSet.as_view()),
]

urlpatterns += router.urls


