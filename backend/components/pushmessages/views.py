# -*- coding: utf-8 -*-
from backend.core.permissions import IsAdminOrReadOnly
from backend.core.views import GenericTableViewSet, GenericModelViewSet
from backend.components.pushmessages.models import PushInterfaceType,\
    PushInterface, Notification, Message, NewsMessage
from backend.components.pushmessages.serializers import PushInterfaceTypeSerializer,\
    PushInterfaceTypeTableSerializer, PushInterfaceTypeQuerySerializer,\
    PushInterfaceSerializer, PushInterfaceTableSerializer,\
    PushInterfaceQuerySerializer, NotificationSerializer,\
    NotificationTableSerializer, NotificationQuerySerializer, MessageSerializer,\
    MessageTableSerializer, MessageQuerySerializer, RegisterInterfaceSerializer,\
    NewsMessageSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from backend.auth.accounts.models import Account, AccountType
from backend.core.metadata import GenericMetadata
from backend.components.pushmessages.metadata import SendNewsMessageMetadata

class PushInterfaceTypeViewSet(GenericModelViewSet):
    app_name = 'pushmessages'
    model_name = 'PushInterfaceType'
    queryset = PushInterfaceType.objects.all()
    serializer_class = PushInterfaceTypeSerializer
    permission_classes = (IsAdminOrReadOnly,)
    filter_licenser = False

class PushInterfaceTypeTableViewSet(GenericTableViewSet):
    app_name = 'pushmessages'
    model_name = 'PushInterfaceType'
    queryset = PushInterfaceType.objects.all()
    serializer_class = PushInterfaceTypeTableSerializer
    permission_classes = (IsAdminOrReadOnly,)
    filter_licenser = False

class PushInterfaceTypeQueryViewSet(GenericTableViewSet):
    app_name = 'pushmessages'
    model_name = 'PushInterfaceType'
    queryset = PushInterfaceType.objects.all()
    serializer_class = PushInterfaceTypeQuerySerializer
    permission_classes = (IsAdminOrReadOnly,)
    filter_licenser = False

class PushInterfaceViewSet(GenericModelViewSet):
    app_name = 'pushmessages'
    model_name = 'PushInterface'
    queryset = PushInterface.objects.all()
    serializer_class = PushInterfaceSerializer
    permission_classes = (IsAdminOrReadOnly,)
    filter_licenser = False

class PushInterfaceTableViewSet(GenericTableViewSet):
    app_name = 'pushmessages'
    model_name = 'PushInterface'
    queryset = PushInterface.objects.all()
    serializer_class = PushInterfaceTableSerializer
    permission_classes = (IsAdminOrReadOnly,)
    filter_licenser = False

class PushInterfaceQueryViewSet(GenericTableViewSet):
    app_name = 'pushmessages'
    model_name = 'PushInterface'
    queryset = PushInterface.objects.all()
    serializer_class = PushInterfaceQuerySerializer
    permission_classes = (IsAdminOrReadOnly,)
    filter_licenser = False

class NotificationViewSet(GenericModelViewSet):
    app_name = 'pushmessages'
    model_name = 'Notification'
    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer
    permission_classes = (IsAdminOrReadOnly,)
    filter_licenser = False

class NotificationTableViewSet(GenericTableViewSet):
    app_name = 'pushmessages'
    model_name = 'Notification'
    queryset = Notification.objects.all()
    serializer_class = NotificationTableSerializer
    permission_classes = (IsAdminOrReadOnly,)
    filter_licenser = False

class NotificationQueryViewSet(GenericTableViewSet):
    app_name = 'pushmessages'
    model_name = 'Notification'
    queryset = Notification.objects.all()
    serializer_class = NotificationQuerySerializer
    permission_classes = (IsAdminOrReadOnly,)
    filter_licenser = False

class MessageViewSet(GenericModelViewSet):
    app_name = 'pushmessages'
    model_name = 'Message'
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
    permission_classes = (IsAdminOrReadOnly,)
    filter_licenser = False

class MessageTableViewSet(GenericTableViewSet):
    app_name = 'pushmessages'
    model_name = 'Message'
    queryset = Message.objects.all()
    serializer_class = MessageTableSerializer
    permission_classes = (IsAdminOrReadOnly,)
    filter_licenser = False

class MessageQueryViewSet(GenericTableViewSet):
    app_name = 'pushmessages'
    model_name = 'Message'
    queryset = Message.objects.all()
    serializer_class = MessageQuerySerializer
    permission_classes = (IsAdminOrReadOnly,)
    filter_licenser = False

class RegisterInterfaceViewSet(APIView):
    serializer_class = RegisterInterfaceSerializer

    def post(self, request):
        validate_response = '1'
        data = {}
        actual_account = Account.getAccount(request.user)
        if actual_account is None:
            data['ERROR'] = 'Debe ser un usuario registrado'
        else:
            licenser = actual_account.licenser
        interface_type = None
        sessionid = None
        interfaceid = None
        data = {}
        
        if 'sessionid' in request.data.keys() and request.data['sessionid']:
            sessionid = request.data['sessionid']
        else:
            validate_response = '0'
            error_msg = 'Debe ingresar el id de la sesión'

        if 'interfaceid' in request.data.keys() and request.data['interfaceid']:
            interfaceid = request.data['interfaceid']
        else:
            validate_response = '0'
            error_msg = 'Debe ingresar el id de la interfaz'
        
        if 'interface_type' in request.data.keys() and request.data['interface_type']:
            interface_type_name = request.data['interface_type']
            try:
                interface_type = PushInterfaceType.objects.get(licenser=licenser, name=interface_type_name)
            except PushInterfaceType.DoesNotExist:
                validate_response = '0'
                error_msg = 'No Existe un tipo de Interfaz con el nombre enviado.'
        else:
            validate_response = '0'
            error_msg = 'Debe ingresar el nombre del tipo de interfaz.'

        if validate_response == '1':
            try:
                interface = PushInterface.objects.get(licenser=licenser, interface_type=interface_type, sessionid=sessionid)
            except PushInterface.DoesNotExist:
                interface = PushInterface()
                interface.licenser = licenser
                interface.interface_type = interface_type
                interface.sessionid = sessionid
            interface.interfaceid = interfaceid
            interface.active = True
            interface.save()

            data['id'] = interface.id
            data['interface_type'] = interface.interface_type.name
            data['sessionid'] = interface.sessionid
            data['interfaceid'] = interface.interfaceid
            data['SUCCESS'] = "Interfaz registrada satisfactoreamente"

        if validate_response == '1':
            return Response(data=data)
        return Response(data={'VAL': validate_response, 'ERROR': error_msg}, status=status.HTTP_400_BAD_REQUEST)

class UnregisterInterfaceViewSet(APIView):
    serializer_class = RegisterInterfaceSerializer

    def post(self, request):
        validate_response = '1'
        data = {}
        actual_account = Account.getAccount(request.user)
        if actual_account is None:
            data['ERROR'] = 'Debe ser un usuario registrado'
        else:
            licenser = actual_account.licenser
        interfaceid = None
        error_msg = ''
        data = {}
        
        if 'interfaceid' in request.data.keys() and request.data['interfaceid']:
            interfaceid = request.data['interfaceid']
        
        if validate_response == '1':
            if interfaceid is not None:
                try:
                    interface = PushInterface.objects.get(licenser=licenser, interfaceid=interfaceid)
                    interface.active = False
                    interface.save()
                except PushInterface.DoesNotExist:
                    pass

            data['SUCCESS'] = "Interfaz deregistrada satisfactoreamente"

        if validate_response == '1':
            return Response(data=data)
        return Response(data={'VAL': validate_response, 'ERROR': error_msg}, status=status.HTTP_400_BAD_REQUEST)


class SendNewsMessageViewSet(APIView):
    serializer_class = NewsMessageSerializer
    metadata_class = SendNewsMessageMetadata

    def get(self, request, *args, **kwargs):
        validate_response = '1'
        error_msg = ''
        error_field = None
        data = {}

        if validate_response == '1':
            return Response(data=data)
        return Response(data={'ERROR': error_msg, 'FIELD': error_field}, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request):
        validate_response = '1'
        error_status = status.HTTP_400_BAD_REQUEST
        error_msg = ''
        actual_account = Account.getAccount(request.user)
        if actual_account is None:
            validate_response = '0'
            error_msg = 'Debe ser un usuario registrado'
        title = '(Sin Título)'
        message = None
        account_type = None
        account_list = None
        data = {}
        
        if 'title' in request.data.keys()and request.data['title']:
            title = request.data['title']
        if 'message' in request.data.keys() and request.data['message']:
            message = request.data['message']
        else:
            validate_response = '0'
            error_msg = 'Debe ingresar el Mensaje'
            
        if 'account_type' in request.data.keys() and request.data['account_type']:
            account_type_id = request.data['account_type']
            try:
                account_type = AccountType.objects.get(pk=account_type_id)
            except AccountType.DoesNotExist:
                account_type = None
            
        if 'account_list' in request.data.keys() and request.data['account_list']:
            account_id_list = request.data['account_list']
            
            account_list = Account.objects.filter(pk__in=account_id_list)
        
        if validate_response == '1':
            response = NewsMessage.createMessage(actual_account, title, message, account_type=account_type, account_list=account_list)
            if response != 200:
                error_status = response
        
        if validate_response == '1':
            data['SUCCESS'] = 'Mensaje enviado satisfactoreamente'
            return Response(data=data)
        return Response(data={'VAL': validate_response, 'ERROR': error_msg}, status=error_status)



