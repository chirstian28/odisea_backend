# -*- coding: utf-8 -*-
from rest_framework import serializers
from backend.components.auditor.models import AuditorParameter
from backend.auth.accounts.models import Account
from backend.components.pushmessages.models import PushInterfaceType,\
    PushInterface, Notification, Message, NewsMessage
from backend.core.models import ResourceHost

class PushInterfaceTypeSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        request = self.context['request']
        actual_account = Account.getAccount(request.user)
        if actual_account is not None:
            instance, instance_created = PushInterfaceType.objects.get_or_create(creation_username=actual_account.username, **validated_data)
            AuditorParameter.registerEvent(request, 'pushmessages', 'PushInterfaceType', validated_data, instance.id,
                                           AuditorParameter.CREATE,
                                           action='CREATE_PUSHINTERFACETYPE')

            return instance

    def update(self, instance, validated_data):
        request = self.context['request']
        actual_account = Account.getAccount(request.user)
        if actual_account is not None:
            old_serializer_data = PushInterfaceTypeSerializer(instance).data
            serializer = super(PushInterfaceTypeSerializer, self).update(instance, validated_data)
            instance.update_username = request.user.username
            instance.save()
            instance_data = PushInterfaceTypeSerializer(instance)
            AuditorParameter.registerEvent(request, 'pushmessages', 'PushInterfaceType', instance_data.data, instance.id,
                                           AuditorParameter.UPDATE, before_register_object=old_serializer_data,
                                           action='UPDATE_PUSHINTERFACETYPE')
            return serializer

    class Meta:
        model = PushInterfaceType
        verbose_name = 'Tipo de Interfaz'
        fields = ( "id", "name", )

class PushInterfaceTypeTableSerializer(serializers.ModelSerializer):

    class Meta:
        model = PushInterfaceType
        verbose_name = 'Tipos de Interfaces'
        fields = ("id", "name", )
        search_fields = ['$name', ]
        ordering_fields = ('name', )

class PushInterfaceTypeQuerySerializer(serializers.ModelSerializer):

    class Meta:
        model = PushInterfaceType
        verbose_name = 'Tipos de Interfaces'
        fields = ("id", "name", )
        search_fields = ['$name', ]
        ordering_fields = ('name', )

class PushInterfaceSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        request = self.context['request']
        actual_account = Account.getAccount(request.user)
        if actual_account is not None:
            instance, instance_created = PushInterface.objects.get_or_create(creation_username=actual_account.username, **validated_data)
            AuditorParameter.registerEvent(request, 'pushmessages', 'PushInterface', validated_data, instance.id,
                                           AuditorParameter.CREATE,
                                           action='CREATE_PUSHINTERFACE')

            return instance

    def update(self, instance, validated_data):
        request = self.context['request']
        actual_account = Account.getAccount(request.user)
        if actual_account is not None:
            old_serializer_data = PushInterfaceSerializer(instance).data
            serializer = super(PushInterfaceSerializer, self).update(instance, validated_data)
            instance.update_username = request.user.username
            instance.save()
            instance_data = PushInterfaceSerializer(instance)
            AuditorParameter.registerEvent(request, 'pushmessages', 'PushInterface', instance_data.data, instance.id,
                                           AuditorParameter.UPDATE, before_register_object=old_serializer_data,
                                           action='UPDATE_PUSHINTERFACE')
            return serializer

    class Meta:
        model = PushInterface
        verbose_name = 'Interfaz'
        fields = ("id", "interface_type", "sessionid", "interfaceid", "active", )

class PushInterfaceTableSerializer(serializers.ModelSerializer):
    interface_type = serializers.CharField(label='Tipo de Interafaz')
    
    class Meta:
        model = PushInterface
        verbose_name = 'Interfaces'
        fields = ("id", "interface_type", "sessionid", "interfaceid", "active", )
        ordering_fields = ('interface_type', )

class PushInterfaceQuerySerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField('get_alternate_name')

    def get_alternate_name(self, obj):
        return obj.interfaceid

    class Meta:
        model = PushInterface
        verbose_name = 'Interfaces'
        fields = ("id", "name", )

class NotificationSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        request = self.context['request']
        actual_account = Account.getAccount(request.user)
        if actual_account is not None:
            instance, instance_created = Notification.objects.get_or_create(creation_username=actual_account.username, **validated_data)
            AuditorParameter.registerEvent(request, 'pushmessages', 'Notification', validated_data, instance.id,
                                           AuditorParameter.CREATE,
                                           action='CREATE_NOTIFICATION')

            return instance

    def update(self, instance, validated_data):
        request = self.context['request']
        actual_account = Account.getAccount(request.user)
        if actual_account is not None:
            old_serializer_data = NotificationSerializer(instance).data
            serializer = super(NotificationSerializer, self).update(instance, validated_data)
            instance.update_username = request.user.username
            instance.save()
            instance_data = NotificationSerializer(instance)
            AuditorParameter.registerEvent(request, 'pushmessages', 'Notification', instance_data.data, instance.id,
                                           AuditorParameter.UPDATE, before_register_object=old_serializer_data,
                                           action='UPDATE_NOTIFICATION')
            return serializer

    class Meta:
        model = Notification
        verbose_name = 'Notificación'
        fields = ("id", "date_time", "account", "date_time", "source", "data", "sent", )

class NotificationTableSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Notification
        verbose_name = 'Notificaciones'
        fields = ("id", "date_time", "account", "source", )

class NotificationQuerySerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField('get_alternate_name')

    def get_alternate_name(self, obj):
        return obj.date_time.strftime('%Y%m%d') + '_' + obj.data

    class Meta:
        model = Notification
        verbose_name = 'Notificaciones'
        fields = ("id", "name", )

class MessageSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        request = self.context['request']
        actual_account = Account.getAccount(request.user)
        if actual_account is not None:
            instance, instance_created = Message.objects.get_or_create(creation_username=actual_account.username, **validated_data)
            AuditorParameter.registerEvent(request, 'pushmessages', 'Message', validated_data, instance.id,
                                           AuditorParameter.CREATE,
                                           action='CREATE_MESSAGE')

            return instance

    def update(self, instance, validated_data):
        request = self.context['request']
        actual_account = Account.getAccount(request.user)
        if actual_account is not None:
            old_serializer_data = MessageSerializer(instance).data
            serializer = super(MessageSerializer, self).update(instance, validated_data)
            instance.update_username = request.user.username
            instance.save()
            instance_data = MessageSerializer(instance)
            AuditorParameter.registerEvent(request, 'pushmessages', 'Message', instance_data.data, instance.id,
                                           AuditorParameter.UPDATE, before_register_object=old_serializer_data,
                                           action='UPDATE_MESSAGE')
            return serializer

    class Meta:
        model = Message
        verbose_name = 'Mensaje'
        fields = ("id", "date_time", "message_type", "message_class", "interface_type", "push_interface", 
                  "data", "sent", 
            )

class MessageTableSerializer(serializers.ModelSerializer):
    interface_type = serializers.CharField(label='Tipo de Interfaz')
    message_type = serializers.CharField(label='Tipo de Mensaje')
    message_class = serializers.CharField(label='Clase de Mensaje')

    class Meta:
        model = Message
        verbose_name = 'Mensajes'
        fields = ("id", "date_time", "message_type", "message_class", "interface_type", )

class MessageQuerySerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField('get_alternate_name')

    def get_alternate_name(self, obj):
        return obj.date_time.strftime('%Y%m%d') + '_' + obj.data

    class Meta:
        model = Message
        verbose_name = 'Mensajes'
        fields = ("id", "name", )

class RegisterInterfaceSerializer(serializers.Serializer):
    interface_type = serializers.CharField(label='Tipo de Interfaz')
    sessionid = serializers.CharField(label='ID Sesión', required=True)
    interfaceid = serializers.CharField(label='ID Interfaz', required=True)
    
    class Meta:
        verbose_name = 'Registro de Interfaz'
        fields = ("interface_type", "sessionid", 'interfaceid',  )

class NewsMessageSerializer(serializers.ModelSerializer):

    class Meta:
        model = NewsMessage
        verbose_name = 'Noticia'
        fields = ("id", "title", "message", "account_type", "account_list", )
        fields_actions = {
                    'account_type': {'fill_field': ['account_list'], 
                        },
                    'account_list': { 'fill_url': ResourceHost.getURL('BASE_URL') + '/accounts/accountfilter/', 'fill_group_by': [{'name':'account_type', 'filter_null': True}],
                        },
                }

class NewsMessageTableSerializer(serializers.ModelSerializer):
    source_account = serializers.CharField(label='Cuenta Fuente')
    
    class Meta:
        model = NewsMessage
        verbose_name = 'Noticias'
        fields = ("id", "date_time", "source_account", "title", "message", "sent", )

class NewsMessageQuerySerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField('get_alternate_name')

    def get_alternate_name(self, obj):
        return obj.date_time.strftime('%Y%m%d') + '_' + obj.data

    class Meta:
        model = NewsMessage
        verbose_name = 'Noticias'
        fields = ("id", "name", )

