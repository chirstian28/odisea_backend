# -*- coding: utf-8 -*-
from rest_framework import serializers
from backend.components.errors.models import ErrorType, ErrorMessage
 
class ErrorTypeSerializer(serializers.ModelSerializer):
 
    class Meta:
        model = ErrorType
        fields = ("id", "name")
 
class ErrorMessageSerializer(serializers.ModelSerializer):
 
    class Meta:
        model = ErrorMessage
        fields = ("id", "error_type", "name", "description", )
 
class ErrorMessageQuerySerializer(serializers.ModelSerializer):
 
    class Meta:
        model = ErrorMessage
        fields = ("id", "name", )
 
class ErrorMessageTableSerializer(serializers.ModelSerializer):
 
    class Meta:
        model = ErrorMessage
        fields = ("id", "error_type", "name", )
 

