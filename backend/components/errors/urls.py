from backend.components.errors import views
from rest_framework.routers import DefaultRouter
from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns

router = DefaultRouter()
 
router.register(r'errortype', views.ErrorTypeViewSet)
router.register(r'errormessage', views.ErrorMessageViewSet)
router.register(r'errormessagetable', views.ErrorMessageTableViewSet,basename='errormessagetable')
router.register(r'errormessagequery', views.ErrorMessageQueryViewSet,basename='errormessagequery')

urlpatterns = [
    url(r'', include(router.urls)), 
]

urlpatterns += format_suffix_patterns([
])

    
