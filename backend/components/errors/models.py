# -*- coding: utf-8 -*-
from django.db import models
import datetime
from backend.library.sendmail import send_mail

class ErrorType(models.Model):
    id = models.CharField(max_length=50, primary_key=True)
    name = models.CharField(max_length=50)
 
    def __str__(self):
        return u"%s" % (self.name)
 
    class Meta:
        app_label = 'errors'

class ErrorMessage(models.Model):
    id = models.CharField(max_length=50, primary_key=True)
    error_type = models.ForeignKey('ErrorType', on_delete=models.PROTECT)
    name = models.CharField(max_length=255, help_text='large')
    description = models.TextField(max_length=255, null=True, blank=True, help_text='xlarge')
    
    def __str__(self):
        return u"%s" % (self.name)

    class Meta:
        app_label = 'errors'


