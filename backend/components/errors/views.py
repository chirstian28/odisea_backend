# -*- coding: utf-8 -*-
from rest_framework import viewsets
from backend.core.permissions import IsAdminOrReadOnly, IsAdminAndReadOnly
from backend.components.errors.models import ErrorType, ErrorMessage
from backend.components.errors.serializers import ErrorTypeSerializer,\
    ErrorMessageSerializer, ErrorMessageTableSerializer,\
    ErrorMessageQuerySerializer
from backend.core.views import GenericModelViewSet, GenericTableViewSet,\
    GenericQueryViewSet
from rest_framework.permissions import IsAuthenticated
 
class ErrorTypeViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = ErrorType.objects.all()
    serializer_class = ErrorTypeSerializer
    permission_classes = (IsAdminOrReadOnly,)
 
class ErrorMessageViewSet(GenericModelViewSet):
    app_name = 'errors'
    model_name = 'ErrorMessages'
    queryset = ErrorMessage.objects.all()
    serializer_class = ErrorMessageSerializer
    permission_classes = (IsAuthenticated, IsAdminOrReadOnly,)
     
class ErrorMessageTableViewSet(GenericTableViewSet):
    app_name = 'errors'
    model_name = 'ErrorMessages'
    queryset = ErrorMessage.objects.all()
    serializer_class = ErrorMessageTableSerializer
    permission_classes = (IsAdminAndReadOnly,)
     
class ErrorMessageQueryViewSet(GenericQueryViewSet):
    app_name = 'errors'
    model_name = 'ErrorMessages'
    queryset = ErrorMessage.objects.all()
    serializer_class = ErrorMessageQuerySerializer
    permission_classes = (IsAdminAndReadOnly,)
