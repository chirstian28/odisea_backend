# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
import datetime
from django.core.management import call_command
from backend.core.models import ResourceHost, Environment
import os

def upload_file_to(instance, filename):
    filename_base, filename_ext = os.path.splitext(filename)
    url_path = settings.DBBACKUP_BACKUP_DIRECTORY
    url_path += filename_base.replace(' ','_')
    url_path += filename_ext.lower()
    return url_path

class BackupFile(models.Model):
    environment = models.CharField(verbose_name='Entorno', unique=True, max_length=30)
    backup_file = models.FileField(max_length=255, verbose_name="Archivo Backup", upload_to=upload_file_to, null=True, blank=True)
    creation_date_time = models.DateTimeField(verbose_name="Fecha y Hora de Creación", auto_now_add=True)
    
    def delete(self, *args, **kwargs):
        from django.core.files.storage import default_storage as storage
        if self.backup_file is not None:
            storage.delete(self.backup_file.name)
        super(BackupFile, self).delete()
        
    def get_datafile_url(self):
        from django.core.files.storage import default_storage as storage
        if not self.backup_file.name:
            return ""
        if storage.exists(self.backup_file.name):
            base_url = ''
            if settings.STORAGE_TYPE != "S3":
                base_url = ResourceHost.getURL('BASE_URL')
            return base_url + storage.url(self.backup_file.name)
        return ""

    def createBackup(logger=None):
        from django.core.files.base import ContentFile
        import zipfile
        
        filename = settings.COMERCIAL_APP_NAME.upper()
        filename += '_'
        filename += Environment.getEnvironmentName()
        filename += '_'
        filename += datetime.datetime.now().strftime('%Y%m%d%H%M%S')
        filename += '.json'
        if logger is not None:
            logger.info('filename: ' + filename)
        
        filepath = settings.TMP_ROOT
        filepath += filename
        
        print (filepath)
        try:
            call_command('dumpdata', '-a', '-o', filepath )
        except Exception as e:
            if logger is not None:
                logger.error('Error al generar backup: %s' % str(e))

        zipfilepath = filepath + '.bz'
        zipfilename = filename + '.bz'

        myzip = zipfile.ZipFile(zipfilepath, 'w', zipfile.ZIP_BZIP2)
        myzip.write(filepath, filename)
        myzip.close()

        os.remove(filepath)

        backup_fp = open(zipfilepath, 'rb')
        content_file = backup_fp.read()
        backup_fp.close()

        os.remove(zipfilepath)
        
        backup_file = BackupFile()
        backup_file.environment = Environment.getEnvironmentName()
        backup_file.backup_file.save(zipfilename, ContentFile(content_file))
        backup_file.save()

    createBackup = staticmethod(createBackup)
    
    def getDict(self):
        result_dict = {}
        result_dict['file'] = {}
        result_dict['file']['id'] = self.id 
        result_dict['file']['filename'] = self.backup_file.name.split('/')[-1]
        result_dict['file']['file_url'] = self.get_datafile_url()
        return result_dict

    def restoreBackup(self, logger):
        pass
    
    class Meta:
        app_label = 'backup'
        ordering = ['environment', '-creation_date_time']
        verbose_name = 'Copia de Respaldo'
        verbose_name_plural = 'Copias de Respaldo'
