from backend.components.backup import views
from rest_framework import routers
from django.urls import path

router = routers.DefaultRouter()

router.register(r'backupfile', views.BackupFileViewSet, basename='backupfile')
router.register(r'backupfiletable', views.BackupFileTableViewSet, basename='backupfiletable')

urlpatterns = [
    path('downloadfile/<int:backup_file_id>/', views.downloadBackupViewSet.as_view()),
]

urlpatterns += router.urls

