# -*- coding: utf-8 -*-
from backend.core.metadata import GenericTableMetadata

class BackupFileTableMetadata(GenericTableMetadata):

    def determine_metadata(self, request, view):
        meta_data_dict = GenericTableMetadata.determine_metadata(self, request, view)
        del meta_data_dict['actions']['BASIC']['UPDATE']
        del meta_data_dict['actions']['BASIC']['QUERY']
        del meta_data_dict['buttons']['ADD']
        
        return meta_data_dict
    
