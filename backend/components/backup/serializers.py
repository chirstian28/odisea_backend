# -*- coding: utf-8 -*-
from rest_framework import serializers
from backend.components.backup.models import BackupFile
from backend.components.auditor.models import AuditorParameter
from backend.core.models import Environment

class BackupFileSerializer(serializers.ModelSerializer):
 
    def create(self, validated_data):
        request = self.context['request']
        instance, instance_created = BackupFile.objects.get_or_create(environment=Environment.getEnvironmentName(), **validated_data)
        instance_data = BackupFileSerializer(instance)
        AuditorParameter.registerEvent(request, 'backup', 'BackupFile', instance_data.data, instance.id, AuditorParameter.CREATE, action='CREATE_BACKUPFILE')
        return instance

    class Meta:
        verbose_name = 'Copia de Respaldo'
        model = BackupFile
        fields = ("id", "creation_date_time", "backup_file",)
                      
class BackupFileTableSerializer(serializers.ModelSerializer):
    creation_date_time = serializers.SerializerMethodField('get_date_time')

    def get_date_time(self, obj):
        return "%s" % obj.creation_date_time.strftime('%Y-%m-%d %H:%M')

    class Meta:
        model = BackupFile
        verbose_name = 'Copia de Respaldo'
        fields = ("id", "creation_date_time", )
        ordering_fields = ("creation_date_time", )

        basic_actions = {'DOWNLOAD': {
                                    'order': 5,
                                    'title': 'Descargar',
                                    'kind': 'DOWNLOAD',
                                    'url': '/backup/downloadfile/',
                                    'disable': False,
                                    'icon': 'Download',
                                    'backurl': '/backup/backupfiletable/',
                                    'method': 'GET',
                                }
            }

                
