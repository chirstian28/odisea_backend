# -*- coding: utf-8 -*-
from backend.core.views import GenericTableViewSet, GenericModelViewSet
from backend.components.backup.models import BackupFile
from backend.components.backup.serializers import BackupFileTableSerializer, BackupFileSerializer
from backend.core.permissions import IsAdminAndReadOnly
from backend.components.backup.metadata import BackupFileTableMetadata
from backend.core.models import Environment
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

class BackupFileViewSet(GenericModelViewSet):
    app_name = 'backup'
    model_name = 'BackupFile'
    queryset = BackupFile.objects.all()
    serializer_class = BackupFileSerializer
    filter_licenser = False

class BackupFileTableViewSet(GenericTableViewSet):
    app_name = 'backup'
    model_name = 'BackupFile'
    queryset = BackupFile.objects.all()
    serializer_class = BackupFileTableSerializer
    permission_classes = (IsAdminAndReadOnly,)
    metadata_class = BackupFileTableMetadata
    filter_licenser = False

class downloadBackupViewSet(APIView):
    title = 'Descargar Archivo'
    
    def get(self, request, backup_file_id):
        error = None
            
        backup_file = None
        data = {}

        if error is None:
            try:
                backup_file = BackupFile.objects.get(environment=Environment.getEnvironmentName(), pk=backup_file_id)
            except BackupFile.DoesNotExist:
                error = 'Registro no encontrado'
         
        if error is None:
            data = backup_file.getDict()

        if error is None:
            return Response(data=data)
        return Response(data={'level': 'error', 'message': error}, status=status.HTTP_400_BAD_REQUEST)
