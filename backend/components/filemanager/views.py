# -*- coding: utf-8 -*-
from backend.core.permissions import IsAdminOrReadOnly, IsAdminAndReadOnly
from backend.components.filemanager.models import TemporalFile, SegmentTemporalFile,\
    ImportLog
from backend.components.filemanager.serializers import TemporalFileSerializer,\
    TemporalFileQuerySerializer, UploadSegmentTemporalFileSerializer,\
    ImportSerializer, ImportLogQuerySerializer, ImportLogTableSerializer,\
    ImportLogSerializer
from backend.components.filemanager.metadata import GenericImportDataModelMetadata,\
    ImportLogMetadata
from backend.core.views import GenericQueryViewSet, GenericModelViewSet,\
    GenericTableViewSet
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from backend.core.metadata import GenericMetadata
from backend.auth.accounts.models import Account
import datetime
from rest_framework import status
from rest_framework.response import Response
from django.core.files.base import ContentFile
from backend.core.models import ResourceHost

class TemporalFileQueryViewSet(GenericQueryViewSet):
    app_name = 'filemanager'
    model_name = 'TemporalFile'
    queryset = TemporalFile.objects.all()
    serializer_class = TemporalFileQuerySerializer
    permission_classes = (IsAdminAndReadOnly,)
    filter_licenser = False

class TemporalFileViewSet(GenericModelViewSet):
    app_name = 'filemanager'
    model_name = 'TemporalFile'
    queryset = TemporalFile.objects.all()
    serializer_class = TemporalFileSerializer
    permission_classes = (IsAuthenticated, IsAdminOrReadOnly,)
    filter_licenser = False
   
class ImportLogQueryViewSet(GenericQueryViewSet):
    app_name = 'filemanager'
    model_name = 'ImportLog'
    queryset = ImportLog.objects.all()
    serializer_class = ImportLogQuerySerializer
    permission_classes = (IsAdminAndReadOnly,)
    filter_licenser = False

class ImportLogTableViewSet(GenericTableViewSet):
    app_name = 'filemanager'
    model_name = 'ImportLog'
    queryset = ImportLog.objects.all()
    serializer_class = ImportLogTableSerializer
    permission_classes = (IsAdminAndReadOnly,)
    filter_licenser = False

class ImportLogViewSet(GenericModelViewSet):
    app_name = 'filemanager'
    model_name = 'ImportLog'
    queryset = ImportLog.objects.all()
    serializer_class = ImportLogSerializer
    permission_classes = (IsAuthenticated, IsAdminOrReadOnly,)
    filter_licenser = False
    
class UploadTemporalFileViewSet(APIView):
    metadata_class = GenericMetadata
    serializer_class = UploadSegmentTemporalFileSerializer
    title = 'Cargue de Archivo'

    def post(self, request, *args, **kwargs):
        actual_account = Account.getAccount(request.user)
        validate_response = '1'
        data = {}
        error_msg = ''
        name = None
        file_type = None
        segments_quantity = None

        if  actual_account is None:
            validate_response = '0'
            error_msg = 'El usuario debe estar registrado en el sistema'
    
        if validate_response == '1':
            if 'name' in request.data.keys() and request.data['name']:
                name = request.data['name']
            else:
                validate_response = '0'
                error_msg = 'No está el nombre del archivo'
        
        if validate_response == '1':
            if 'quantity' in request.data.keys() and request.data['quantity']:
                try:
                    segments_quantity = int(request.data['quantity'])
                except:
                    validate_response = '0'
                    error_msg = 'El formato de la cantidad de segmentos es errado'
            else:                    
                validate_response = '0'
                error_msg = 'No se Indicó la cantidad de Segmentos'
            
        if validate_response == '1':
            if 'type' in request.data.keys() and request.data['type']:
                file_type = request.data['type']
     
            temporal_file = TemporalFile()
            temporal_file.date_time = datetime.datetime.now()
            temporal_file.name = name
            temporal_file.type = file_type
            temporal_file.username = actual_account.username
            temporal_file.segments_quantity = segments_quantity
            temporal_file.save()
             
            data['id'] = temporal_file.id
 
        data['VAL'] = validate_response
        if validate_response == '1':
            data['SUCCESS'] = 'Se han importado el archivo correctamente'
            response_status = status.HTTP_200_OK
        else:
            response_status = status.HTTP_400_BAD_REQUEST
            data['ERROR'] = error_msg
        return Response(data=data, status=response_status)
         
    def put(self, request, create_file='no'):
        
        actual_account = Account.getAccount(request.user)
        validate_response = '1'
        data = {}
        error_msg = ''
        temporal_file_id = 0
        segment_number = None
        segment_content = ''
        
        if  actual_account is None:
            validate_response = '0'
            error_msg = 'El usuario debe estar registrado en el sistema'
            
        if validate_response == '1':
            if 'id' in request.data.keys() and request.data['id']:
                try:
                    temporal_file_id = int(request.data['id'])
                except:
                    validate_response = '0'
                    error_msg = 'Debe Ingresar el Id del Archivo'
            else:                    
                validate_response = '0'
                error_msg = 'No se Indicó el Id del Archivo'
    
        if validate_response == '1':
            if 'content' in request.data.keys() and request.data['content']:
                segment_content = request.data['content']
            else:                    
                validate_response = '0'
                error_msg = 'No se adjuntó el Segmento'
            
        if validate_response == '1':
            if 'number' in request.data.keys() and request.data['number'] is not None:
                try:
                    segment_number = int(request.data['number'])
                except:
                    validate_response = '0'
                    error_msg = 'El formato del número de segmento es errado'
            else:                    
                validate_response = '0'
                error_msg = 'No se indicó el número de segmento'

        if validate_response == '1':
            try:
                temporal_file = TemporalFile.objects.get(pk=temporal_file_id)
            except TemporalFile.DoesNotExist:
                validate_response = '0'
                error_msg = 'No Existe el archivo indicado'
        
        if validate_response == '1':
            
            try:
                segment_temporal_file = SegmentTemporalFile.objects.get(temporal_file=temporal_file, segment_number=segment_number)
            except SegmentTemporalFile.DoesNotExist:
                segment_temporal_file = SegmentTemporalFile()
                segment_temporal_file.temporal_file = temporal_file
                segment_temporal_file.segment_number = segment_number
            segment_temporal_file.segments_quantity = temporal_file.segments_quantity
            segment_temporal_file.segment_content = segment_content
            segment_temporal_file.username = actual_account.username
            segment_temporal_file.save()
           
            uploaded_segments_quantity = SegmentTemporalFile.objects.filter(temporal_file=temporal_file).count()
            
            data['number'] = segment_number
                
            if temporal_file.segments_quantity == uploaded_segments_quantity:
                try:
                    data_file = temporal_file.getTotalContent()
                    temporal_file.content = data_file
                    if create_file:
                        temporal_file.data_file.save(temporal_file.name, ContentFile(TemporalFile.generateBinary(data_file)))
                    else:
                        temporal_file.content = data_file
                    temporal_file.save()
                    
                    SegmentTemporalFile.objects.filter(temporal_file=temporal_file).delete()
                except:
                    validate_response = '0'
                    error_msg = 'Error al crear el archivo'

        data['VAL'] = validate_response
        if validate_response == '1':
            data['id'] = temporal_file.id
            response_status = status.HTTP_200_OK
        else:
            response_status = status.HTTP_400_BAD_REQUEST
            data['ERROR'] = error_msg
        return Response(data=data, status=response_status)

class DeleteTemporalFileViewSet(APIView):
    metadata_class = GenericMetadata
    serializer_class = TemporalFileSerializer
    title = 'Eliminación de Archivo'
    
    def post(self, request, *args, **kwargs):
        actual_account = Account.getAccount(request.user)
        validate_response = '1'
        data = {}
        error_msg = ''
        file_id = None
        
        if 'id' in request.data.keys() and request.data['id']:
            file_id = request.data['id']
        else:
            validate_response = '0'
            error_msg = 'No está el ID del archivo'
        
        if  actual_account is not None:
            if file_id is not None:
                try:
                    temporal_file = TemporalFile.objects.get(pk=file_id)
                    temporal_file.delete()
                except TemporalFile.DoesNotExist:
                    pass
                
            else:
                validate_response = '0'
                error_msg = 'No se ingresó el ID del archivo'
        else:
            validate_response = '0'
            error_msg = 'El usuario no se ha registrado'

        data['VAL'] = validate_response
        if validate_response == '1':
            data['SUCCESS'] = 'Se ha eliminado el archivo correctamente'
            response_status = status.HTTP_200_OK
        else:
            response_status = status.HTTP_400_BAD_REQUEST
            data['ERROR'] = error_msg
        return Response(data=data, status=response_status)
    
class GenericImportDataModelViewSet(APIView):
    metadata_class = GenericImportDataModelMetadata
    serializer_class = ImportSerializer

    def post(self, request, *args, **kwargs):
        import base64
        actual_account = Account.getAccount(request.user)
        validate_response = '1'
        error_msg = ''

        import_file = None
        data = {}
        other_parameters = {}
        
        if actual_account is None:
            validate_response = '0'
            error_msg = 'El usuario no se ha registrado'

        import_log = ImportLog()
        import_log.date_time = datetime.datetime.now()

        if actual_account is not None:
            import_log.username = actual_account.username
            
        if validate_response == '1':
            if 'import_file' in request.data.keys() and request.data['import_file']:
                import_file = request.data['import_file']
            else:                    
                validate_response = '0'
                error_msg = 'No se Anexó el Identificador del Archivo a procesar'
        
        
        if validate_response == '1':
            try:
                temp_file = TemporalFile.objects.get(pk=import_file)
                import_log.filename = temp_file.name
            except:
                temp_file = None

        if validate_response == '1':
            for key in request.data.keys():
                if key != 'import_file' and request.data[key]:
                    other_parameters[key] =  request.data[key]
        
            binary_file = None
            if temp_file is not None and temp_file.content:
                binary_file = base64.b64decode(temp_file.content.encode('utf-8'))
            elif import_file is not None:
                if import_file.content_type == 'application/json':
                    binary_file = import_file
                else:
                    binary_file = import_file.read()
        
            if binary_file is not None:
                try:
                    import_log.name = self.model_class.__name__
                    import_result = self.model_class.importData(actual_account, binary_file, other_parameters)
                    import_log.log_messages = import_result['LOG']
                    import_log.imported_register = import_result['OK']
                    import_log.error_register = import_result['ERROR']
                    import_log.total_register = import_result['TOTAL']
                except Exception as e:
                    validate_response = '0'
                    error_msg = 'No se pudo importar el archivo: %s' % str(e)
            else:
                validate_response = '0'
                error_msg = 'Error en el contenido del archivo'

        data['SUCCESS'] = 'Se han importado %d Registros' % import_log.imported_register

        if validate_response == '0':
            import_log.message = error_msg
        else:
            import_log.message = data['SUCCESS']
        import_log.save()

        data['dialog_url'] = ResourceHost.getURL('BASE_URL') + '/filemanager/checklog/%d/' % import_log.id
        data['VAL'] = validate_response
        result_status = status.HTTP_200_OK
        if validate_response == '0':
            result_status = status.HTTP_400_BAD_REQUEST
            data['ERROR'] = error_msg
        
        return Response(data=data, status=result_status)

class CheckLogViewSet(APIView):
    metadata_class = ImportLogMetadata
    serializer_class = ImportLogSerializer
    title = 'Log de Importación'
    
    def get(self, request, import_log_id):
        validate_response = '1'
        error_msg = ''
        account = Account.getAccount(request.user)
        
        try:
            import_log = ImportLog.objects.get(pk=import_log_id)
        except ImportLog.DoesNotExist:
            validate_response = '0'
            error_msg = 'No Existe un Reporte de Importación con el ID enviado.'
    
        if validate_response == '1':
            data = ImportLogSerializer(import_log).data
                
        if validate_response == '1':
            return Response(data=data)
        return Response(data={'VAL': validate_response, 'ERROR': error_msg}, status=status.HTTP_400_BAD_REQUEST)
