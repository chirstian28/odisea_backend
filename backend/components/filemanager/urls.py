from backend.components.filemanager import views
from rest_framework import routers
from django.urls import path

router = routers.DefaultRouter()

router.register(r'temporalfile', views.TemporalFileViewSet)
router.register(r'temporalfilequery', views.TemporalFileQueryViewSet)
router.register(r'importlog', views.ImportLogViewSet)
router.register(r'importlogquery', views.ImportLogQueryViewSet)
router.register(r'importlogtable', views.ImportLogTableViewSet)

urlpatterns = [
    path('uploadtemporalfile/', views.UploadTemporalFileViewSet.as_view()),
    path('uploadtemporalfile/<int:create_file>/', views.UploadTemporalFileViewSet.as_view()),
    path('deletetemporalfile/', views.DeleteTemporalFileViewSet.as_view()),
    path('checklog/<int:import_log_id>/', views.CheckLogViewSet.as_view()),
]

urlpatterns += router.urls
