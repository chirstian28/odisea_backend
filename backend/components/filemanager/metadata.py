# -*- coding: utf-8 -*-
from backend.core.metadata import GenericMetadata

class GenericImportDataModelMetadata(GenericMetadata):

    def determine_metadata(self, request, view):
        meta_data_dict = GenericMetadata.determine_metadata(self, request, view)
        meta_data_dict['savebutton'] = 'Importar'
        meta_data_dict['exitbutton'] = 'Cancelar'
        return meta_data_dict
    
class ImportLogMetadata(GenericMetadata):

    def determine_metadata(self, request, view):
        meta_data_dict = GenericMetadata.determine_metadata(self, request, view)
        meta_data_dict['savebutton'] = None
        meta_data_dict['exitbutton'] = 'Aceptar'
        meta_data_dict['type'] = 'LOG'
        try:
            meta_data_dict['title'] = view.title
        except:
            pass
        return meta_data_dict
