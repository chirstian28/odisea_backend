# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
import os
import base64
from backend.core.models import ResourceHost

def temporal_file_upload_to_data_file(instance, filename):
    from django.utils.timezone import now
    filename_base, filename_ext = os.path.splitext(filename)
    folder = f'{settings.UPLOAD_DIR}'
    filename_base = f"{filename_base.replace(' ','_')}_{now().strftime('%Y%m%d%H%M%S')}"
    return f'{folder}/{filename_base}{filename_ext.lower()}'

class SegmentTemporalFile(models.Model):
    creation_date_time = models.DateTimeField(verbose_name='Fecha y Hora', auto_now=True)
    temporal_file = models.ForeignKey('TemporalFile', verbose_name="Archivo Temporal", on_delete=models.CASCADE)
    segment_number = models.IntegerField(verbose_name='Número de Segmento', default=0)
    segment_content = models.TextField(verbose_name='Contenido del Segmento')
    username = models.CharField(max_length=100, verbose_name="Usuario", help_text='hidden')
    
    def __str__(self):
        return u"%s %d" % (self.temporal_file, self.segment_number)
    
    class Meta:
        app_label = 'filemanager'
        ordering = ('temporal_file','segment_number',)
        verbose_name = 'Segmento Archivo Temporal'
        verbose_name_plural = 'Segmentos Archivos Temporales'

class TemporalFile(models.Model):
    date_time = models.DateTimeField(verbose_name='Fecha y Hora', help_text='readonly', auto_now_add=True)
    name = models.CharField(max_length=100, db_index=True, verbose_name='Nombre', help_text='large')
    type = models.CharField(max_length=100, verbose_name='Tipo de Archivo', help_text='large', null=True, blank=True)
    username = models.CharField(max_length=100, verbose_name="Usuario", help_text='hidden')
    data_file = models.FileField(max_length=255, verbose_name="Archivo Cargado", upload_to=temporal_file_upload_to_data_file, null=True, blank=True)
    content = models.TextField(verbose_name='Contenido', null=True, blank=True)
    segments_quantity = models.IntegerField(verbose_name='Cantidad de Segmentos', default=0)
        
    def __str__(self):
        return u"%s" % (self.name)

    def getDict(self):
        result_dict = {}
        result_dict['file'] = {}
        result_dict['file']['id'] = self.id 
        result_dict['file']['filename'] = self.data_file.name.split('/')[-1]
        result_dict['file']['file_url'] = self.get_datafile_url()
        return result_dict

    def getTotalContent(self):
        content_file = ''
        segment_temporal_file_list = SegmentTemporalFile.objects.filter(temporal_file=self).order_by('segment_number')
        for segment_temporal_file in segment_temporal_file_list:
            content_file += segment_temporal_file.segment_content
        return content_file
    
    def delete(self, *args, **kwargs):
        from django.core.files.storage import default_storage as storage
        if self.data_file.name:
            if storage.exists(self.data_file.name):
                storage.delete(self.data_file.name)
        super(TemporalFile, self).delete()

    def get_datafile_url(self):
        from django.core.files.storage import default_storage as storage
        if not self.data_file.name:
            return ""
        if storage.exists(self.data_file.name):
            base_url = ''
            if settings.STORAGE_TYPE != "S3":
                base_url = ResourceHost.getURL('BASE_URL')
            return base_url + storage.url(self.data_file.name)
        return ""
    
    def get_filename(self):
        if not self.data_file.name:
            return ""
        return self.data_file.name.split('/')[-1]

    def generateBinary(data_base64):
        data_binary = base64.b64decode(data_base64)
        return data_binary

    generateBinary = staticmethod(generateBinary)

    class Meta:
        app_label = 'filemanager'
        ordering = ('name',)
        verbose_name = 'Archivo Temporal'
        verbose_name_plural = 'Archivos Temporales'

class ImportLog(models.Model):
    date_time = models.DateTimeField(verbose_name='Fecha y Hora', help_text='readonly')
    name = models.CharField(max_length=100, null=True, blank=True, verbose_name='Nombre', help_text='large')
    filename = models.CharField(max_length=100, null=True, blank=True, verbose_name='Nombre de Archivo', help_text='large')
    username = models.CharField(max_length=100, verbose_name="Usuario", null=True, blank=True, help_text='hidden')
    message = models.CharField(max_length=100, default='', verbose_name="Resultado", help_text='hidden')
    total_register = models.IntegerField(verbose_name='Cantidad de Registros', default=0)
    imported_register = models.IntegerField(verbose_name='Cantidad de Importados', default=0)
    error_register = models.IntegerField(verbose_name='Cantidad de Errados', default=0)
    log_messages = models.TextField(verbose_name='Mensajes de Log', default='', help_text='huge')
    
    def __str__(self):
        return u"%s" % (self.name)

    class Meta:
        app_label = 'filemanager'
        ordering = ('filename',)
        verbose_name = 'Log'
        verbose_name_plural = 'Log'
