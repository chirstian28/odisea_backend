# -*- coding: utf-8 -*-
from rest_framework import serializers
from backend.components.filemanager.models import TemporalFile, ImportLog

class UploadSegmentTemporalFileSerializer(serializers.Serializer):
    id = serializers.IntegerField(label='ID de Archivo')
    content = serializers.CharField(label='Contenido del Segmento')
    number = serializers.IntegerField(label='Número de Segmento')
    quantity = serializers.IntegerField(label='Cantidad de Segmentos')
    name = serializers.CharField(label='Nombre Archivo')
    type = serializers.CharField(label='Tipo de Archivo')

    class Meta:
        fields = ("name", "type", "number", "total", "content", )
            
class TemporalFileSerializer(serializers.ModelSerializer):

    class Meta:
        model = TemporalFile
        fields = ("name", "data_file", "type" )

class TemporalFileQuerySerializer(serializers.ModelSerializer):

    class Meta:
        model = TemporalFile
        fields = ("id", "name", )
     
class ImportLogSerializer(serializers.ModelSerializer):

    class Meta:
        model = ImportLog
        fields = ("date_time", "name", "filename", "message", "total_register", "imported_register", "error_register","username", "log_messages", )

class ImportLogQuerySerializer(serializers.ModelSerializer):

    class Meta:
        model = ImportLog
        fields = ("id", "name", )

class ImportLogTableSerializer(serializers.ModelSerializer):

    class Meta:
        model = ImportLog
        fields = ("date_time", "name", "filename", "total_register", "imported_register", "error_register", )

class ImportSerializer(serializers.Serializer):
    import_file = serializers.FileField(max_length=100, label="Archivo a importar",help_text='large|uploadtemporalcontent', required=True,use_url=True)
 
    class Meta:
        verbose_name = 'Importar Archivo'
        fields = ("import_file",)
