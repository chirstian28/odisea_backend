import django_filters

from backend.components.menu.models import Option

class OptionTableFilter(django_filters.rest_framework.FilterSet):

    class Meta:
        model = Option
        fields = ["type"]
