# -*- coding: utf-8 -*-
from backend.components.menu import views
from rest_framework import routers
from django.urls import path

router = routers.DefaultRouter()

router.register(r'option', views.OptionViewSet, basename='option')
router.register(r'optiontable', views.OptionTableViewSet, basename='optiontable')

urlpatterns = [
                path('query/', views.MenuView.as_view()),
]

urlpatterns += router.urls
