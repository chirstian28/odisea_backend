from django.db import models
from backend.auth.accounts.models import UserDate

class Menu(models.Model):
    
    def getMenu(account, parent=None):
        
        menu_dict = {}
        option_dict_list = []
        option_list = Option.objects.all()
        if parent is None:
            option_list = option_list.filter(parentid__isnull=True)
        else:
            option_list = option_list.filter(parentid=parent.id)
        
        for option in option_list:
            menuoption_dict = option.getDict(account)
            if menuoption_dict is not None:
                option_dict_list.append(menuoption_dict)
        if len(option_dict_list) > 0:
            menu_dict['links'] = option_dict_list  
            return [menu_dict]
    
    getMenu = staticmethod(getMenu)
   
    class Meta:
        abstract = True
        app_label = 'menu'
        verbose_name = 'Menú'
        verbose_name_plural = 'Menús'
     
class OptionType(models.Model):
    id = models.CharField(max_length=5, primary_key=True)
    name = models.CharField(max_length=60)

    def __str__(self):
        return "%s" % (self.name)

    class Meta:
        ordering = ['id']
        app_label = 'menu'

class Option(UserDate):
    id = models.IntegerField(verbose_name='Código', primary_key=True)
    name = models.CharField(max_length=50, verbose_name='Título')
    url = models.CharField(max_length=100, null=True, blank=True, verbose_name='URL')
    type = models.ForeignKey('OptionType', verbose_name='Tipo de Opción', on_delete=models.PROTECT)
    description = models.CharField(max_length=100, verbose_name='Descripción', null=True, blank=True)
    target = models.CharField(null=True, blank=True, verbose_name='Target', max_length=30)
    parentid = models.IntegerField(null=True, blank=True, verbose_name='ID menu padre')
    icon = models.CharField(max_length=100, verbose_name='Icono', null=True, blank=True)
    accounttype_list = models.ManyToManyField('accounts.AccountType', verbose_name='Tipos de Cuenta')

    def __str__(self):
        return "%s" % (self.title)
        
    def getDict(self, account):
        option_dict = None
        
        if account is not None and self.accounttype_list.filter(pk=account.type.id).count() > 0:
            option_dict = {
                        "key": f"MENU_{self.id}",
                        "name": self.name,
                        "type": self.type.id,
                    }

        if option_dict is not None: 
            if self.target is not None:
               option_dict['target'] = self.target
            if self.url is not None:
                option_dict['url'] = self.url
            if self.icon is not None:
                option_dict['icon'] = self.icon

            sub_menu_list = Menu.getMenu(account, self)
            if sub_menu_list is not None and len(sub_menu_list) == 1:
                sub_menu = sub_menu_list[0]
                if sub_menu is not None:
                    option_dict.update(sub_menu)
                    option_dict['isExpanded'] = False
                    del option_dict['key']
                    if 'icon' in option_dict.keys() and option_dict['icon'] is not None:
                        del option_dict['icon']
        return option_dict
    
    class Meta:
        ordering = ['id']
        app_label = 'menu'
        verbose_name = 'Opción Menú'
        verbose_name_plural = 'Opciones Menú'
       
