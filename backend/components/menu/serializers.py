# -*- coding: utf-8 -*-
from backend.auth.accounts.models import AccountType
from backend.core.models import ResourceHost
from rest_framework import serializers
from backend.components.auditor.models import AuditorParameter
from backend.components.menu.models import Option, OptionType
from backend.core.generic_serializer import GenericQuerySerializer
      
class OptionSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(label='Código', help_text='showfield:ADD')
    accounttype_list = serializers.SerializerMethodField('get_accounttypes_str',label='Tipos de Cuenta',help_text='multiselect|large',required=False)

    def get_accounttypes_str(self, obj):
        result = None
        accounttype_list = obj.accounttype_list.all()
        if accounttype_list.count() > 0:
            result = ','.join([str(account_type.id) for account_type in accounttype_list ])
        return result
    
    def validate(self, data):
        is_update = False
        option = None
        if self.context['request']._request.method == 'PUT':
            is_update = True
        try:
            option = Option.objects.get(pk=data['id'])
        except Option.DoesNotExist:
            pass
            
        if is_update and option is None:
            raise serializers.ValidationError({'id': "No existe una opción creada con el Código"})
        if not is_update and option is not None:        
            raise serializers.ValidationError({'id': "Hay una opción creada con el Código"})
        return data

    def create(self, validated_data):
        request = self.context['request']
        account_types_id = []
        try:
            accounttype_list_str = self.initial_data['accounttype_list']
            if accounttype_list_str != '':
                account_types_id = accounttype_list_str.split(',')
        except KeyError:
            account_types_id = []
        validated_data['type'] = OptionType.objects.get(pk='EXTER')
        validated_data['target'] = '_blank'
        instance, instance_created = Option.objects.get_or_create(creation_username=request.user.username, **validated_data)
        accounttype_list = AccountType.objects.filter(pk__in=account_types_id)
        instance.accounttype_list.set(accounttype_list)
        AuditorParameter.registerEvent(request, 'menu', 'Option', validated_data, instance.id,
                                       AuditorParameter.CREATE,
                                       action='CREATE_OPTION')

        return instance

    def update(self, instance, validated_data):
        request = self.context['request']
        account_types_id = []
        try:
            accounttype_list_str = self.initial_data['accounttype_list']
            if accounttype_list_str != '':
                account_types_id = accounttype_list_str.split(',')
        except KeyError:
            account_types_id = []

        old_serializer = OptionSerializer(instance)
        serializer = super(OptionSerializer, self).update(instance, validated_data)
        instance.update_username = request.user.username
        instance.save()
        instance_data = OptionSerializer(instance)
        accounttype_list = AccountType.objects.filter(pk__in=account_types_id)
        instance.accounttype_list.set(accounttype_list)
        AuditorParameter.registerEvent(request, 'menu', 'Option', instance_data.data, instance.id, AuditorParameter.UPDATE, before_register_object=old_serializer.data, action='UPDATE_OPTION' )
        return serializer

    class Meta:
        app_name = 'menu'
        description = 'Solo se permite crear opciones externas (la url enruta a sitios externos)'
        model = Option
        fields = ("id", "name", "url", "accounttype_list", )
        fields_actions = {
                            'accounttype_list': { 'fill': { 'url': ResourceHost.getURL('BASE_URL') + '/accounts/accounttypefilter/', 
                                                    }
                                          },
                       }

class OptionTableSerializer(serializers.ModelSerializer):
    type = serializers.CharField(label="Tipo de Opción")

    class Meta:
        model = Option
        verbose_name = 'Opciones de Menú'
        fields = ("id", "name", "url", "parentid", "type", )
        search_fields = ['^name', '=parentid', '=id']
        ordering_fields = ('id', 'name',)
        filter_fields = ("type",)

class OptionQuerySerializer(GenericQuerySerializer):

    class Meta:
        model = Option
        verbose_name = 'Opciones de Menú'
        fields = ("id", "name", )
        search_fields = ['^name', '=parentid']

