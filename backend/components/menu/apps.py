# -*- coding: utf-8 -*-
from django.apps import AppConfig

class ComponentsConfig(AppConfig):
    name = 'backend.components.menu'
