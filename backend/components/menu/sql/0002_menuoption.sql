INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '200', 'Configurar Sistema', '', NULL, NULL, NULL, NULL );
INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '201', 'Configurar Menú', '', '/menu/optiontable/', NULL, NULL, '200' );
-- INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '202', 'Configurar DashBoard', '', '/dashboard/dashboardwidgettable/', NULL, NULL, '200' );
-- INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '204', 'Base de Datos de Errores', '', '/errors/errormessagestable/', NULL, NULL, '200' );

INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '100', 'Inicio', '', '/dashboard', NULL, NULL, NULL );

INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '1000', 'Admin', '', NULL, NULL, NULL, NULL);
INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '1002', 'Cuentas', '', 'accounts/accounttable/', NULL, NULL, '1000');
INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '1003', 'Copias de Respaldo', '', '/backup/backupfiletable/', NULL, NULL, '1000' );

INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '2000', 'Entidades', '', NULL, NULL, NULL, NULL);
INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '2001', 'Caja de compensación', '', 'entities/compensationentitytable/', NULL, NULL, '2000');
INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '2002', 'Entidades promotoras de salud (EPS)', '', 'entities/epstable/', NULL, NULL, '2000');
INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '2003', 'Parafiscales', '', 'entities/parafiscaltable/', NULL, NULL, '2000');
INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '2004', 'Impuestos', '', 'entities/taxestable/', NULL, NULL, '2000');
INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '2005', 'Administradora de riesgos laborales (ARL)', '', 'entities/arltable/', NULL, NULL, '2000');
INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '2006', 'Fondos de pensiones', '', 'entities/pensionfundtable/', NULL, NULL, '2000');
INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '2007', 'Bancos', '', 'entities/banktable/', NULL, NULL, '2000');

INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '3000', 'General', '', NULL, NULL, NULL, NULL);
INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '3001', 'Crear parametros', '', 'general/parameter/', NULL, NULL, '3000');

INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '4000', 'Conceptos de nómina', '', NULL, NULL, NULL, NULL);
INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '4001', 'Provisión', '', 'items/provisiontable/', NULL, NULL, '4000');
INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '4002', 'Items', '', 'items/itemtable/', NULL, NULL, '4000');

INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '5000', 'Empleados', '', NULL, NULL, NULL, NULL);
INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '5001', 'Tabla de empleados', '', 'payrolls/employeetable/', NULL, NULL, '5000');
INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '5002', 'Tabla de cargos', '', 'payrolls/positiontable/', NULL, NULL, '5000');

INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '6000', 'Nómina', '', NULL, NULL, NULL, NULL);
INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '6001', 'Lista de nómina', '', 'payrolls/payrolltable/', NULL, NULL, '6000');

INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '7000', 'Eventos', '', NULL, NULL, NULL, NULL);
INSERT INTO menu_option ( type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '7001', 'Lista de eventos', '', 'events/eventtable/', NULL, NULL, '7000');
INSERT INTO menu_option (  type_id, id, name, description, url, target, icon, parentid ) VALUES ('INTER', '10000', 'Acerca de', '', 'about/', NULL, NULL, NULL);



