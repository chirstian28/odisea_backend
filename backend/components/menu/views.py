# -*- coding: utf-8 -*-
from backend.core.permissions import IsAdminOrReadOnly, IsAdminAndReadOnly
from backend.components.menu.models import Option, Menu
from backend.components.menu.serializers import OptionSerializer, \
                                               OptionTableSerializer
from rest_framework.views import APIView
from backend.auth.accounts.models import Account
from rest_framework.response import Response
from backend.core.views import GenericModelViewSet, GenericTableViewSet
from backend.components.menu.filters import OptionTableFilter

class OptionViewSet(GenericModelViewSet):
    app_name = 'menu'
    model_name = 'Option'
    queryset = Option.objects.all()
    serializer_class = OptionSerializer
    permission_classes = (IsAdminOrReadOnly,)
    filter_licenser = False
    
class OptionTableViewSet(GenericTableViewSet):
    app_name = 'menu'
    model_name = 'Option'
    queryset = Option.objects.all()
    serializer_class = OptionTableSerializer
    permission_classes = (IsAdminAndReadOnly,)
    filter_class = OptionTableFilter
    filter_licenser = False
    
class MenuView(APIView):

    def get(self, request, format=None):
        account = Account.getAccount(request.user)
        data = Menu.getMenu(account)
        return Response(data)
