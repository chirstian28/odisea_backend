from enum import unique
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.db import models
from django.conf import settings
from django.core.files.base import ContentFile
import datetime
import hashlib

class Environment(models.Model):
    name = models.CharField(max_length=20,primary_key=True, verbose_name='Nombre')
    is_active = models.BooleanField(default=False, verbose_name='Está activo?')
    
    def save(self, *args, **kwargs):
        if self.is_active:
            Environment.objects.filter(is_active=True).update(is_active=False)
        super(Environment, self).save()
    
    def getEnvironmentName():
        try:
            environment = Environment.objects.get(is_active=True)
            environment_name = environment.name
        except:
            environment_name = settings.CONF_ENVIRONMENT
        return environment_name
    
    getEnvironmentName = staticmethod(getEnvironmentName)
        
    def __str__(self):
        return self.name
    
    class Meta:
        app_label = 'core'
        verbose_name = 'Entorno'
        verbose_name_plural = 'Entornos'

class Protocol(models.Model):
    name = models.CharField(max_length=20,primary_key=True, verbose_name='Nombre')
    
    def __str__(self):
        return self.name
    
    class Meta:
        app_label = 'core'
        verbose_name = 'Protocolo'
        verbose_name_plural = 'Protocolos'

class ResourceHost(models.Model):
    environment = models.ForeignKey('Environment', on_delete=models.PROTECT, verbose_name='Entorno')
    name = models.CharField(max_length=30, verbose_name='Nombre')
    host = models.CharField(max_length=255, verbose_name='Host')
    port = models.IntegerField(default=80, verbose_name='Puerto')
    protocol = models.ForeignKey('Protocol', on_delete=models.PROTECT, verbose_name='Protocolo')
    autosigned = models.BooleanField(verbose_name='El certificado es autofirmado?', default=True)

    def __str__(self):
        return self.name
    
    def getProtocol(name='BASE_URL'):
        environment_name = Environment.getEnvironmentName()

        try:
            resource_host = ResourceHost.objects.get(environment__name=environment_name, name=name)
            return resource_host.protocol.name
        except ResourceHost.DoesNotExist:
            resource_host = None

    getProtocol = staticmethod(getProtocol)
    
    def isAutosigned(name='BASE_URL'):
        environment_name = Environment.getEnvironmentName()

        try:
            resource_host = ResourceHost.objects.get(environment__name=environment_name, name=name)
            return resource_host.autosigned
        except ResourceHost.DoesNotExist:
            resource_host = None
        return False

    isAutosigned = staticmethod(isAutosigned)
    
    def getURL(name='BASE_URL'):
        environment_name = Environment.getEnvironmentName()

        try:
            resource_host = ResourceHost.objects.get(environment__name=environment_name, name=name)
        except ResourceHost.DoesNotExist:
            resource_host = None
        result_url = ''
        if resource_host is not None:
            result_url += resource_host.protocol.name
            result_url += '://'
            result_url += resource_host.host
            result_url += ':'
            result_url += str(resource_host.port)
        else:
            result_url += settings.PRODUCTION_BASE_URL
        return result_url
    
    getURL = staticmethod(getURL)
    
    def getBase():
        return ResourceHost.getURL('BASE_URL')
    
    getBase = staticmethod(getBase)
    
    def getEnvironment(account=None):
        data = {}
        data['NAME'] = Environment.getEnvironmentName()
        data['BASE'] = ResourceHost.getURL('BASE_URL') 
        data['LOGIN'] =  data['BASE'] + '/accounts/login/'
        data['MENU'] =  data['BASE'] + '/menu/query/'
        data['LOGOUT'] =  data['BASE'] + '/accounts/logout/'
        data['DASHBOARD'] =  data['BASE'] + '/dashboard/'
        
        if account is not None:
            data["LICENSER"] = account.licenser.name
            data["PROFILE"] = account.getProfile()
            data['CHANGEPASSWORD'] =  data['BASE'] + '/accounts/changepassword/'
            data['TOKENREFRESH'] =  data['BASE'] + '/accounts/tokenrefresh/'
            data['TOKENVERIFY'] =  data['BASE'] + '/accounts/tokenverify/' 
        return data
    
    getEnvironment = staticmethod(getEnvironment)
    
    class Meta:
        unique_together = [['environment', 'name']]
        app_label = 'core'
        verbose_name = 'Resource Host'
        verbose_name_plural = 'Resources Hosts'

class ModuleInstalled(models.Model):
    name = models.CharField(max_length=100, verbose_name='Nombre')
    filename = models.CharField(max_length=100, verbose_name='Nombre Archivo')

    def __str__(self):
        return self.name
    
    class Meta:
        app_label = 'core'
        verbose_name = 'Módulo Instalado'
        verbose_name_plural = 'Módulos Instalados'

class TemporalData(models.Model):
    account = models.ForeignKey('accounts.Account', on_delete=models.CASCADE)
    app_name = models.CharField(max_length=255)
    model_name = models.CharField(max_length=255)
    tmpid = models.CharField(max_length=100, unique=True)
    data = models.JSONField()
    objectid = models.IntegerField(null=True, blank=True)
    data_table = models.JSONField(null=True, blank=True)
    delete_registers = models.JSONField(null=True, blank=True)
    creation_date_time = models.DateTimeField(verbose_name='Fecha creación', auto_now_add=True)

    def __str__(self):
        return f'{self.tmpid}'

    @staticmethod
    def generateKey(actual_account, app_name, model_name):
        now = datetime.datetime.now()
        return hashlib.new("sha1", f'{app_name}_{model_name}_{actual_account.username}_{now}'.encode() ).hexdigest()


    @staticmethod
    def getRegisterData(process_name, actual_account, object_id, data=None):

        error = None
        if data is None:
            data = {}
        
        if object_id is not None:
            object_id_components = object_id.split('_')
            if len(object_id_components) == 2:
                temporal_data_id = object_id_components[0]
                try:
                    temporal_data = TemporalData.objects.get(account=actual_account, pk=temporal_data_id)
                except TemporalData.DoesNotExist:
                    error = 'No existe el proceso temporal'
                
                if error is None:
                    if object_id in temporal_data.data[process_name].keys():
                        temporal_object = temporal_data.data[process_name][object_id]
                        for key, value in temporal_object.items():
                            data[key] = value
                    else:
                        error = 'No Existe el registro temporal'

        return data, error 

    @staticmethod
    def deleteRegister(process_name, actual_account, object_id):
        error = None
        
        if object_id is not None:
            object_id_components = object_id.split('_')
            if len(object_id_components) == 2:
                temporal_data_id = object_id_components[0]
                try:
                    temporal_data = TemporalData.objects.get(account=actual_account, pk=temporal_data_id)
                except TemporalData.DoesNotExist:
                    error = 'No existe el proceso temporal'
                if error is None:
                    data_copy = temporal_data.data[process_name].copy()
                    data_table_copy = temporal_data.data_table[process_name].copy()

                    if object_id in data_copy.keys():
                        if 'objectid' not in data_copy[object_id].keys():
                            del data_copy[object_id]
                        else:
                            data_copy[object_id]['_DELETED'] = True
                    else:
                        error = 'No Existe el registro temporal'

                if error is None:
                    if object_id in data_table_copy.keys():
                        del data_table_copy[object_id]

                    temporal_data.data[process_name] = data_copy
                    temporal_data.data_table[process_name] = data_table_copy
                    temporal_data.save()

                    print (f'{object_id} Eliminado')

        return error

    def createRegister(self, process_name, actual_account, serializer_data, request_data, fields):
        from backend.components.filemanager.models import TemporalFile

        error = None 
        data = {}

        object_dict = {}
        object_dict['_UPDATED'] = True
        for field in fields:
            if field in serializer_data.keys() and serializer_data[field] != None:
                object_dict[field] = serializer_data[field]
            elif field in request_data.keys() and request_data[field] is not None:
                object_data = request_data[field]
                if type(object_data) == InMemoryUploadedFile:
                    temporal_file = TemporalFile()
                    if 'name' in request_data.keys():
                        temporal_file.name = object_dict['name']
                    else:
                        temporal_file.name = 'TEMPORAL'
                    temporal_file.username = actual_account.username
                    temporal_file.segments_quantity = 1
                    temporal_file.save()
                    temporal_file.data_file.save(object_data.name, ContentFile(object_data.read()))
                    object_dict[field] = temporal_file.id 
            else:
                object_dict[field] = None
        try:
            object_dict["id"] = f"{self.id}_{self.data[process_name]['_next_index']}"
            if process_name not in self.data.keys():
                self.data[process_name] = {}
                self.data[process_name]['_next_index'] = 0
            self.data[process_name][object_dict["id"]] = object_dict
            self.data[process_name]['_next_index'] += 1
            self.save()
        except:
            error = 'Objeto Temporal errado'

        return data, error

    @staticmethod
    def updateRegister(process_name, actual_account, object_id, register_data, fields):
        error = None
        
        object_id_components = object_id.split('_')
        if len(object_id_components) == 2:
            temporal_data_id = object_id_components[0]
            try:
                temporal_data = TemporalData.objects.get(account=actual_account, pk=temporal_data_id)
            except TemporalData.DoesNotExist:
                error = 'No existe el proceso temporal'

        if error is None:
            data_copy = temporal_data.data[process_name].copy()
            if object_id in data_copy.keys():
                update_element = data_copy[object_id].copy()
                update_element['_UPDATED'] = True
                del data_copy[object_id]
                for field in fields:
                    if field in register_data.keys():
                        update_element[field] = register_data[field] 
                    else:
                        update_element[field] = None
                temporal_data.data[process_name][object_id] = update_element
                temporal_data.save()
            else:
                error = 'No Existe el registro temporal'
        return error

    class Meta:
        ordering = ['-creation_date_time', 'tmpid']
        unique_together = [['account', 'tmpid']]
        app_label = 'core'
        verbose_name = 'Dato Temporal'
        verbose_name_plural = 'Tabla de datos Temporales'
