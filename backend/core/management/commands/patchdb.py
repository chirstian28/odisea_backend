from django.core.management.base import BaseCommand
from django.db import connection
from django.conf import settings

class Command(BaseCommand):

    def handle(self, *args, **options):
        base_path = settings.BASE_DIR + '/scripts/sql/patchdb.sql'
         
        fp = open(base_path, 'r')
        lines = fp.read()
        lines = lines.replace('\n', ' ')
        try:
            cursor = connection.cursor()
            cursor.execute(lines)
        except Exception as e:
            print ( str(e) )
                
