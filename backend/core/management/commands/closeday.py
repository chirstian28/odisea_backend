# -*- coding:utf-8 -*-
from django.core.management.base import BaseCommand
from django.conf import settings
import logging
import datetime
from backend.auth.license.models import Licenser

class Command(BaseCommand):

    def handle(self, *args, **options):
        date = datetime.date.today()
        
        licenser_list = Licenser.objects.all()
        for licenser in licenser_list:
            log_filename = settings.LOG_ROOT 
            log_filename += '/close_day_'
            log_filename += settings.APP_NAME
            log_filename += '_'
            log_filename += licenser.id
            log_filename += '_'
            log_filename += date.strftime('%Y%m%d')
            log_filename += '.txt'
            
            Command.process(licenser, log_filename, date)
    
            try:
                licenser.sendLogEmail(log_filename, "CloseDay", date)
            except Exception as e:
                print ("Exception: " + str(e))
                print ("Error Send Mail Close Day...")

    def process(licenser, log_filename, date):
        logger = logging.getLogger('closeDay')
        hdlr = logging.FileHandler(log_filename)
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        hdlr.setFormatter(formatter)
        logger.addHandler(hdlr)
        logger.setLevel(logging.DEBUG)
        logger.info('----------------------------')
        logger.info("Begin Close Day...")
        try:
            actual_step = 1
            actual_step += 1
            logger.info( "End Close Day...")
        except Exception as e:
            logger.error("Error Close Day...")
            logger.error("Exception: " + str(e) + "")
        logger.removeHandler(hdlr)
        hdlr.close()
    
    process = staticmethod(process)
