import os
import itertools

from django.db import migrations

from django.conf import settings
from django.db import connection

from django.core.management.base import BaseCommand
from backend.core.models import ModuleInstalled


import re

class Command(BaseCommand):

    def add_arguments(self, parser):

        parser.add_argument('--ignore', nargs='+', type=str)
        parser.add_argument('--fake', nargs='+', type=str)
        parser.add_argument('--force', nargs='+', type=str)
        parser.add_argument('--apps', nargs='+', type=str)
        parser.add_argument('--filenumber', type=str)

    def handle(self, *args, **options):
        SQL_FOLDER = "/sql/"
        
        app_list = settings.PROJECT_COMPONENTS
        
        #import pdb; pdb.set_trace()

        if 'apps' in options and options['apps'] is not None:
            app_list = ['backend.' + app for app in options['apps'] ]

        for app in app_list:
            if re.match("backend\.\w+", app):
                app_path = '/'.join(app.split('.')[1:3])
                app_name = app_path.replace('/', '_')
                if 'ignore' in options.keys() and options['ignore'] is not None \
                                              and app_name in options['ignore']:
                    continue
                
                path_directory = os.path.join(settings.BASE_DIR, app_path + SQL_FOLDER)
                
                if os.path.isdir(path_directory):
                    has_error = False
                    print ("Instalando el módulo %s ..." % app_name)
                    sql_file_list = sorted([path_directory + file for file in os.listdir(path_directory) if file.lower().endswith('.sql')])
                    
                    for sql_file in sql_file_list:
                        start_installation = True
                        sql_filename = sql_file.split('/')[-1]
                        if 'filenumber' in options.keys() and options['filenumber'] is not None:
                            number = sql_filename.split('_')[0]
                            if number < options['filenumber']:
                                start_installation = False

                        try:
                            module_installed = ModuleInstalled.objects.get(name=app_name, filename=sql_filename)
                        except ModuleInstalled.DoesNotExist:
                            module_installed = None

                        if module_installed is not None:
                            start_installation = False
                            if 'force' in options.keys() and options['force'] is not None \
                                and app_name in options['force']:
                                start_installation = True
                            
                        if 'fake' in options.keys() and options['fake'] is not None \
                                                    and app_name in options['fake']:
                            start_installation = False

                        if start_installation:
                            filename = ''
                            with open(sql_file, 'r') as f:
                                lines = f.read()
                                try:
                                    cursor = connection.cursor()
                                    cursor.execute(lines)
                                except Exception as e:
                                    print ("No se ha Instalado el módulo %s correctamente. " % app_name)
                                    print (f"Se presentó una excepción en {app_name}, {sql_filename}: {e}")
                                    return False
                    
                        if module_installed is None:
                            module_installed = ModuleInstalled()
                            module_installed.name = app_name
                            module_installed.filename = sql_filename
                            module_installed.save()

                    print ("Instalado el módulo %s. " % app_name)
