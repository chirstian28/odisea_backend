from django.core.management.base import BaseCommand
from django.db import connection
from django.conf import settings
from backend.auth.license.models import ModuleInstalled

class Command(BaseCommand):

    def handle(self, *args, **options):
        base_path = settings.BASE_DIR + '/scripts/sql/'
         
        module_name_list = [    
                            'datemanager.sql',
                            'localization.sql',
                            'license.sql',
                            'auditor.sql',
                            'accounts.sql',
                            'menu.sql',
                            'dashboard.sql',
                            'controlpanel.sql',
                            'workflow.sql',
                            #'dataio.sql',
                            #'collect.sql',
                            #'base.sql',
                            #'parameters.sql',
                            #'operative.sql',
        ]

        for module_name in module_name_list:
            print ("Instalando el módulo %s...." % module_name)
            try:
                module_installed = ModuleInstalled.objects.get(name=module_name)
            except ModuleInstalled.DoesNotExist:
                fp = open(base_path + module_name, 'r')
                lines = fp.read()
                lines = lines.replace('\n', ' ')
                try:
                    cursor = connection.cursor()
                    cursor.execute(lines)
                except Exception as e:
                    print (str(e))
                
                module_installed = ModuleInstalled()
                module_installed.name = module_name
                module_installed.save()

                print ("Instalado el módulo %s. " % module_name)
                


