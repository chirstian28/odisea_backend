from django.urls import path, include
from backend.core import views
from django.conf import settings
from rest_framework import routers
from django.conf.urls.static import static

router = routers.DefaultRouter()
        
router.register(r'modulesinstalled', views.ModuleInstalledViewSet)

urlpatterns = [
    path('', views.BaseView.as_view()),
    path('dashboard/', views.DashboardView.as_view()),
    path('scheme/', views.SchemeView.as_view()),
    path('about/', views.AboutView.as_view()),
    path('closeday/', views.CloseDayView.as_view()),
    path('initday/', views.InitDayView.as_view()),
    path('health/', include('health_check.urls')),
    path('license/', include('backend.auth.license.urls')),
    path('localization/', include('backend.library.localization.urls')),
    path('datemanager/', include('backend.library.datemanager.urls')),
    path('auditor/', include('backend.components.auditor.urls')),
    path('accounts/', include('backend.auth.accounts.urls')),
    path('backup/', include('backend.components.backup.urls')),
    path('menu/', include('backend.components.menu.urls')),
    path('pushmessages/', include('backend.components.pushmessages.urls')),
    path('errors/', include('backend.components.errors.urls')),
] 

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static('favicon.ico', document_root=settings.FAVICON_ROOT)

urlpatterns += router.urls
