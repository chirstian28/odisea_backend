from django.urls import include, path
from backend.core.base_urls import urlpatterns
from backend.core import views

urlpatterns += [
    path('', views.BaseView.as_view()),
    path('employees/', include('backend.apps.employees.urls')),
    path('entities/', include('backend.apps.entities.urls')),
    path('events/', include('backend.apps.events.urls')),
    path('payrolls/', include('backend.apps.payroll.urls')),
    path('items/', include('backend.apps.items.urls')),
    path('general/', include('backend.apps.general.urls')),
] 

