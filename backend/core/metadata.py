from rest_framework.metadata import BaseMetadata
from django.conf import settings
from backend.auth.accounts.models import Account
from backend.core.models import ResourceHost


class GenericTotalField:
    
    def getFieldData(request, field_name, fields_actions, fields_buttons, serializer_class, class_model, order, container_name=None, component_name=None):
        field_dict = None
        
        field_action = None
        field_buttons = None
        if field_name in fields_actions.keys():
            field_action = fields_actions[field_name]
        if field_name in fields_buttons.keys():
            field_buttons = fields_buttons[field_name]
                        
        field = None
        field_model = None 
        verbose_name = field_name.capitalize()  
        
        try:
            if class_model is not None:
                field_model = class_model._meta.get_field(field_name)
        except:
            pass
        
        try:
            field = serializer_class._declared_fields[field_name]
            if field.label is not None:
                verbose_name = field.label.capitalize()
        except:
            pass
        
        if field is None and field_model is not None:
            try:
                verbose_name = field_model.verbose_name.capitalize()
                field = field_model
            except:
                pass

        if field is not None:
            field_dict = GenericTotalField.generate_field_parameters(request, field, field_name, verbose_name, order, field_action, container_name=container_name, component_name=component_name)
        return field_dict
    
    getFieldData = staticmethod(getFieldData)
    
    def getFilterData(request, field_name, fields_actions, serializer_class, class_model, order):
        field_dict = None
        
        field_action = None
        if field_name in fields_actions.keys():
            field_action = fields_actions[field_name]
                        
        field = None
        verbose_name = field_name.capitalize()  
        try:
            if class_model is not None:
                field = class_model._meta.get_field(field_name)
                verbose_name = field.verbose_name.capitalize()
        except:
            pass
        
        if field is None:
            try:
                field = serializer_class._declared_fields[field_name]
                if field.label is not None:
                    verbose_name = field.label.capitalize()
            except:
                pass

        if field is not None:
            field_dict = GenericTotalField.generate_field_parameters(request, field, field_name, verbose_name, order, field_action, isfilter=True)

        return field_dict
    
    getFilterData = staticmethod(getFilterData)
    
    def getFieldDescription(request, serializer_class, container_name=None, component_name=None, is_table=False, allreadonly=False):
        meta_data_dict = { }
        meta_data_dict['fields'] = []
        try:
            class_model = serializer_class.Meta.model
        except:
            class_model = None

        try:
            ordering_model = class_model._meta.ordering
        except:
            ordering_model = []

        try:
            field_name_list = serializer_class.Meta.fields
        except:
            field_name_list = []
        try:
            fields_actions = serializer_class.Meta.fields_actions
        except:
            fields_actions = {}
        try:
            fields_buttons = serializer_class.Meta.fields_buttons
        except:
            fields_buttons = {}        
        try:
            total_name_list = serializer_class.Meta.total_fields
        except:
            total_name_list = []
        try:
            filter_name_list = serializer_class.Meta.filter_fields
        except:
            filter_name_list = []
        try:
            ordering_fields = serializer_class.Meta.ordering_fields
        except:
            ordering_fields = []

        field_dict_list = []
        total_dict_list = []
        
        order = 0
        for field_name in field_name_list:
            field_dict = GenericTotalField.getFieldData(request, field_name, fields_actions, fields_buttons, serializer_class, class_model, order, container_name=container_name, component_name=component_name)
            if field_dict is not None:
                if field_name in filter_name_list:
                    field_dict['iconName'] = 'GroupedDescending'
                if field_name in ordering_fields:
                    field_dict['canSort'] = True
                if field_name in ordering_model:
                    field_dict['isSorted'] = True
                if '-' + field_name in ordering_model:
                    field_dict['isSorted'] = True
                    field_dict['isSortedDescending'] = True
                if allreadonly:
                    field_dict['readonly'] = True
                    
                field_dict_list.append(field_dict)

                if field_name in total_name_list:
                    total_dict = field_dict.copy()
                    total_dict_list.append(total_dict)

            order += 1
        meta_data_dict['fields'] = field_dict_list
        
        if is_table:
            filter_dict_list = []
            order = 0
            meta_data_dict['total'] = []
            meta_data_dict['filters'] = []
            meta_data_dict['search'] = False
            try:
                search_name_list = serializer_class.Meta.search_fields
            except:
                search_name_list = []

            for field_name in filter_name_list:
                filter_dict = GenericTotalField.getFilterData(request, field_name, fields_actions, serializer_class, class_model, order)
                filter_dict['required'] = False
                if filter_dict is not None:
                    filter_dict_list.append(filter_dict)
                order += 1
            
            meta_data_dict['filters'] = filter_dict_list
            if len(search_name_list) > 0:
                meta_data_dict['search'] = True
            meta_data_dict['total'] = total_dict_list
            
        return meta_data_dict

    getFieldDescription = staticmethod(getFieldDescription)
        
    def generate_field_parameters(request, field, field_name, verbose_name, order, field_action, isfilter=False, container_name=None, component_name=None):
        
        actual_account = Account.getAccount(request.user)

        key = ''
        if container_name is not None and container_name != '':
            key += f'{container_name}_'
        if component_name is not None and component_name != '':
            key += f'{component_name}_'
        if key == '':
            key = 'key'

        field_dict = {
            'key': f"{key}_{order}",
            'id': field_name,
            'name': verbose_name,
            'fieldName': field_name,
            'isSorted': False,
            'isSortedDescending': False,
            'isRowHeader': False,
            'isResizable': True,
            'data': 'any',
            'size': 'normal',
            'rows': 1,
            'readonly': False,
            'required': False,
            'canSort': False,
            }   
        null_verbose = '------'
        ispassword = False
        multiselect = False
        tabledict = False
        tablerow = False
        quantity_rows = 1
        can_null = False
        filegroup = False
        try:
            can_null = field.null
        except:
            pass


        try:
            field_dict['readonly'] = not field.editable
        except:
            pass

        try:
            field_dict['readonly'] = field.read_only
        except:
            pass
        
        try:
            field_dict['required'] = field.required
        except:
            pass

        try:
            field_dict['required'] = not field.null
        except:
            pass
        
        scape_tuple = { '<two_points>': ('\\:', ':'),
                        '<vertical_bar>': ('\\|', '|')

        }

        help_text_var_list = []
        if field.help_text is not None:
            for scape in scape_tuple.keys():
                field.help_text = field.help_text.replace(scape_tuple[scape][0], scape)
            help_text_var_list = field.help_text.split('|') 
        # tiny: 0.25X 
        # small: 0.5X
        # normal: X (160)
        # large: 2X
        # xlarge: 400
        # xxlarge: 500
        # xxxlarge: 600
        # huge: 700
        for help_text_var in help_text_var_list:
            if help_text_var in ('tiny','small','large','xlarge','xxlarge','xxxlarge','huge'):
                field_dict['size'] = help_text_var
            if help_text_var == 'sorted':
                field_dict['isSorted'] = True
                field_dict['sortAscendingAriaLabel'] = 'Ord. Ascendente.'
            if help_text_var == 'descsorted':
                field_dict['isSorted'] = True
                field_dict['isSortedDescending'] = True
                field_dict['sortAscendingAriaLabel'] = 'Ord. Descendente'
            if help_text_var == 'cansort':
                field_dict['canSort'] = True
            if help_text_var == 'required':
                field_dict['required'] = True
                
                
            if help_text_var == 'isid':
                field_dict['isRowHeader'] = True
            if help_text_var == 'noresizable':
                field_dict['isResizable'] = False
            if help_text_var == 'password':
                ispassword = True
            if help_text_var == 'noupdate':
                field_dict['noUpdate'] = True
            if 'null' in help_text_var:
                null_tuple = help_text_var.split(':')
                try:
                    null_verbose = null_tuple[1]
                except:
                    pass
            if help_text_var == 'readonly':
                field_dict['readonly'] = True
            if help_text_var == 'multiselect':
                multiselect = True
            if help_text_var == 'tabledict':
                tabledict = True
            if help_text_var == 'tablerow':
                tablerow = True
            if help_text_var == 'filegroup':
                filegroup = True
            if 'help' in help_text_var:
                help_tuple = help_text_var.split(':')
                try:
                    field_dict['help'] = help_tuple[1]
                    for scape in scape_tuple.keys():
                        field_dict['help'] = field_dict['help'].replace(scape, scape_tuple[scape][1])
                except:
                    pass
            if 'description' in help_text_var:
                description_tuple = help_text_var.split(':')
                try:
                    field_dict['description'] = description_tuple[1]
                    for scape in scape_tuple.keys():
                        field_dict['description'] = field_dict['description'].replace(scape, scape_tuple[scape][1])
                except:
                    pass
            if 'rows' in help_text_var:
                rows_tuple = help_text_var.split(':')
                try:
                    quantity_rows = int(rows_tuple[1])
                except:
                    pass
            if 'file_format' in help_text_var:
                file_format_tuple = help_text_var.split(':')
                try:
                    field_dict['file_format'] = file_format_tuple[1]
                except:
                    pass
            if 'hidden' in help_text_var:
                field_dict['hidden'] = True
            if 'title' in help_text_var:
                title_tuple = help_text_var.split(':')
                try:
                    field_dict['title'] = title_tuple[1].split(',')
                except:
                    pass

        if tablerow:
            field_dict['data'] = 'table'
            field_dict['child'] = GenericTotalField.getFieldDescription(request, field.child, component_name=field_name)
            base_url = None
            if 'element_url' in field_action:
                base_url = field_action['element_url']
            base_backurl = None
            if 'table_backurl' in field_action:
                base_backurl = field_action['table_backurl']

            field_dict['buttons'] = {}

            operation_list = ('ADD', 'UPDATE', 'DELETE', 'QUERY',)
            for operation in operation_list:
                disable = False
                operation_url = base_url
                operation_backurl = base_backurl
                if operation in field_action and field_action[operation]:
                    field_action_operation = field_action[operation].copy()
                    if 'disable' in field_action_operation and field_action_operation['disable']:
                        disable = True
                    if 'url' in field_action_operation and field_action_operation['url']:
                        operation_url = field_action_operation['url']
                    if 'backurl' in field_action_operation and field_action_operation['backurl']:
                        operation_backurl = field_action_operation['backurl']

                if not disable:
                    field_dict['buttons'][operation] = { 
                                                            'kind': operation,
                                                            'url': operation_url,
                                                            'disable': False,
                                                            'default': False,
                                                            'backurl': operation_backurl, 
                                                    }   
                    if operation == 'ADD':
                        field_dict['buttons'][operation]['order'] = 1
                        field_dict['buttons'][operation]['title'] = 'Adicionar'
                        field_dict['buttons'][operation]['icon'] = operation.lower()
                        field_dict['buttons'][operation]['method'] = 'POST'
                    elif operation == 'QUERY':
                        field_dict['buttons'][operation]['order'] = 2
                        field_dict['buttons'][operation]['title'] = 'Ver'
                        field_dict['buttons'][operation]['icon'] = 'view'
                        field_dict['buttons'][operation]['method'] = 'GET'
                    elif operation == 'UPDATE':
                        field_dict['buttons'][operation]['order'] = 3
                        field_dict['buttons'][operation]['title'] = 'Editar'
                        field_dict['buttons'][operation]['icon'] = 'edit'
                        field_dict['buttons'][operation]['method'] = 'PUT'
                    elif operation == 'DELETE':
                        field_dict['buttons'][operation]['order'] = 4
                        field_dict['buttons'][operation]['title'] = 'Eliminar'
                        field_dict['buttons'][operation]['icon'] = operation.lower()
                        field_dict['buttons'][operation]['method'] = 'DELETE'
                    
        elif field.__class__.__name__ ==  'AutoField':
            field_dict['data'] = 'number'
            field_dict['readonly'] = True
            field_dict['required'] = False
        elif field.__class__.__name__ == 'TextField':
            field_dict['data'] = 'multiline'
            field_dict['size'] = 'large'
            field_dict['rows'] = 3
        elif field.__class__.__name__  == 'CharField':
            if ispassword:
                field_dict['data'] = 'password'
            elif multiselect:
                field_dict['data'] = 'multiselect'
            elif filegroup:
                field_dict['data'] = 'filegroup'
            else:
                field_dict['data'] = 'string'

            if field_dict['size'] == 'huge':
                field_dict['data'] = 'multiline'
                field_dict['size'] = 'large'
                field_dict['rows'] = 3
        elif field.__class__.__name__  == 'SerializerMethodField':
            field_dict['data'] = 'string'
            if multiselect:
                field_dict['data'] = 'multiselect'
            if tabledict:
                field_dict['data'] = 'tabledict'
        elif field.__class__.__name__  == 'EmailField':
            field_dict['data'] = 'email'
        elif field.__class__.__name__ in  ('IntegerField', 'DecimalField'):
            field_dict['data'] = 'number'
        elif field.__class__.__name__  == 'DateField':
            field_dict['data'] = 'date'
        elif field.__class__.__name__  == 'TimeField':
            field_dict['data'] = 'time'
        elif field.__class__.__name__  == 'DateTimeField':
            field_dict['data'] = 'datetime'
        elif field.__class__.__name__  == 'BooleanField':
            if can_null:
                field_dict['data'] = 'select'
                field_dict['options'] = [
                    {'id': '', 'name': '------'},
                    {'id': True, 'name': 'Sí'},
                    {'id': False, 'name': 'No'},
                ]
            else:
                field_dict['data'] = 'bool'
                field_dict['required'] = False
        elif field.__class__.__name__  == 'JSONField':
            field_dict['data'] = 'tabledict'

        elif field.__class__.__name__  in ('ForeignKey','OneToOneRel','OneToOneField'):
            field_dict['data'] = 'select'
            if field_action is None or 'fill' not in field_action:
                field_dict['options'] = []
                if not isfilter:
                    field_dict['options'].append({'id': '', 'name': null_verbose})
                option_list = field.related_model.objects.all()
                if settings.FILTER_LICENSER and actual_account is not None:
                    try:
                        option_list = option_list.filter(licenser=actual_account.licenser)
                    except:
                        pass
                for option in option_list:
                    option_dict = {}
                    option_dict['id'] = option.id
                    option_dict['name'] = str(option)
                    field_dict['options'].append(option_dict)

        elif field.__class__.__name__  == 'ChoiceField':
            field_dict['data'] = 'select'
            if field_action is None or 'fill' not in field_action:
                field_dict['options'] = []
                if not isfilter:
                    field_dict['options'].append({'id': '', 'name': null_verbose})
                for key in field.choices:
                    option_dict = {}
                    option_dict['id'] = key
                    option_dict['name'] = field.choices[key]
                    field_dict['options'].append(option_dict)
        elif field.__class__.__name__  == 'NullBooleanField':
            field_dict['data'] = 'select'
            field_dict['options'] = [
                {'id': '', 'name': '------'},
                {'id': True, 'name': 'Verdadero'},
                {'id': False, 'name': 'Falso'},
            ]
        elif field.__class__.__name__  == 'ManyToManyField':
            field_dict['data'] = 'multiple'
            if field_action is None or 'fill' not in field_action:
                field_dict['options'] = []
                option_list = field.related_model.objects.all()
                if settings.FILTER_LICENSER and actual_account is not None:
                    try:
                        option_list = option_list.filter(licenser=actual_account.licenser)
                    except:
                        pass
                for option in option_list:
                    option_dict = {}
                    option_dict['id'] = option.id
                    option_dict['name'] = str(option)
                    field_dict['options'].append(option_dict)
        elif field.__class__.__name__ == 'MultipleChoiceField':
            field_dict['data'] = 'multiple'
            if field_action is None or 'fill' not in field_action:
                field_dict['options'] = []
                for key in field.choices:
                    option_dict = {}
                    option_dict['id'] = key
                    option_dict['name'] = field.choices[key]
                    field_dict['options'].append(option_dict)
        elif field.__class__.__name__  == 'FileField':
            field_dict['data'] = 'file'

        if quantity_rows > field_dict['rows']:
            field_dict['rows'] = quantity_rows

        if field_action is not None:
            field_dict.update(field_action)

        return field_dict

    generate_field_parameters = staticmethod(generate_field_parameters)

class GenericTableMetadata(BaseMetadata):
    """
    Don't include field and other information for `OPTIONS` request.
    Just return the name and description.
    """

    def determine_metadata(self, request, view):
        try:
            serializer_class = view.serializer_class
            class_model = serializer_class.Meta.model

            meta_data_dict = GenericTotalField.getFieldDescription(request, serializer_class, is_table=True)
            meta_data_dict['type'] = 'TABLE'
            meta_data_dict['title'] = class_model._meta.verbose_name_plural
            meta_data_dict['name'] = class_model._meta.verbose_name
            try:
                meta_data_dict['description'] = serializer_class.Meta.description
            except:
                meta_data_dict['description'] = ''
            
            modelurl = '/' + class_model._meta.app_label + '/' + class_model.__name__.lower() + '/'
            actualurl = '/' + class_model._meta.app_label + '/' + class_model.__name__.lower() + 'table/'
            
            meta_data_dict['actions'] = { 
                                            'BASIC': { 
                                                'UPDATE': {
                                                            'order': 3,
                                                            'title': 'Editar',
                                                            'kind': 'UPDATE',
                                                            'url': modelurl,
                                                            'disable': False,
                                                            'icon': 'edit',
                                                            'backurl': actualurl,
                                                            'method': 'PUT',
                                                    },
                                                'DELETE': {
                                                            'order': 4,
                                                            'title': 'Eliminar',
                                                            'kind': 'DELETE',
                                                            'url': modelurl,
                                                            'disable': False,
                                                            'icon': 'delete',
                                                            'backurl': actualurl,
                                                            'method': 'DELETE',
                                                    },
                                                'QUERY': {
                                                            'order': 2,
                                                            'title': 'Ver',
                                                            'kind': 'QUERY',
                                                            'url': modelurl,
                                                            'disable': False,
                                                            'icon': 'view',
                                                            'backurl': actualurl,
                                                            'method': 'GET',
                                                    },
                                            }
                                        }

            try:
                meta_data_dict['actions']['BASIC'].update(serializer_class.Meta.basic_actions)
            except:
                pass
            try:
                meta_data_dict['actions']['EXTRA'] = serializer_class.Meta.extra_actions
            except:
                meta_data_dict['actions']['EXTRA'] = []

            meta_data_dict['buttons'] = {
                'ADD': {  'order': 1,
                          'title': 'Crear',
                          'kind': 'ADD',
                          'url': modelurl,
                          'disable': False,
                          'icon': 'add',
                          'backurl': actualurl,
                          'default': True,
                          'method': 'GET',
                    },
                }
                
        except:
            meta_data_dict = {}
        return meta_data_dict

class GenericMetadata(BaseMetadata):
    """
    Don't include field and other information for `OPTIONS` request.
    Just return the name and description.
    """

    def determine_metadata(self, request, view):
        serializer_class = view.serializer_class
        meta_data_dict = GenericTotalField.getFieldDescription(request, serializer_class)
        meta_data_dict['type'] = 'FORM'
        meta_data_dict['name'] = serializer_class.__name__.lower()
        meta_data_dict['default_add'] = None
        meta_data_dict['default_add_id'] = False
        try:
            meta_data_dict['description'] = serializer_class.Meta.description
        except:
            meta_data_dict['description'] = ''
        try:
            meta_data_dict['title'] = serializer_class.Meta.verbose_name
        except:
            meta_data_dict['title'] = meta_data_dict['name']

        actualurl = request._request.path
        backurl = '/dashboard'
        try:
            class_model = serializer_class.Meta.model
            meta_data_dict['modelname'] = class_model.__name__.lower()
            meta_data_dict['title'] = class_model._meta.verbose_name.title()
            actualurl = '/' + class_model._meta.app_label + '/' + class_model.__name__.lower() + '/'
            backurl = '/' + class_model._meta.app_label + '/' + class_model.__name__.lower() + 'table/'
            if view.kwargs is not None and 'pk' in view.kwargs:
                try:
                    meta_data_dict['title'] += ' - ' + str(class_model.objects.get(pk=view.kwargs['pk']))
                except:
                    pass
                
        except:
            class_model = None

        meta_data_dict['buttons'] = {
                'SAVE': { 'order': 1,
                          'title': 'Guardar',
                          'kind': 'SAVE',
                          'url': actualurl,
                          'disable': False,
                          'icon': 'save',
                          'backurl': backurl,
                          'default': True,
                          'method': 'POST',
                    },
                'EXIT': { 'order': 2,
                          'title': 'Salir',
                          'kind': 'EXIT',
                          'url': backurl,
                          'disable': False,
                          'icon': 'back',
                          'default': False,
                          'method': 'GET',
                    },
                }

        return meta_data_dict

class GenericQueryMetadata(BaseMetadata):

    def determine_metadata(self, request, view):
        try:
            serializer_class = view.serializer_class
            class_model = serializer_class.Meta.model
            meta_data_dict = GenericTotalField.getFieldDescription(request, serializer_class, is_table=True)
            meta_data_dict['modelname'] = class_model.__name__.lower()
            meta_data_dict['name'] = class_model._meta.verbose_name.title()
            meta_data_dict['title'] = class_model._meta.verbose_name_plural.title()
            meta_data_dict['type'] = 'QUERY'
        except:
            meta_data_dict = {}
        return meta_data_dict


class GenericPanelsMetadata(BaseMetadata):

    def determine_metadata(self, request, view):
        serializer_class = view.serializer_class
        meta_data_dict = GenericTotalField.getFieldDescription(request, serializer_class)
        meta_data_dict['type'] = 'PANELS'
        meta_data_dict['title'] = serializer_class.Meta.label
        meta_data_dict['columns'] = 2
        return meta_data_dict   

class GenericTabsMetadata(BaseMetadata):

    def determine_metadata(self, request, view):
        serializer_class = view.serializer_class
        url = ResourceHost.getURL('BASE_URL') + request._request.path
        meta_data_dict = {}
        meta_data_dict['base_url'] = url
        meta_data_dict['type'] = 'TABS'
        meta_data_dict['title'] = serializer_class.Meta.label

        try:
            tab_list = serializer_class.Meta.fields
        except:
            tab_list = []

        meta_data_dict['tab_list'] = []
        for field_name in tab_list:

            field = None
            verbose_name = field_name.capitalize()

            try:
                field = serializer_class._declared_fields[field_name]
                if field.label is not None:
                    verbose_name = field.label.capitalize()
            except Exception as e:
                pass

            try:
                readonly = field.read_only
            except Exception as e:
                readonly = False

            tab_info = GenericTotalField.getFieldDescription(request, field, container_name=field_name, allreadonly=readonly)
            tab_info['name'] = field_name
            tab_info['title'] = verbose_name
            tab_info['readonly'] = readonly

            help_text_var_list = []
            if field.help_text is not None:
                help_text_var_list = field.help_text.split('|') 
            for help_text_var in help_text_var_list:
                if help_text_var in ('form', 'table'):
                    tab_info['type'] = help_text_var.upper()

            meta_data_dict['tab_list'].append(tab_info)

        meta_data_dict['buttons'] = {
                'SAVE': { 'order': 1,
                          'title': 'Guardar',
                          'kind': 'SAVE',
                          'url': url,
                          'disable': False,
                          'icon': 'save',
                          'backurl': None,
                          'default': True,
                          'method': 'POST',
                    },
                'EXIT': { 'order': 2,
                          'title': 'Salir',
                          'kind': 'EXIT',
                          'url': None,
                          'disable': False,
                          'icon': 'back',
                          'default': False,
                          'method': 'GET',
                    },
                }

        return meta_data_dict

class GenericTabTableElementMetadata(GenericMetadata):

    def determine_metadata(self, request, view):
        
        meta_data_dict = GenericMetadata.determine_metadata(self, request, view)
        url = request._request.path
        backurl = request._request.path[:-1] + "table/"
        meta_data_dict["buttons"]["SAVE"]["url"] = url
        meta_data_dict["buttons"]["SAVE"]["backurl"] = backurl
        meta_data_dict["buttons"]["EXIT"]["url"] = backurl

        return meta_data_dict


##########

class GenericWizardMetadata(BaseMetadata):

    def determine_metadata(self, request, view):
        serializer_class = view.serializer_class
        url = ResourceHost.getURL('BASE_URL') + request._request.path
        meta_data_dict = {}
        meta_data_dict['base_url'] = url
        meta_data_dict['type'] = 'WIZARD'
        meta_data_dict['title'] = serializer_class.Meta.label

        try:
            step_list = serializer_class.Meta.fields
        except:
            step_list = []

        meta_data_dict['step_list'] = []
        for field_name in step_list:

            field = None
            verbose_name = field_name.capitalize()

            try:
                field = serializer_class._declared_fields[field_name]
                if field.label is not None:
                    verbose_name = field.label.capitalize()
            except Exception as e:
                pass

            try:
                readonly = field.read_only
            except Exception as e:
                readonly = False

            step_info = GenericTotalField.getFieldDescription(request, field, container_name=field_name, allreadonly=readonly)
            step_info['name'] = field_name
            step_info['title'] = verbose_name
            step_info['readonly'] = readonly

            help_text_var_list = []
            if field.help_text is not None:
                help_text_var_list = field.help_text.split('|') 
            for help_text_var in help_text_var_list:
                if help_text_var in ('form', 'table'):
                    step_info['type'] = help_text_var.upper()

            meta_data_dict['step_list'].append(step_info)

        meta_data_dict['buttons'] = {
                'SAVE': { 'order': 1,
                          'title': 'Guardar',
                          'kind': 'SAVE',
                          'url': url,
                          'disable': False,
                          'icon': 'save',
                          'backurl': None,
                          'default': True,
                          'method': 'POST',
                    },
                'EXIT': { 'order': 2,
                          'title': 'Salir',
                          'kind': 'EXIT',
                          'url': None,
                          'disable': False,
                          'icon': 'back',
                          'default': False,
                          'method': 'GET',
                    },
                }

        return meta_data_dict

class GenericFramesMetadata(BaseMetadata):

    def determine_metadata(self, request, view):
        serializer_class = view.serializer_class
        meta_data_dict = GenericTotalField.getFieldDescription(request, serializer_class)
        meta_data_dict['type'] = 'FRAMES'
        meta_data_dict['title'] = serializer_class.Meta.label
        meta_data_dict['columns'] = 1
        return meta_data_dict   


class AboutMetadata(GenericQueryMetadata):

    def determine_metadata(self, request, view):
        meta_data_dict = GenericQueryMetadata.determine_metadata(self, request, view)
        meta_data_dict['fields'] = [
                                        {  'name': 'sw_name', 'verbose_name': 'Plataforma' },
                                        {  'name': 'version', 'verbose_name': 'Versión Backend' },
                                        {  'name': 'environment', 'verbose_name': 'Entorno Backend' },
                                        {  'name': 'licenser', 'verbose_name': 'Propietario' },
                                        {  'name': 'licenser_email', 'verbose_name': 'Email Propietario' },
                                        {  'name': 'developer', 'verbose_name': "Desarrollador" },
                                        {  'name': 'developer_url', 'verbose_name': "URL Desarrollador" },
                                        {  'name': 'developer_email', 'verbose_name': "Email Desarrollador" },
                                        {  'name': 'developer_address', 'verbose_name': "Dirección Desarrollador" },
                                        {  'name': 'developer_phone', 'verbose_name': "Teléfono Desarrollador" },
                                        {  'name': 'developer_city', 'verbose_name': "Ciudad Desarrollador" },
            ]

        return meta_data_dict
