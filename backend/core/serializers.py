# -*- coding: utf-8 -*-
from rest_framework import serializers
from backend.core.models import ModuleInstalled

class GenericQuerySerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField('get_alternate_name')

    def get_alternate_name(self, obj):
        result = ''
        result += str(obj)
        return result

class ModuleInstalledSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = ModuleInstalled
        fields = ("id", "name")
