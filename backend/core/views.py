# -*- coding: utf-8 -*-
from backend.components.filemanager.models import TemporalFile
from backend.components.dashboard.models import Dashboard
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from django.conf import settings
from rest_framework import status, viewsets
from backend.core.metadata import AboutMetadata, GenericTabTableElementMetadata, GenericTableMetadata,\
    GenericMetadata, GenericQueryMetadata, GenericTabsMetadata
from backend.auth.accounts.models import Account
from backend.core.models import ResourceHost, Environment, ModuleInstalled, TemporalData
from django.views.generic.base import RedirectView
from backend.components.auditor.models import AuditorParameter
from rest_framework import pagination
from backend.core.serializers import ModuleInstalledSerializer
from backend.core.permissions import IsAdminAndReadOnly

favicon_view = RedirectView.as_view(url='/media/favicon.ico', permanent=True)

class DashboardView(APIView):
    permission_classes = (IsAuthenticated, )

    def get(self, request, format=None):
        account = Account.getAccount(request.user)
        data = Dashboard.getDashboard(account)
        return Response(data)
    
class ModuleInstalledViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = ModuleInstalled.objects.all()
    serializer_class = ModuleInstalledSerializer
    permission_classes = (IsAdminAndReadOnly, )

class AboutView(APIView):
    metadata_class = AboutMetadata
    title = 'Acerca de...'

    def get(self, request, format=None):
        account = Account.getAccount(user=request.user)
        data = {}
        data['sw_name'] = settings.COMERCIAL_APP_NAME
        data['version'] = settings.VERSION
        data['environment'] = Environment.getEnvironmentName(account=account)
        if account is not None:
            if account.licenser:
                data['licenser'] = account.licenser.name.upper()
                if account.licenser.email is not None:
                    data['licenser_email'] = account.licenser.email
                else:
                    data['licenser_email'] = ''
            else:
                data['licenser'] = "ER TECHNOLOGY LTDA."
                data['licenser_email'] = "info@er-technology.net"
        data['developer'] = "ER Technology LTDA."
        data['developer_url'] = "http://www.er-technology.net"
        data['developer_email'] = "info@er-technology.net"
        data['developer_address'] = "Avenida 9N 10N-89 Of. 102"
        data['developer_phone'] = "(+57) 322 654 2978 - (+57) 2 373 17 33"
        data['developer_city'] = "Cali - Colombia"

        return Response(data)

class BaseView(APIView):
    permission_classes = (AllowAny,)

    def get(self, request, format=None):
        account = Account.getAccount(user=request.user)
        data = {}
        data['app_name'] = settings.COMERCIAL_APP_NAME                             
        data['backend_version'] = settings.VERSION
        data['developer'] = settings.DEVELOPER_INFO
        data['ENVIRONMENT'] = ResourceHost.getEnvironment(account)

        return Response(data)

class APIBaseView(APIView):
    permission_classes = (AllowAny,)

    def get(self, request, format=None):
        account = Account.getAccount(user=request.user)
        data = {}
        data['app_name'] = settings.COMERCIAL_APP_NAME                             
        data['backend_version'] = settings.VERSION
        data['developer'] = settings.DEVELOPER_INFO
        data['ENVIRONMENT'] = ResourceHost.getEnvironment(account)

        return Response(data)

class SchemeView(APIView):
    permission_classes = (AllowAny,)

    def get(self, request, format=None):
        data = {}
        base_url = ResourceHost.getURL('BASE_URL')

        for key in settings.COMPONENT_LIST.keys():
            url = base_url + '/'
            url += settings.COMPONENT_LIST[key]["COMPONENT_PATH"]
            url += '/'
            data_url = {}
            data_url['NAME'] = settings.COMPONENT_LIST[key]["NAME"]
            data_url['URL'] = url
            data[key] = data_url

        return Response(data)

class CloseDayView(APIView):

    def get(self, request, format=None):
        error = None
        data = {}
        from django.core.management import call_command
        try:
            call_command('closeday')
            data['SUCCESS'] = 'Proceso Ejecutado Correctamente...'
        except Exception as e:
            error = 'Error: %s' % str(e)
        
        if error is None:
            return Response(data=data)
        return Response(data={'ERROR': error}, status=status.HTTP_400_BAD_REQUEST)

class InitDayView(APIView):

    def get(self, request, format=None):
        error = None
        data = {}
        from django.core.management import call_command
        try:
            call_command('initday')
            data['SUCCESS'] = 'Proceso Ejecutado Correctamente...'
        except Exception as e:
            error = 'Error: %s' % str(e)
        
        if error is None:
            return Response(data=data)
        return Response(data={'ERROR': error}, status=status.HTTP_400_BAD_REQUEST)

class GenericTableViewSet(viewsets.ReadOnlyModelViewSet):
    metadata_class = GenericTableMetadata
    pagination.PageNumberPagination.page_size = 20
    permission_classes= (IsAuthenticated, )
    filter_licenser = settings.FILTER_LICENSER

    def get_queryset(self, queryset=None):
        if queryset is None:
            queryset = self.queryset
        try:
            self.filter_fields = self.serializer_class.Meta.filter_fields
        except:
            pass
        try:
            self.search_fields = self.serializer_class.Meta.search_fields
        except:
            pass
        try:
            self.ordering_fields = self.serializer_class.Meta.ordering_fields
        except:
            pass
        context = self.get_serializer_context()
        request = context['request']
        try:
            actual_account = Account.getAccount(request.user)
            AuditorParameter.registerEvent(self.request, self.app_name, self.model_name, None, None, AuditorParameter.QUERY, action='QUERY_'+ self.model_name.upper() )
            if actual_account is None:
                queryset = queryset.none()
            elif self.filter_licenser:
                queryset = queryset.filter(licenser=actual_account.licenser)
        except:
            queryset = queryset.none()
        return queryset

class GenericQueryViewSet(viewsets.ReadOnlyModelViewSet):
    metadata_class = GenericQueryMetadata
    filter_licenser = settings.FILTER_LICENSER

    def get_queryset(self, queryset=None):
        if queryset is None:
            queryset = self.queryset
        try:
            self.filter_fields = self.serializer_class.Meta.filter_fields
        except:
            pass
        try:
            self.search_fields = self.serializer_class.Meta.search_fields
        except:
            pass
        context = self.get_serializer_context()
        request = context['request']
        try:
            actual_account = Account.getAccount(request.user)
            AuditorParameter.registerEvent(self.request, self.app_name, self.model_name, None, None, AuditorParameter.QUERY, action='QUERY_'+ self.model_name.upper() )
            if self.filter_licenser:
                queryset = queryset.filter(licenser=actual_account.licenser)
        except:
            queryset = queryset.none()
        return queryset
    
    def paginate_queryset(self, queryset, view=None):
        return None

class GenericModelViewSet(viewsets.ModelViewSet):
    metadata_class = GenericMetadata
    permission_classes = (IsAuthenticated, )
    filter_licenser = settings.FILTER_LICENSER

    def paginate_queryset(self, queryset, view=None):
        return None

    def create(self, request, *args, **kwargs):
        try:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            data = serializer.data.copy()
            data['level'] = 'success'
            data['message'] = 'Se creó el registro satisfactoriamente'
            return Response(data, status=status.HTTP_201_CREATED, headers=headers)
        except Exception as error:
            data = {}
            data['level'] = 'error'
            data['message'] = f'Se presentó un error al crear el objeto: {error}'
            return Response(data, status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def get_queryset(self):
        queryset = self.queryset
        context = self.get_serializer_context()
        request = context['request']
        try:
            self.filter_fields = self.serializer_class.Meta.filter_fields
        except:
            pass
        try:
            actual_account = Account.getAccount(request.user)
            AuditorParameter.registerEvent(self.request, self.app_name, self.model_name, None, None, AuditorParameter.QUERY, action='QUERY_'+ self.model_name.upper() )
            if actual_account is None:
                queryset = self.queryset.none()
            elif self.filter_licenser:
                queryset = queryset.filter(licenser=actual_account.licenser)
        except:
            queryset = self.queryset.none()
        return queryset

    def perform_destroy(self, instance):
        old_serializer_data = self.serializer_class(instance).data
        try:
            AuditorParameter.registerEvent(self.request, self.app_name, self.model_name, None, instance.id, AuditorParameter.DELETE, before_register_object=old_serializer_data, action='DELETE_' + self.model_name.upper() )
        except:
            pass
        instance.delete()
    
class GenericTabsView(APIView):
    metadata_class = GenericTabsMetadata
    serializer_class = None
    permission_classes = (IsAuthenticated,)
    app_name = None
    model_name = None

    def delete(self, request):
        error = None
        
        actual_account = Account.getAccount(request.user)
        if actual_account is None:
            error = 'Debe haber un usuario registrado'
        
        if 'tmpid' in request.data.keys() and request.data['tmpid'] is not None:
            tmpid = request.data['tmpid']
        else:
            error = 'No se envió el id del proceso temporal'

        if error is None:

            try:
                temporal_data = TemporalData.objects.get(account=actual_account, tmpid=tmpid)
            except TemporalData.DoesNotExist:
                error = 'No Existe el proceso temporal'

        if error is None:
            temporal_data.delete()

        if error is None:    
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response(data={"level": "error", 'message': error}, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, data=None, data_table=None, object_id=None):
        error = None
        
        if data is None:
            data = {}
        
        if data_table is None:
            data_table = {}
        
        actual_account = Account.getAccount(request.user)
        if actual_account is None:
            error = 'Debe haber un usuario registrado'
        
        if 'tmpid' in request.GET.keys() and request.GET['tmpid'] is not None:
            tmpid = request.GET['tmpid']
        else:
            tmpid = TemporalData.generateKey(actual_account, self.app_name, self.model_name)

        if error is None:
            data['tmpid'] = tmpid

            try:
                temporal_data = TemporalData.objects.get(account=actual_account, tmpid=tmpid)
            except TemporalData.DoesNotExist:
                temporal_data = TemporalData()
                temporal_data.account = actual_account
                temporal_data.tmpid = tmpid
                temporal_data.app_name = self.app_name
                temporal_data.model_name = self.model_name
                temporal_data.data = {}
                temporal_data.data_table = {}
                temporal_data.delete_registers = []
                temporal_data.objectid = object_id
            temporal_data.save()

            try:
                table_fields = self.serializer_class.Meta.table_fields
            except:
                table_fields = []
            
            for process in table_fields:
                temporal_data_dict = {'_next_index': 0}
                temporal_data_table_dict = {'_next_index': 0}
                if process not in temporal_data.data.keys():
                    if process not in data.keys():
                        data[process] = {}
                        data_table[process] = {}

                    i = 0
                    for object_id in data[process].keys():
                        register = data[process][object_id]
                        register_id = f'{temporal_data.id}_{i}'
                        register['objectid'] = register['id']
                        register['id'] = register_id
                        temporal_data_dict[register_id] = register
                        temporal_data_dict['_next_index'] += 1
                        i += 1

                    i = 0
                    for object_id in data_table[process].keys():
                        register = data_table[process][object_id]
                        register_id = f'{temporal_data.id}_{i}'
                        register['objectid'] = register['id']
                        register['id'] = register_id
                        temporal_data_table_dict[register_id] = register
                        temporal_data_table_dict['_next_index'] += 1
                        i += 1

                    temporal_data.data[process] = temporal_data_dict
                    temporal_data.data_table[process] = temporal_data_table_dict

            temporal_data.save()

        if error is None:
            return Response(data=data)
        data['level'] = 'error'
        data['message'] = error
        return Response(data=data, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request):
        error = None
        actual_account = Account.getAccount(request.user)
        if actual_account is None:
            error = 'Debe haber un usuario registrado'

        data = {}

        if 'tmpid' in request.data.keys() and request.data['tmpid'] is not None:
            tmpid = request.data['tmpid']
        else:
            error = 'No se envió el id del proceso temporal'

        if error is None:
            try:
                table_fields = self.serializer_class.Meta.table_fields
            except:
                table_fields = []

            try:
                temporal_data = TemporalData.objects.get(account=actual_account, tmpid=tmpid)
            except TemporalData.DoesNotExist:
                error = 'No Existe el proceso temporal'
        
        if error is None:
            for process in table_fields:
                data[process] = { process + '_list': [], 'next_index': 0}
                if process in temporal_data.data.keys():
                    for id in temporal_data.data[process].keys():
                        if '_next_index' != id:
                            data[process][process + '_list'].append(temporal_data.data[process][id])
                        else:
                            data[process]['next_index'] = temporal_data.data[process]['_next_index']

        if error is None:
            return Response(data=data)
        data['level'] = 'error'
        data['message'] = error
        return Response(data=data, status=status.HTTP_400_BAD_REQUEST)

class GenericTabTableView(APIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = None
    app_name = None
    model_name = None
    process_name = None

    def get(self, request):
        error = None
        data = {}
        actual_account = Account.getAccount(request.user)
        if actual_account is None:
            error = 'Debe haber un usuario registrado'

        if error is None:    
            if 'tmpid' in request.GET.keys() and request.GET['tmpid'] is not None:
                tmpid = request.GET['tmpid']
            else:
                error = 'No se encuentra el id temporal'

        temporal_data = None
        if error is None:
            try:
                temporal_data = TemporalData.objects.get(account=actual_account, tmpid=tmpid)
            except TemporalData.DoesNotExist:
                error = 'No existe el proceso temporal'
        
        if error is None:
            data = {}
            data[self.process_name] = {self.process_name + '_list': []}
            fields = self.serializer_class.Meta.fields

            for register_id in temporal_data.data[self.process_name].keys():
                if register_id != '_next_index':
                    element_dict = temporal_data.data[self.process_name][register_id]
                    if '_DELETED' not in element_dict.keys() or not element_dict['_DELETED']:
                        element_table_dict = { 'ORIGINAL':  element_dict, 
                                            'TABLE':  {} 
                                        }
                        for field in fields:
                            element_table_dict['TABLE'][field] = element_dict[field]

                        data[self.process_name][self.process_name + '_list'].append(element_table_dict)
            data['temporal_data'] = temporal_data

        if error is None:
            return Response(data=data)
        return Response(data={"level": "error", 'message': error}, status=status.HTTP_400_BAD_REQUEST)

class GenericTabTableElementView(APIView):
    permission_classes = (IsAuthenticated, )
    metadata_class = GenericTabTableElementMetadata
    serializer_class = None
    app_name = None
    model_name = None
    process_name = None

    def get(self, request, object_id=None, data=None):

        error = None
        if data is None:
            data = {}
        actual_account = Account.getAccount(request.user)
        if actual_account is None:
            error = 'Debe haber un usuario registrado'        
        
        if error is None:
            data, error = TemporalData.getRegisterData(self.process_name, actual_account, object_id, data)

        if error is None:    
            return Response(data=data)

        return Response(data={"level": "error", 'message': error}, status=status.HTTP_400_BAD_REQUEST)
    
    def delete(self, request, object_id):
        error = None
        
        actual_account = Account.getAccount(request.user)
        if actual_account is None:
            error = 'Debe haber un usuario registrado'
        
        if error is None:
            error = TemporalData.deleteRegister(self.process_name, actual_account, object_id)

        if error is None:    
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response(data={"level": "error", 'message': error}, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request):
        error = None 
        data = {}
        actual_account = Account.getAccount(request.user)
        if actual_account is None:
            error = 'Debe haber un usuario registrado'

        tmpid = None
        if error is None:
            if 'tmpid' in request.data.keys() and request.data['tmpid']:
                tmpid = request.data['tmpid']
            else:
                error = 'No se encuentra el id temporal'
        
        temporal_data = None
        if error is None:
            try:
                temporal_data = TemporalData.objects.get(account=actual_account, tmpid=tmpid)
            except TemporalData.DoesNotExist:
                error = 'No Existe el registro temporal'

        if error is None:
            serializer = self.serializer_class(data=request.data)
            serializer.is_valid(raise_exception=True)
            
        if error is None:
            data, error = temporal_data.createRegister(self.process_name, actual_account, serializer.data, request.data, self.serializer_class.Meta.fields)

        data['level'] = 'success'
        data['message'] = 'Proceso Exitoso'

        if error is None:    
            return Response(data=data)
        return Response(data={"level": "error", 'message': error}, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, object_id):
        error = None
        
        actual_account = Account.getAccount(request.user)
        if actual_account is None:
            error = 'Debe haber un usuario registrado'
        
        if error is None:
            serializer = self.serializer_class(data=request.data)
            serializer.is_valid(raise_exception=True)
            
        if error is None:
            error = TemporalData.updateRegister(self.process_name, actual_account, object_id, serializer.data, self.serializer_class.Meta.fields)
    
        if error is None:  
            return Response()
        return Response(data={"level": "error", 'message': error}, status=status.HTTP_400_BAD_REQUEST)
    
