INSERT INTO core_protocol ( name ) VALUES ('http');
INSERT INTO core_protocol ( name ) VALUES ('https');

INSERT INTO core_environment ( name, is_active ) VALUES ('DEVELOPMENT', TRUE);
INSERT INTO core_environment ( name, is_active ) VALUES ('TESTING', FALSE);
INSERT INTO core_environment ( name, is_active ) VALUES ('PRODUCTION', FALSE);

