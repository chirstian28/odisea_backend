from rest_framework import serializers

class GenericQuerySerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField('get_alternate_name')

    def get_alternate_name(self, obj):
        result = ''
        result += str(obj)
        return result


class PageSerializer(serializers.ModelSerializer):
    title = serializers.CharField(label='Título', default='no title')
    url = serializers.CharField(label='URL')
    method = serializers.ChoiceField(label='Método', choices=[('POST', 'POST' ), ('GET', 'GET',)], default='GET')
    call_type = serializers.ChoiceField(label='Tipo llamado', choices=[('INTER', 'Interno' ), ('EXTER', 'Externo',)])

    class Meta:
        label = 'Página'
        fields = ( "title", "method", "url", "call_type" )

