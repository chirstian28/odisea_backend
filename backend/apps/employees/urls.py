# -*- coding: utf-8 -*-
from django.urls import path, include
from backend.apps.employees import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'employeetable', views.EmployeeTableViewSet,basename='employeetable')
router.register(r'employeequery', views.EmployeeQueryViewSet, basename='employeequery')
router.register(r'positiontable', views.PositionTableViewSet, basename='positiontable')
router.register(r'positionquery', views.PositionQueryViewSet, basename='positionquery')
router.register(r'position', views.PositionViewSet, basename='position')

urlpatterns = [
    path('employee/', views.EmployeeView.as_view()),
]

urlpatterns += router.urls
