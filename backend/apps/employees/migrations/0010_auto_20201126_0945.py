# Generated by Django 2.2.17 on 2020-11-26 09:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('entities', '0014_delete_sector'),
        ('license', '0002_auto_20201118_1238'),
        ('employees', '0009_auto_20201125_1308'),
    ]

    operations = [
        migrations.CreateModel(
            name='Kinship',
            fields=[
                ('id', models.CharField(max_length=2, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=22, verbose_name='Nombre')),
            ],
            options={
                'verbose_name': 'Parentesco',
                'verbose_name_plural': 'Parentescos',
                'ordering': ['name'],
            },
        ),
        migrations.AlterModelOptions(
            name='bloodtype',
            options={'verbose_name': 'Grupo Sanguineo', 'verbose_name_plural': 'Grupos sanguíneos'},
        ),
        migrations.RemoveField(
            model_name='familyinfo',
            name='relationship',
        ),
        migrations.AddField(
            model_name='employee',
            name='bank',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='entities.Bank', verbose_name='Banco'),
        ),
        migrations.AddField(
            model_name='employee',
            name='bank_account',
            field=models.CharField(blank=True, max_length=40, null=True, verbose_name='Cuenta Bancaria'),
        ),
        migrations.AlterField(
            model_name='employee',
            name='blood_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='employees.BloodType', verbose_name='Tipo de sangre'),
        ),
        migrations.AlterField(
            model_name='employee',
            name='immediate_boss',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='employees.Employee', verbose_name='Jefe inmediato'),
        ),
        migrations.AlterField(
            model_name='employee',
            name='study_level',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='employees.StudyLevel', verbose_name='Nivel de estudios'),
        ),
        migrations.AlterField(
            model_name='studyinfo',
            name='institution',
            field=models.CharField(max_length=100, verbose_name='Nombre de la Institución'),
        ),
        migrations.AlterField(
            model_name='studyinfo',
            name='name',
            field=models.CharField(max_length=100, verbose_name='Título Obtenido'),
        ),
        migrations.AlterUniqueTogether(
            name='employee',
            unique_together={('licenser', 'identification')},
        ),
        migrations.AddField(
            model_name='familyinfo',
            name='kinship',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.PROTECT, to='employees.Kinship', verbose_name='Parentesco'),
            preserve_default=False,
        ),
    ]
