from rest_framework import serializers
from backend.apps.employees.models import Employee, Position, StudyInfo
from backend.core.generic_serializer import GenericQuerySerializer
from backend.auth.accounts.models import Account
from backend.components.auditor.models import AuditorParameter
from rest_framework.fields import ValidationError
from backend.core.models import ResourceHost

class PositionTableSerializer(serializers.ModelSerializer):
    risk_level = serializers.CharField(label='Nivel de Riesgo')

    class Meta:
        model = Position
        fields = ('name', 'risk_level', 'salarial_base', 'salarial_period',)
        search_fields = ['^name', ]
        filter_fields = ('risk_level', "salarial_period", )
        
class PositionQuerySerializer(GenericQuerySerializer):

    class Meta:
        model = Position
        fields = ('id', 'name',)

class PositionSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        request = self.context['request']
        actual_account = Account.getAccount(request.user)
        if actual_account is not None:
            instance, instance_created = Position.objects.get_or_create(licenser=actual_account.licenser, **validated_data)
            instance_data = PositionSerializer(instance)
            AuditorParameter.registerEvent(request, 'employees', 'Position', instance_data.data, instance.id, AuditorParameter.CREATE, action='CREATE_POSITION')
        else:
            raise ValidationError("No se encuentra un inicio de sesión")
        return instance

    def update(self, instance, validated_data):
        request = self.context['request']
        old_serializer_data = PositionSerializer(instance).data
        serializer = super(PositionSerializer, self).update(instance, validated_data)
        instance_data = PositionSerializer(instance)
        AuditorParameter.registerPosition(request, 'employees', 'Position', instance_data.data, instance.id,
                                       AuditorParameter.UPDATE, before_register_object=old_serializer_data,
                                       action='UPDATE_POSITION')
        return serializer

    class Meta:
        model = Position
        fields = ('name', 'functions_performed', 'risk_level', 'salarial_base', 
                  'salarial_period',)

class EmployeeTableSerializer(serializers.ModelSerializer):
    position = serializers.CharField(label='Cargo')
    status = serializers.CharField(label='Estado')

    class Meta:
        
        model = Employee
        verbose_name = 'Empleados'
        fields = ("identification", "name", "last_name", "position", "status")
        search_fields = ['^name', "^position", ]
        fields_actions = {
                            'position': { 'items': { 'url': ResourceHost.getURL('BASE_URL') + '/employees/positionquery/', 
                                          'always': True,
                                    },
                                },                            
                    }

class EmployeeQuerySerializer(GenericQuerySerializer):

    class Meta:
        model = Employee
        fields = ('id', 'name',)

class EmployeeSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        request = self.context['request']
        actual_account = Account.getAccount(request.user)
        if actual_account is not None:
            instance, instance_created = Employee.objects.get_or_create(licenser=actual_account.licenser, **validated_data)
            instance_data = EmployeeSerializer(instance)
            AuditorParameter.registerEvent(request, 'employees', 'Employee', instance_data.data, instance.id, AuditorParameter.CREATE, action='CREATE_EMPLOYEE')
        else:
            raise ValidationError("No se encuentra un inicio de sesión")
        return instance

    def update(self, instance, validated_data):
        request = self.context['request']
        old_serializer_data = EmployeeSerializer(instance).data
        serializer = super(EmployeeSerializer, self).update(instance, validated_data)
        instance_data = EmployeeSerializer(instance)
        AuditorParameter.registerPosition(request, 'employees', 'Employee', instance_data.data, instance.id,
                                       AuditorParameter.UPDATE, before_register_object=old_serializer_data,
                                       action='UPDATE_EMPLOYEE')
        return serializer

    class Meta:
        model = Employee
        fields = ('identification', 'name', 'last_name', 'blood_type', 'address', 
                  'phone',  'personal_email', 'birthdate', 'study_level', 
                  'photo', 'cv', 'position', 'corporate_email', 'extension', 
                  'immediate_boss', 'salary', 'salarial_period', 'eps',
                  'pension_fund', 'bank', 'bank_account', 'status',
                 )

class EmployeeForm1Serializer(serializers.ModelSerializer):

   class Meta:
       model = Employee
       fields = ('identification', 'name', 'last_name', 'birthdate', 
                 'blood_type', 'address', 'phone', 'personal_email', )


class EmployeeForm2Serializer(serializers.ModelSerializer):

   class Meta:
       model = Employee
       fields = ('position', 'corporate_email', 'extension', 'immediate_boss', 
                 'salarial_period', 'salary', 'study_level', 'eps', 
                 'pension_fund',)

class EmployeeForm3Serializer(serializers.ModelSerializer):

   class Meta:
       model = Employee
       fields = ('photo', 'cv', 'hobbies', 'bank', 'bank_account', 
                 'status')

class StudyInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = StudyInfo
        fields = ('name', 'institution', 'level', 'start_date', 'end_date',)

class FamilyInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = StudyInfo
        fields = ('name', 'kinship', 'phone',)


class EmployeeStartSerializer(serializers.Serializer):
   form_1 = EmployeeForm1Serializer(label="Información personal")
   form_2 = EmployeeForm2Serializer(label="Información laboral")
   form_3 = EmployeeForm3Serializer(label="Información Adicional y aficiones")
   form_4 = StudyInfoSerializer(label="Información de Estudios")
   form_5 = StudyInfoSerializer(label="Información Familiar")

   class Meta:
       label = "Formulario Empleado"
       fields = ('form_1', 'form_2', 'form_3', 'form_4', 'form_5')


class EmployeeInfoSerializer(serializers.ModelSerializer):
    
   class Meta:
       model = Employee
       label = 'Empleados'
       fields = ("identification", "name", "last_name", "salary")
