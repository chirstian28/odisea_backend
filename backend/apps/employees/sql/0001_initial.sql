INSERT INTO employees_studylevel ( name, id, "order" ) VALUES ('Primaria', 'PR', '1');
INSERT INTO employees_studylevel ( name, id, "order" ) VALUES ('Secundaria', 'SC', '2');
INSERT INTO employees_studylevel ( name, id, "order" ) VALUES ('Curso', 'CU', '3');
INSERT INTO employees_studylevel ( name, id, "order" ) VALUES ('Técnico', 'TC', '4');
INSERT INTO employees_studylevel ( name, id, "order" ) VALUES ('Diplomado', 'DI', '5');
INSERT INTO employees_studylevel ( name, id, "order" ) VALUES ('Tecnólogo', 'TG', '6');
INSERT INTO employees_studylevel ( name, id, "order" ) VALUES ('Universitario', 'UN', '7');
INSERT INTO employees_studylevel ( name, id, "order" ) VALUES ('Especialización', 'ES', '8');
INSERT INTO employees_studylevel ( name, id, "order" ) VALUES ('Postgrado', 'PS', '9');
INSERT INTO employees_studylevel ( name, id, "order" ) VALUES ('Maestría', 'MA', '10');
INSERT INTO employees_studylevel ( name, id, "order" ) VALUES ('Doctorado', 'DO', '11');

INSERT INTO employees_bloodtype ( type, symbol ) VALUES ('A', '+');
INSERT INTO employees_bloodtype ( type, symbol ) VALUES ('A', '-');
INSERT INTO employees_bloodtype ( type, symbol ) VALUES ('AB', '+');
INSERT INTO employees_bloodtype ( type, symbol ) VALUES ('AB', '-');
INSERT INTO employees_bloodtype ( type, symbol ) VALUES ('B', '+');
INSERT INTO employees_bloodtype ( type, symbol ) VALUES ('B', '-');
INSERT INTO employees_bloodtype ( type, symbol ) VALUES ('O', '+');
INSERT INTO employees_bloodtype ( type, symbol ) VALUES ('O', '-');

INSERT INTO employees_risklevel ( id, name, percent ) VALUES ('01', 'Riesgo I', 0.522);
INSERT INTO employees_risklevel ( id, name, percent ) VALUES ('02', 'Riesgo II', 1.044);	
INSERT INTO employees_risklevel ( id, name, percent ) VALUES ('03', 'Riesgo III', 2.436);	
INSERT INTO employees_risklevel ( id, name, percent ) VALUES ('04', 'Riesgo IV', 4.350);
INSERT INTO employees_risklevel ( id, name, percent ) VALUES ('05', 'Riesgo V', 6.960);

INSERT INTO employees_kinship ( id, name ) VALUES ('CO', 'CONYUGE');
INSERT INTO employees_kinship ( id, name ) VALUES ('MA', 'MADRE');
INSERT INTO employees_kinship ( id, name ) VALUES ('PA', 'PADRE');
INSERT INTO employees_kinship ( id, name ) VALUES ('HE', 'HERMANO(A)');
INSERT INTO employees_kinship ( id, name ) VALUES ('TI', 'TIO(A)');
INSERT INTO employees_kinship ( id, name ) VALUES ('AB', 'ABUELO(A)');
INSERT INTO employees_kinship ( id, name ) VALUES ('HI', 'HIJO(A)');
INSERT INTO employees_kinship ( id, name ) VALUES ('SU', 'SUEGRO(A)');
INSERT INTO employees_kinship ( id, name ) VALUES ('OT', 'OTRO');

