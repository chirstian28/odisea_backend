from rest_framework import serializers

from backend.apps.employees.metadata import CreateEmployeeRequestMetadata
from backend.apps.employees.models import Employee, Position
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from backend.auth.accounts.models import Account
from backend.core.views import GenericTableViewSet, GenericQueryViewSet, GenericModelViewSet
from backend.core.permissions import IsAdminOrReadOnly
from backend.apps.employees.serializers import (
    EmployeeQuerySerializer,
    EmployeeTableSerializer,
    EmployeeStartSerializer,
    PositionSerializer,
    PositionQuerySerializer,
    PositionTableSerializer
    )

class PositionTableViewSet(GenericTableViewSet):
    app_name = 'employees'
    model_name = 'Position'
    queryset = Position.objects.all()
    serializer_class = PositionTableSerializer
    
class PositionQueryViewSet(GenericQueryViewSet):
    app_name = 'employees'
    model_name = 'Position'
    queryset = Position.objects.all()
    serializer_class = PositionQuerySerializer

class PositionViewSet(GenericModelViewSet):
    app_name = 'employees'
    model_name = 'Position'
    queryset = Position.objects.all()
    serializer_class = PositionSerializer
    permission_classes = (IsAdminOrReadOnly, )

class EmployeeView(APIView):
   serializer_class = EmployeeStartSerializer
   metadata_class = CreateEmployeeRequestMetadata
   permission_classes = [IsAdminOrReadOnly]

   def post(self, request):
       actual_account = Account.getAccount(request.user)
       data = {}
       list_errors = []
       validate_response = '1'

       if actual_account is None:
           validate_response = '0'
           list_errors.append('No se encuentra un inicio de sesion')
       else:
           obj_employee, list_errors = Employee.save_data(request.data, actual_account)

           if obj_employee is None and len(list_errors) != 0:
               validate_response = '0'
               list_errors.append('El empleado no se pudo guardar')

       if validate_response == '1':
           return Response(data=data)

       return Response(data={'VAL': validate_response, 'ERROR': list_errors}, status=status.HTTP_400_BAD_REQUEST)


class EmployeeTableViewSet(GenericTableViewSet):
   app_name = 'employees'
   model_name = 'Employee'
   queryset = Employee.objects.all()
   serializer_class = EmployeeTableSerializer
   permission_classes = (IsAdminOrReadOnly, )

class EmployeeQueryViewSet(GenericQueryViewSet):
   app_name = 'employees'
   model_name = 'Employee'
   queryset = Employee.objects.all()
   serializer_class = EmployeeQuerySerializer
   permission_classes = (IsAdminOrReadOnly, )

