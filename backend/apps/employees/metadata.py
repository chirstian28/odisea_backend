# -*- coding: utf-8 -*-
from backend.core.metadata import GenericWizardMetadata


class CreateEmployeeRequestMetadata(GenericWizardMetadata):

    def determine_metadata(self, request, view):
        meta_data_dict = GenericWizardMetadata.determine_metadata(self, request, view)
        meta_data_dict['step_list'][-1]['buttons']['FINISH']['title'] = 'Finalizar'
        return meta_data_dict