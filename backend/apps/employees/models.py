from django.db import models
from backend.library.datemanager.models import Holiday
from backend.apps.entities.models import Eps, PensionFund, Bank
from backend.apps.general.models import Period, Status
from django.core.exceptions import ObjectDoesNotExist


class RiskLevel(models.Model):
    id = models.CharField(max_length=10, primary_key=True, verbose_name="ID")
    name = models.CharField(max_length=70, verbose_name="Nombre")
    percent = models.DecimalField(max_digits=7, decimal_places=4, verbose_name='Porcentaje de Riesgo')

    def __str__(self):
        return f'{self.name}'   

    class Meta:
        ordering = ['id']
        app_label = 'employees'
        verbose_name = 'Nivel de Riesgo'
        verbose_name_plural = 'Niveles de Riesgos'

class Position(models.Model):
    licenser = models.ForeignKey('license.Licenser', on_delete=models.PROTECT)
    name = models.CharField(max_length=22, verbose_name="Nombre del cargo")
    functions_performed = models.TextField(max_length=250, verbose_name="Funciones realizadas")
    risk_level = models.ForeignKey('RiskLevel', on_delete=models.PROTECT, verbose_name="Nivel de Riesgo")
    salarial_base = models.IntegerField(verbose_name="Base Salarial", null=True, blank=True)
    salarial_period = models.ForeignKey('general.Period', on_delete=models.PROTECT, verbose_name="Periodo Salarial", null=True, blank=True)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        unique_together = [['licenser', 'name']]
        app_label = 'employees'
        verbose_name = 'Position'
        verbose_name_plural = 'Cargos'

class BloodType(models.Model):
    BLOOD_TYPE_CHOICES = [
        ('+', 'Positive'),
        ('-', 'Negative'),
    ]

    type = models.CharField(max_length=2, verbose_name="Tipo")
    symbol = models.CharField(choices=BLOOD_TYPE_CHOICES, max_length=1, default='+', verbose_name="Simbolo")

    def __str__(self):
        return f"{self.type} {self.symbol}"
    
    class Meta:
        app_label = 'employees'
        verbose_name = 'Grupo Sanguineo'
        verbose_name_plural = 'Grupos sanguíneos'

class StudyLevel(models.Model):
    id = models.CharField(max_length=2, primary_key=True, verbose_name="ID")
    name = models.CharField(max_length=22, verbose_name="Nombre")
    order = models.IntegerField(verbose_name='Orden de Importancia', default=0)

    def __str__(self):
        return f"{self.name}"
    
    class Meta:
        ordering = ['order']
        app_label = 'employees'
        verbose_name = 'Nivel de Estudio'
        verbose_name_plural = 'Niveles de Estudios'

class Employee(models.Model):
    licenser = models.ForeignKey('license.Licenser', on_delete=models.PROTECT)
    account = models.OneToOneField('accounts.Account', null=True, blank=True, on_delete=models.SET_NULL)
    identification = models.CharField(max_length=22, verbose_name="Identificación")
    name = models.CharField(max_length=20, verbose_name="Nombre")
    last_name = models.CharField(max_length=20, verbose_name="Apellido")
    position = models.ForeignKey('Position', on_delete=models.PROTECT, verbose_name="Cargo")
    immediate_boss = models.ForeignKey('Employee', on_delete=models.PROTECT, verbose_name="Jefe inmediato", null=True, blank=True)
    salarial_period = models.ForeignKey('general.Period', on_delete=models.PROTECT, verbose_name="Periodo Salarial")
    salary = models.IntegerField(verbose_name="Salario")
    birthdate = models.DateField(verbose_name="Fecha de nacimiento")
    study_level = models.ForeignKey('StudyLevel', on_delete=models.PROTECT, verbose_name="Nivel de estudios")
    blood_type = models.ForeignKey('BloodType', on_delete=models.PROTECT, verbose_name="Tipo de sangre")
    eps = models.ForeignKey('entities.Eps', on_delete=models.PROTECT, verbose_name="Eps", null=True, blank=True)
    pension_fund = models.ForeignKey('entities.PensionFund', on_delete=models.PROTECT, verbose_name="Fondo de pensiones", null=True, blank=True)
    address = models.CharField(max_length=255, verbose_name="Dirección")
    phone = models.CharField(max_length=10, verbose_name="Telefono")
    personal_email = models.EmailField(max_length=255, verbose_name="Email personal")
    corporate_email = models.EmailField(max_length=255, verbose_name="Email corporativo", null=True, blank=True)
    extension = models.CharField(max_length=10, verbose_name="Teléfono Corporativo")
    photo = models.FileField(verbose_name="Foto")
    cv = models.FileField(verbose_name="Hoja de vida")
    hobbies = models.TextField(max_length=250, verbose_name="Aficiones", null=True, blank=True)
    bank = models.ForeignKey('entities.Bank', on_delete=models.PROTECT, verbose_name="Banco", null=True, blank=True)
    bank_account = models.CharField(max_length=40, verbose_name='Cuenta Bancaria', null=True, blank=True)
    status = models.ForeignKey('general.Status', on_delete=models.PROTECT, verbose_name="Estado", null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha y hora")

    def __str__(self):
        return f"{self.name} {self.last_name}"

    def save_data(data, actual_account):
        employee = Employee()
        list_errors = []
        employee.licenser = actual_account.licenser

        if 'form_1.identification' in data.keys() and \
                data['form_1.identification'] != '':
            if len(data['form_1.identification']) <= 22:
                try:
                    emp_exist = Employee.objects.get(identification=data['form_1.identification'])
                except Employee.DoesNotExist:
                    emp_exist = None
                if emp_exist is None:
                    employee.identification = data['form_1.identification']
                else:
                    list_errors.append('El numero de identificación ya se encuentra registrado')
            else:
                list_errors.append('largo de identificacion no valido')
        else:
            list_errors.append('identificacion no enviada')

        if 'form_1.name' in data.keys() and \
                data['form_1.name'] != '':
            if len(data['form_1.name']) <= 20:
                employee.name = data['form_1.name']
            else:
                list_errors.append('largo de nombre no valido')
        else:
            list_errors.append('nombre no enviado')

        if 'form_1.last_name' in data.keys() and \
                data['form_1.last_name'] != '':
            if len(data['form_1.last_name']) <= 20:
                employee.last_name = data['form_1.last_name']
            else:
                list_errors.append('largo de apellido no valido')
        else:
            list_errors.append('apellido no enviado')

        if 'form_1.birthdate' in data.keys() and \
                data['form_1.birthdate'] != '':
            employee.birthdate = data['form_1.birthdate']
        else:
            list_errors.append('fecha de nacimiento no enviada')

        if 'form_1.blood_type' in data.keys() and \
                data['form_1.blood_type'] != '':
            id_blood_type = BloodType.objects.get(id=data['form_1.blood_type'])
            if id_blood_type is not None:
                employee.blood_type = id_blood_type
            else:
                list_errors.append('No se encontro un tipo de sangre asociado a ese id')
        else:
            list_errors.append('tipo de sangre no enviado')

        if 'form_1.address' in data.keys() and \
                data['form_1.address'] != '':
            if len(data['form_1.address']) <= 20:
                employee.address = data['form_1.address']
            else:
                list_errors.append('largo de la direccion no valido')
        else:
            list_errors.append('direccion no enviada')

        if 'form_1.phone' in data.keys() and \
                data['form_1.phone'] != '':
            if len(data['form_1.phone']) <= 10:
                employee.phone = data['form_1.phone']
            else:
                list_errors.append('numero de telefono no valido')
        else:
            list_errors.append('telefono no enviado')

        if 'form_1.personal_email' in data.keys() and \
                data['form_1.personal_email'] != '':
            if len(data['form_1.personal_email']) <= 254:
                employee.personal_email = data['form_1.personal_email']
            else:
                list_errors.append('largo del correo no valido')
        else:
            list_errors.append('correo no enviado')

        if 'form_2.corporate_email' in data.keys() and \
                data['form_2.corporate_email'] != '':
            if len(data['form_2.corporate_email']) <= 254:
                employee.corporate_email = data['form_2.corporate_email']
            else:
                list_errors.append('largo del correo corporativo no valido')
        else:
            list_errors.append('correo corporativo no enviado')

        if 'form_2.position' in data.keys() and \
                data['form_2.position'] != '':
            position = Position.objects.filter(id=data['form_2.position']).first()
            if position is not None:
                employee.position = position
            else:
                list_errors.append('No se encontro un cargo asociado a ese id')
        else:
            list_errors.append('cargo no enviado')

        if 'form_2.immediate_boss' in data.keys() and \
                data['form_2.immediate_boss'] != '':
            obj_immediate_boss = Employee.objects.get(id=data['form_2.immediate_boss'])
            if obj_immediate_boss is not None:
                employee.immediate_boss = obj_immediate_boss
            else:
                list_errors.append('No se encontro un empleado asociado a ese id')
        else:
            employee.immediate_boss = None

        if 'form_2.salary' in data.keys() and \
                data['form_2.salary'] != '':
            if len(data['form_2.salary']) <= 8:
                employee.salary = data['form_2.salary']
            else:
                list_errors.append('largo del salario no valido')
        else:
            list_errors.append('salario no enviado')

        if 'form_2.salarial_period' in data.keys() and \
                data['form_2.salarial_period'] != '':
            obj_period = Period.objects.get(id=data['form_2.salarial_period'])
            if obj_period is not None:
                employee.salarial_period = obj_period
            else:
                list_errors.append('No se encontro un periodo de pago asociado a ese id')
        else:
            list_errors.append('periodo de pago no enviado')

        if 'form_2.study_level' in data.keys() and \
                data['form_2.study_level'] != '':
            obj_stydylevel = StudyLevel.objects.get(id=data['form_2.study_level'])
            if obj_stydylevel is not None:
                employee.study_level = obj_stydylevel
            else:
                list_errors.append('No se encontro un nivel de estudios asociado a ese id')
        else:
            list_errors.append('nivel de estudios no enviado')

        if 'form_2.eps' in data.keys() and \
                data['form_2.eps'] != '':
            obj_eps = Eps.objects.get(id=data['form_2.eps'])
            if obj_eps is not None:
                employee.eps = obj_eps
            else:
                list_errors.append('No se encontro una eps asociada a ese id')
        else:
            employee.eps = None

        if 'form_2.pension_fund' in data.keys() and \
                data['form_2.pension_fund'] != '':
            obj_pension_fund = PensionFund.objects.get(id=data['form_2.pension_fund'])
            if obj_pension_fund is not None:
                employee.pension_fund = obj_pension_fund
            else:
                list_errors.append('No se encontro un fondo de pension asociado a ese id')
        else:
            employee.pension_fund = None

        if 'form_2.extension' in data.keys() and \
                data['form_2.extension'] != '':
            if len(data['form_2.extension']) <= 10:
                employee.extension = data['form_2.extension']
            else:
                list_errors.append('numero de extension no valido')
        else:
            list_errors.append('extension no enviada')

        if 'form_3.photo' in data.keys() and \
                data['form_3.photo'] != '':
            employee.photo = data['form_3.photo']
        else:
            list_errors.append('foto no enviada')

        if 'form_3.cv' in data.keys() and \
                data['form_3.cv'] != '':
            employee.cv = data['form_3.cv']
        else:
            list_errors.append('hoja de vida no enviada')

        if 'form_3.hobbies' in data.keys() and \
                data['form_3.hobbies'] != '':
            if len(data['form_3.hobbies']) <= 250:
                employee.hobbies = data['form_3.hobbies']
            else:
                list_errors.append('texto demasiado largo')
        else:
            employee.hobbies = None

        if 'form_3.bank' in data.keys() and \
                data['form_3.bank'] != '':
            obj_bank = Bank.objects.get(id=data['form_3.bank'])
            if obj_bank is not None:
                employee.bank = obj_bank
            else:
                list_errors.append('No se encontro un banco asociado a ese id')
        else:
            employee.bank = None

        if 'form_3.bank_account' in data.keys() and \
                data['form_3.bank_account'] != '':
            if len(data['form_3.bank_account']) <= 40:
                employee.bank_account = data['form_3.bank_account']
            else:
                list_errors.append('numero de cuenta bancaria invalido')
        else:
            employee.bank_account = None

        if 'form_3.status' in data.keys() and \
                data['form_3.status'] != '':
            obj_status = Status.objects.get(id=data['form_3.status'])
            if obj_status is not None:
                employee.status = obj_status
            else:
                list_errors.append('No se encontro un estado asociado a ese id')
        else:
            employee.status = None

        if len(list_errors) == 0:
            employee.save()

            return employee, list_errors
        else:

            return None, list_errors

    save_data = staticmethod(save_data)
    
    class Meta:
        unique_together = [['licenser', 'identification']]
        ordering = ["last_name", "name"]
        app_label = 'employees'
        verbose_name = 'Employee'
        verbose_name_plural = 'Empleados'

class StudyInfo(models.Model):
    licenser = models.ForeignKey('license.Licenser', on_delete=models.PROTECT)
    employee = models.ForeignKey('Employee', on_delete=models.CASCADE, verbose_name='Empleado')
    name = models.CharField(max_length=100, verbose_name="Título Obtenido")
    institution = models.CharField(max_length=100, verbose_name='Nombre de la Institución')
    level = models.ForeignKey('StudyLevel', on_delete=models.PROTECT, verbose_name="Nivel de Estudio")
    start_date = models.DateField(null=True, verbose_name="Fecha de inicio")
    end_date = models.DateField(null=True, verbose_name="Fecha de finalizacion")
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        app_label = 'employees'
        verbose_name = 'Estudio Realizado'
        verbose_name_plural = 'Estudios Realizados'

class Kinship(models.Model):
    id = models.CharField(max_length=2, primary_key=True, verbose_name="ID")
    name = models.CharField(max_length=22, verbose_name="Nombre")

    def __str__(self):
        return f"{self.name}"
    
    class Meta:
        ordering = ['name']
        app_label = 'employees'
        verbose_name = 'Parentesco'
        verbose_name_plural = 'Parentescos'

class FamilyInfo(models.Model):
    licenser = models.ForeignKey('license.Licenser', on_delete=models.PROTECT)
    employee = models.ForeignKey('Employee', on_delete=models.CASCADE, verbose_name='Empleado')
    name = models.CharField(max_length=255, verbose_name="Nombre del familiar")
    kinship = models.ForeignKey('Kinship', on_delete=models.PROTECT, verbose_name='Parentesco')
    phone = models.CharField(max_length=10, verbose_name="Telefono del familiar")
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.name}"
    
    class Meta:
        app_label = 'employees'
        verbose_name = 'Información de familia'
        verbose_name_plural = 'Información de familia'


class WorkDayEmployee(models.Model):
    licenser = models.ForeignKey('license.Licenser', on_delete=models.PROTECT)
    employee = models.ForeignKey('Employee', on_delete=models.CASCADE, verbose_name='Empleado')
    days = models.IntegerField(default=30, verbose_name="Dias laborados")
    provision = models.ForeignKey('items.Provision', on_delete=models.PROTECT)
    paid = models.BooleanField(verbose_name='Pagada?', default=False)
    
    def __str__(self):
        return f"{self.employee}"

    def valid_workdayemploye(self, payroll, provision_id):
        obj_holiday = Holiday()
        holidays_dict = obj_holiday.get_holidays(payroll.start_date, payroll.end_date)
        sundays_dict = obj_holiday.get_sundays(payroll.start_date, payroll.end_date)
        workdays = payroll.end_date.day - holidays_dict['holidays'].count() - len(sundays_dict['sundays'])
                    
        try:
            workdayemployee = WorkDayEmployee.objects.get(employee=payroll.employee.id, paid=False)
            workdayemployee.days += workdays
            workdayemployee.save()
        except ObjectDoesNotExist:
            # workdayemployee = WorkDayEmployee()
            self.licenser = payroll.licenser
            self.employee = payroll.employee
            self.provision = provision_id #Provision.objects.get(code=item.destine.code)
            self.days = workdays
            self.save()
    
    class Meta:
        app_label = 'employees'
        verbose_name = 'Dia trabajado por empleado'
        verbose_name_plural = 'Dias trabajados por empleado'
