from rest_framework import serializers

from backend.core.models import ResourceHost
from backend.apps.employees.models import Employee
from backend.apps.employees.serializers import EmployeeInfoSerializer
from backend.apps.payroll.models import Payroll, ItemPayroll
from backend.core.generic_serializer import GenericQuerySerializer
from backend.auth.accounts.models import Account
from backend.components.auditor.models import AuditorParameter
from rest_framework.fields import SerializerMethodField, ValidationError


class PayrollTableSerializer(serializers.ModelSerializer):
    url_detail = SerializerMethodField()
    employee = serializers.ReadOnlyField(source='employee.name')

    class Meta:
        model = Payroll
        verbose_name = 'Nominas'
        fields = ("employee", "days", "basic", "total_accured", "total_deductions", "net_to_pay", "url_detail")
        search_fields = ['^employee']

    def get_url_detail(self, obj):
        str_url = "/payrolls/detail/"
        complete_url = ResourceHost.getURL('BASE_URL') + str_url + str(obj.id)
        return complete_url

class PayrollPDFSerializer(serializers.ModelSerializer):
    detail = SerializerMethodField()
    employee = serializers.ReadOnlyField(source='employee.name')

    class Meta:
        model = Payroll
        verbose_name = 'Nominas'
        fields = ("employee", "days", "basic", "total_accured", "total_deductions", "net_to_pay", "detail")
        search_fields = ['^employee']

        read_only_fields = [
            'detail'
        ]

    def get_detail(self, obj):
        itempayroll_qs = ItemPayroll.objects.filter(payroll=obj.id)
        itempayroll = ItemPayrollDetailSerializer(itempayroll_qs, many=True).data
        return itempayroll


class ItemPayrollDetailSerializer(serializers.ModelSerializer):
    name = serializers.ReadOnlyField(source='item.name')

    class Meta:
        model = ItemPayroll
        verbose_name = 'Item Payroll'
        fields = ("name", "value")


class PayrollSerializer(serializers.ModelSerializer):

    class Meta:
        model = Payroll
        verbose_name = 'Nominas'
        fields = ("employee",)
        search_fields = ['^employee']



class PayrollDetailSerializer(serializers.ModelSerializer):
    items = SerializerMethodField()

    class Meta:
        model = Payroll
        verbose_name = 'Nominas'
        fields = ("employee", "days", "items", "basic", "total_accured", "total_deductions", "net_to_pay")
        search_fields = ['^employee']

    def get_items(self, obj):
        item_qs = ItemPayroll.objects.filter(payroll=obj.id)
        items = ItemPayrollDetailSerializer(item_qs, many=True).data
        return items


class PayrollQuerySerializer(GenericQuerySerializer):
    employee = SerializerMethodField()

    class Meta:
        model = Payroll
        label = 'Publicaciones'
        fields = ("id_employee", "days", "employe")

    def get_employee_data(self, obj):
        employee_qs = Employee.objects.filter(post=obj.id)
        employee_data = EmployeeInfoSerializer(employee_qs, many=True).data
        return employee_data



