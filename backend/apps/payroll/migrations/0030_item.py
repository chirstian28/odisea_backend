# Generated by Django 2.2.17 on 2020-11-25 18:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('entities', '0014_delete_sector'),
        ('license', '0002_auto_20201118_1238'),
        ('datemanager', '0001_initial'),
        ('payroll', '0029_provision'),
    ]

    operations = [
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30, verbose_name='Nombre')),
                ('value', models.FloatField(null=True, verbose_name='Valor')),
                ('calcule_base', models.TextField(blank=True, null=True, verbose_name='Fórmula Base Cálculo')),
                ('calcule_min', models.CharField(blank=True, max_length=50, null=True, verbose_name='Mínimo')),
                ('calcule_max', models.CharField(blank=True, max_length=50, null=True, verbose_name='Máximo')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Fecha y hora')),
                ('concept', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='payroll.Concept')),
                ('destine', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='destine_provision', to='payroll.Provision', verbose_name='Destino')),
                ('entity', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='entities.Entity', verbose_name='Entidad')),
                ('item_type', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='payroll.ItemType')),
                ('licenser', models.ForeignKey(default=1, on_delete=django.db.models.deletion.PROTECT, to='license.Licenser')),
                ('months_payment', models.ManyToManyField(to='datemanager.MonthName', verbose_name='Ingrese meses de pago')),
                ('source', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='payroll.Provision', verbose_name='Fuente')),
            ],
            options={
                'verbose_name': 'Item',
                'verbose_name_plural': 'Items',
                'ordering': ['name'],
            },
        ),
    ]
