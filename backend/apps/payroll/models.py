from django.db import models
import decimal
import datetime
from backend.apps.employees.models import Employee, WorkDayEmployee
from backend.apps.entities.models import Entity
from backend.apps.events.models import Event
from backend.apps.general.models import Parameter
from backend.apps.items.models import Item, Provision
from backend.library.datemanager.models import Holiday, MonthName
from django.core.exceptions import ObjectDoesNotExist
from backend.library.sendmail import send_mail


class Payroll(models.Model):
    licenser = models.ForeignKey('license.Licenser', default=1, on_delete=models.PROTECT)
    employee = models.ForeignKey('employees.Employee', on_delete=models.PROTECT)
    start_date = models.DateField(verbose_name='Fecha Inicio')
    end_date = models.DateField(verbose_name='Fecha Final')
    payment_date = models.DateField(verbose_name='Fecha Pago')
    date_time = models.DateTimeField(auto_now_add=True, verbose_name="Fecha y hora de Liquidación")
    days = models.IntegerField(default=30, verbose_name="Dias laborados")
    basic = models.DecimalField(max_digits=14, decimal_places=2, verbose_name="Básico")
    total_accured = models.DecimalField(max_digits=14, decimal_places=2, default=0, verbose_name="Total Devengos")
    total_deductions = models.DecimalField(max_digits=14, decimal_places=2, default=0, verbose_name="Total Deducciones")
    total_provisions = models.DecimalField(max_digits=14, decimal_places=2, default=0, verbose_name="Total Provisiones")
    total_prestations = models.DecimalField(max_digits=14, decimal_places=2, default=0, verbose_name="Total Prestaciones")
    total_health = models.DecimalField(max_digits=14, decimal_places=2, default=0, verbose_name="Total Seguridad Social")
    total_parafiscal = models.DecimalField(max_digits=14, decimal_places=2, default=0, verbose_name="Total Parafiscal")
    total_payments = models.DecimalField(max_digits=14, decimal_places=2, default=0, verbose_name="Total Consignación")
    net_to_pay = models.DecimalField(max_digits=14, decimal_places=2, default=0, verbose_name="Neto a pagar")

    def __str__(self):
        return f'{self.employee} {self.end_date.strftime("%Y-%m-%d")}'

    def concept_calculator(self, concept):
        current_date = datetime.date.today()
        items = Item.objects.filter(concept=concept)
        summation = 0

        for item in items:
            itempayroll = ItemPayroll()
            itempayroll.licenser = self.licenser
            itempayroll.employee = self.employee
            itempayroll.payroll = self
            itempayroll.item = item
            itempayroll.event = item.event_type

            itempayroll.value = 0
            percentage_value = None

            if item.event_type is not None:
                pass
            else:
                if (item.months_payment.count() == 0) or (
                        item.months_payment.count() > 0 and self.valid_month(item)):

                    if item.item_type.id == 'VFI':
                        itempayroll.value = item.value
                    elif item.item_type.id == 'POR':
                        if item.value is not None and item.value != '':
                            percentage_value = item.value
                    elif item.item_type.id == 'PRO':
                        try:
                            if self.valid_month(item):
                            #item.months_payment.get(monthname__id=current_date.month)
                                if (item.source.id == '00003' or item.source.id == '00004'):
                                    obj_provision = PayrollProvision.objects.get(employee=self.employee.id, provision=item.source.id, paid=False, year=str(current_date -1))
                                else:
                                    obj_provision = PayrollProvision.objects.get(employee=self.employee.id, provision=item.source.id, paid=False)
                                itempayroll.value = obj_provision.value
                                obj_provision.paid = True
                                obj_provision.payroll = self
                                obj_provision.save()
                        except ObjectDoesNotExist:
                            pass
                    elif item.item_type.id == 'VAC':
                        try:
                            obj_event = Event.objects.get(employee=self.employee.id, event_type='00003', status='01')

                            if current_date.month in (obj_event.start_date.month, obj_event.end_date.month):
                                obj_provision = PayrollProvision.objects.get(employee=self.employee.id, provision=item.source.id, paid=False)
                                # all_days_to_pay = abs(obj_event.start_date - obj_event.end_date).days
                                if current_date.month < obj_event.end_date.month:
                                    days_to_pay = abs(self.start_date - self.end_date).days
                                elif current_date.month == obj_event.end_date.month:
                                    days_to_pay = abs(self.start_date - obj_event.end_date).days
                                    obj_event.status = '05'
                                    obj_provision.paid = True
                                else: 
                                    days_to_pay = abs(obj_event.start_date - self.end_date).days
                                day_value = self.employee.salary // 30
                                itempayroll.value = days_to_pay * day_value # (self.basic * obj_workdayemployee.days) // 720
                                obj_provision.save()

                                obj_event.status = '05'
                                obj_event.save()
                        except:
                            pass
                    elif item.item_type.id == 'ARP':
                        percentage_value = self.employee.position.risk_level.percent
                    
            if (item.calcule_min != '') or (item.calcule_max != '') :
                min_value = item.calculate(item.calcule_min, self)
                max_value = item.calculate(item.calcule_max, self)
                if self.employee.salary < min_value or self.employee.salary > max_value:
                    itempayroll.value = 0

            if item.calcule_base != '':
                itempayroll.value = item.calculate(item.calcule_base, self, percentage_value)

            summation += itempayroll.value

            if item.entity_type is not None:
                if item.entity_type.id == 'EPS':
                    itempayroll.entity = self.employee.eps
                elif item.entity_type.id == 'AFP':
                    itempayroll.entity = self.employee.pension_fund
                elif item.entity_type.id == 'PAR':
                    suffix = 'PA'
                    try:
                        entity = suffix + item.name
                        itempayroll.entity = Entity.objects.get(id=entity)
                    except:
                        pass

            itempayroll.save()

            if item.destine is not None:
                payrollprovision = PayrollProvision()
                payrollprovision.valid_payrollprovision(self, item, itempayroll.value)

        return summation

    @staticmethod
    def valid_month(item):
        current_date = datetime.date.today()
        try:
            for m in item.months_payment.all():
                if m.id == current_date.month:
                    return True
            return False
        except:
            return False

    def accurred_calculator(self, other_accurred):
        self.total_accured = decimal.Decimal(self.concept_calculator('DEV')) + other_accurred
        self.save()

    def deductions_calculator(self, other_deductions):
        self.total_deductions = decimal.Decimal(self.concept_calculator('DED')) + other_deductions
        self.save()

    def prestations_calculator(self):
        self.total_prestations = self.concept_calculator('PRE')
        self.save()

    def provisions_calculator(self):
        self.total_provisions = self.concept_calculator('PRE')
        self.save()

        return 0

    def health_calculator(self):
        self.total_health = self.concept_calculator('SEG')
        self.save()

    def parafiscal_calculator(self):
        self.total_parafiscal = self.concept_calculator('PAR')
        self.save()

    def payments_calculator(self):
        self.total_payments = self.concept_calculator('CON')
        self.save()

    def total_calculator(self):
        self.net_to_pay = decimal.Decimal(self.basic) + decimal.Decimal(self.total_accured) - decimal.Decimal(self.total_deductions)
        self.save()

    @staticmethod
    def payroll_employee(acoount, days=30):
        employees = Employee.objects.all()
        #abs(employee.birthdate - current_date).days
        current_date = datetime.date.today()

        for employee in employees:
            if employee.status.id == '01':
                event = Event()
                event_dict = event.evaluate_event(employee)

                days_fix = days - event_dict['days']

                payroll = Payroll()

                payroll.licenser = acoount.licenser
                obj_holiday = Holiday()

                payroll.basic = (employee.salary * days_fix) / employee.salarial_period.days
                payroll.employee = employee
                payroll.start_date = datetime.date(current_date.year, current_date.month, 1)
                payroll.end_date = obj_holiday.month_last_date(current_date, False)
                payroll.payment_date = obj_holiday.month_last_date(current_date, False)
                payroll.days = days_fix
                payroll.save()

                payroll.prestations_calculator()
                # payroll.provisions_calculator()
                payroll.accurred_calculator(event_dict['accurred'])
                payroll.deductions_calculator(event_dict['deductions'])
                payroll.health_calculator()
                payroll.parafiscal_calculator()
                payroll.payments_calculator()
                payroll.total_calculator()
                
                event.assign_payroll(event_dict['event_ids'], payroll)

                #payroll.sendEmailPayroll()
            break

    @staticmethod
    def save_data(data, actual_account):
        list_errors = []
        payroll = Payroll()

        if actual_account is None:
            list_errors.append('No se encuentra un inicio de session activo')
        else:
            payroll.payroll_employee(actual_account)

        return payroll, list_errors

    def sendEmailPayroll(self):

        if self.licenser is not None:
            email_dict = {}
            email_dict['SUBJECT'] = ''
            email_dict['SUBJECT'] += self.licenser.name
            email_dict['SUBJECT'] += ' - '
            email_dict['SUBJECT'] += 'Notificación de Evento - '
            email_dict['SUBJECT'] += 'Generación de Nomina empleados'
            email_dict['TO_EMAIL'] = ('chirstiango@gmail.com',) #[self.employee.mail]

            email_dict['VERBOSE_TITLE'] = 'Notificación de Generación de Nomina empleados'
            email_dict['DATA'] = {}
            email_dict['DATA']['BODY'] = ''

            email_dict['DATA']['BODY'] += '<b>'
            email_dict['DATA']['BODY'] += 'Generación de Nomina empleados'
            email_dict['DATA']['BODY'] += '</b> <br/>'
            email_dict['DATA']['BODY'] += '%s' % (self.date_time.strftime('%Y-%m-%d %H:%M'))
            email_dict['DATA']['BODY'] += '<br/>'
            email_dict['DATA']['BODY'] += 'Empleado: %s' % (self.employee)
            email_dict['DATA']['BODY'] += '<br/>'
            email_dict['DATA']['BODY'] += 'Action: %s' % ('Generación comprobande de nómina')
            email_dict['DATA']['BODY'] += '<br/>'
            #email_dict['DATA']['BODY'] += 'URL: %s' % (event.urlpath)
            email_dict['DATA']['BODY'] += '<br/>'
            #email_dict['DATA']['BODY'] += 'Módulo: %s, Modelo: %s, ID Registro: %s' % (event.module, event.model_name, event.objectid)
            email_dict['DATA']['BODY'] += '<br/>'

            send_mail(email_dict)


    class Meta:
        ordering = ["employee", "-end_date"]
        app_label = 'payroll'
        verbose_name = 'Nómina'
        verbose_name_plural = 'Nominas'

class ItemPayroll(models.Model):
    licenser = models.ForeignKey('license.Licenser', default=1, on_delete=models.PROTECT)
    employee = models.ForeignKey('employees.Employee', on_delete=models.PROTECT)
    payroll = models.ForeignKey('Payroll', on_delete=models.CASCADE)
    item = models.ForeignKey('items.Item', on_delete=models.PROTECT)
    value = models.DecimalField(max_digits=14, decimal_places=2, verbose_name="Valor")
    entity = models.ForeignKey('entities.Entity', blank=True, null=True, on_delete=models.PROTECT, verbose_name="Entidad")
    event = models.OneToOneField('events.Event', blank=True, null=True, on_delete=models.PROTECT, verbose_name='Novedad')
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha y hora")

    def __str__(self):
        return "%s" % (self.name)

    class Meta:
        unique_together = [['licenser', 'employee', 'payroll', 'item']]
        ordering = ['licenser', 'employee', 'payroll', '-created']
        app_label = 'payroll'
        verbose_name = 'Item Nómina'
        verbose_name_plural = 'Items Nómina'

class PayrollProvision(models.Model):
    licenser = models.ForeignKey('license.Licenser', default=1, on_delete=models.PROTECT)
    employee = models.ForeignKey('employees.Employee', on_delete=models.PROTECT)
    payroll = models.ForeignKey('Payroll', on_delete=models.CASCADE)
    # item_payroll = models.ForeignKey('ItemPayroll', on_delete=models.CASCADE)
    provision = models.ForeignKey('items.Provision', on_delete=models.PROTECT)
    value = models.DecimalField(max_digits=14, decimal_places=2, verbose_name="Valor")
    year = models.CharField(max_length=4, default=None, blank=True, null=True, verbose_name="Año en vigencia")
    paid = models.BooleanField(verbose_name='Pagada?', default=False)

    def __str__(self):
        return f'{self.employee} {self.provision}'

    def valid_payrollprovision(self, payroll, obj_item, value_provision):
        current_date = datetime.date.today()

        try:
            payrollprovision = PayrollProvision.objects.get(employee=payroll.employee.id, provision=obj_item.destine.code, paid=False)
            payrollprovision.value += value_provision
            payrollprovision.save()
        except ObjectDoesNotExist:
            #payrollprovision = PayrollProvision()
            self.licenser = payroll.licenser
            self.employee = payroll.employee
            self.payroll = payroll
            if (obj_item.destine.code == '00003' or obj_item.destine.code == '00004'):
                self.year = str(current_date.year)
            # self.item_payroll = itempayroll
            self.provision = obj_item.destine #Provision.objects.get(code=provision_id)
            self.value = value_provision
            self.save()

    class Meta:
        ordering = ['employee']
        unique_together = [["licenser", "employee", "payroll", "provision"]]
        app_label = 'payroll'
        verbose_name = 'Provisión Acumulada'
        verbose_name_plural = 'Provisiones Acumuladas'

