#from backend.apps.employees.metadata import CreateEmployeeRequestMetadata
from backend.apps.employees.models import Employee
from django.db.models import Q
from backend.apps.payroll.models import Payroll
from backend.apps.general.models import Parameter
from rest_framework.response import Response
from rest_framework import status, viewsets
from rest_framework import serializers
from rest_framework.permissions import IsAdminUser
from backend.auth.accounts.models import Account

from backend.core.views import GenericQueryViewSet, GenericModelViewSet, GenericTableViewSet
from backend.core.permissions import IsAdminOrReadOnly
from backend.apps.payroll.serializers import (
    PayrollQuerySerializer,
    PayrollTableSerializer,
    PayrollDetailSerializer,
    PayrollSerializer, 
    PayrollPDFSerializer
    )

class PayrollTableViewSet(GenericTableViewSet):
    app_name = 'payroll'
    model_name = 'Payroll'
    queryset = Payroll.objects.all()
    serializer_class = PayrollTableSerializer
    permission_classes = (IsAdminOrReadOnly, )

    def get_queryset(self):
        queryset = GenericTableViewSet.get_queryset(self)
        context = self.get_serializer_context()
        request = context['request']
        actual_account = Account.getAccount(request.user)

        if actual_account.type.id == 'E':
            queryset = queryset.filter(employee__account=actual_account)
        elif actual_account.type.id == 'J':
            queryset = queryset.filter(Q(employee__account=actual_account)|Q(employee__immediate_boss__account=actual_account))
        else:
            queryset = queryset
        
        return queryset

    # def get_queryset(self):
    #     """
    #     This view should return a list of all the purchases
    #     for the currently authenticated user.
    #     """
    #     user = self.request.user
    #     role_user = RoleUser.objects.get(account=user)
    #     if role_user.role.id != 1:
    #         employee = Employee.objects.get(name='Alejandro')
    #         return Payroll.objects.filter(employee=employee)
    #     return Payroll.objects.all()


class PayrollDetailViewSet(viewsets.ReadOnlyModelViewSet):
    app_name = 'payroll'
    model_name = 'Payroll'
    queryset = Payroll.objects.all()
    serializer_class = PayrollDetailSerializer
    permission_classes = (IsAdminOrReadOnly, )


class PayrollDetailViewSet(viewsets.ReadOnlyModelViewSet):
    app_name = 'payroll'
    model_name = 'Payroll'
    queryset = Payroll.objects.all()
    serializer_class = PayrollDetailSerializer
    permission_classes = (IsAdminOrReadOnly, )


class PayrollQueryViewSet(GenericQueryViewSet):
    app_name = 'payroll'
    model_name = 'Payroll'
    queryset = Payroll.objects.all()
    serializer_class = PayrollQuerySerializer
    permission_classes = (IsAdminOrReadOnly, )


class PayrollViewSet(GenericModelViewSet):
    app_name = 'payroll'
    model_name = 'Payroll'
    queryset = Payroll.objects.all()
    serializer_class = PayrollSerializer
    permission_classes = (IsAdminOrReadOnly, )

    def create(self, request):
        actual_account = Account.getAccount(request.user)
        data = {}
        validate_response = '1'
        obj_payroll, list_errors = Payroll.save_data(request.data, actual_account)

        if obj_payroll is None or len(list_errors) != 0:
            validate_response = '0'
            list_errors.append('la nomina no se pudo guardar')

        if validate_response == '1':
            return Response(data=data, status=status.HTTP_201_CREATED)

        return Response(data={'VAL': validate_response, 'ERROR': list_errors}, status=status.HTTP_400_BAD_REQUEST)


class PayrollPDFViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Payroll.objects.all()
    serializer_class = PayrollPDFSerializer
    permission_classes = (IsAdminOrReadOnly, )
