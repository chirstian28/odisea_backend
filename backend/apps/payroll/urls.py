from django.urls import path, include
from backend.apps.payroll import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'payrolltable', views.PayrollTableViewSet, basename='payrolltable')
router.register(r'payrollquery', views.PayrollQueryViewSet, basename='payrollquery')
router.register(r'detail', views.PayrollDetailViewSet, basename='detail')
router.register(r'payroll', views.PayrollViewSet, basename='payroll')
router.register(r'showpayrolltopdf', views.PayrollPDFViewSet, basename='showpayrolltopdf')

urlpatterns = [
    #path('payroll/', views.PayrollView.as_view()),
    #path('detail/<int:pk>/', views.PayrollDetailViewSet),
]

urlpatterns += router.urls