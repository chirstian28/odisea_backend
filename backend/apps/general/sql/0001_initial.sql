INSERT INTO general_period (id, name, days) VALUES ('01', 'DIARIO', 1);
INSERT INTO general_period (id, name, days) VALUES ('07', 'SEMANAL', 7);
INSERT INTO general_period (id, name, days) VALUES ('14', 'CATORCENA', 14);
INSERT INTO general_period (id, name, days) VALUES ('15', 'QUINCENAL', 15);
INSERT INTO general_period (id, name, days) VALUES ('30', 'MENSUAL', 30);
