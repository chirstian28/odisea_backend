INSERT INTO general_status (id, name) VALUES ('01', 'ACTIVO');
INSERT INTO general_status (id, name) VALUES ('02', 'INCAPACITADO');
INSERT INTO general_status (id, name) VALUES ('03', 'EN VACACIONES');
INSERT INTO general_status (id, name) VALUES ('04', 'SUSPENDIDO');
INSERT INTO general_status (id, name) VALUES ('05', 'RETIRADO');
INSERT INTO general_status (id, name) VALUES ('06', 'LIQUIDADO');