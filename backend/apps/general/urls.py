# -*- coding: utf-8 -*-
from django.urls import path, include
from backend.apps.general import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'parameter', views.ParameterViewSet, basename='parameter')

urlpatterns = [

]

urlpatterns += router.urls