from rest_framework import serializers
from backend.core.generic_serializer import GenericQuerySerializer
from backend.auth.accounts.models import Account
from backend.components.auditor.models import AuditorParameter
from .models import Parameter
from rest_framework.fields import ValidationError

"""

class ParameterSerializer(serializers.ModelSerializer):

    def update(self, instance, validated_data):
        request = self.context['request']
        old_serializer_data = ParameterSerializer(instance).data
        serializer = super(ParameterSerializer, self).update(instance, validated_data)
        instance_data = ParameterSerializer(instance)
        AuditorParameter.registerEvent(request, 'general', 'Parameter', instance_data.data, instance.id,
                                       AuditorParameter.UPDATE, before_register_object=old_serializer_data,
                                       action='UPDATE_PARAMETER')
        return serializer

    class Meta:
        model = Parameter
        fields = ('logo', 'nit', 'name', 'address', 'compensation_entity', 
                  'arl', 'payment_period', 'bank', 'bank_account', 
                  'salarial_base',)


class ParameterQuerySerializer(GenericQuerySerializer):

    class Meta:
        model = Parameter
        fields = ('id', 'name',)

"""


class ParameterSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        request = self.context['request']
        actual_account = Account.getAccount(request.user)
        if actual_account is not None:
            instance, instance_created = Parameter.objects.get_or_create(licenser=actual_account.licenser, **validated_data)
            instance_data = ParameterSerializer(instance)
            AuditorParameter.registerEvent(request, 'general', 'Parameter', instance_data.data, instance.id, AuditorParameter.CREATE, action='CREATE_PARAMETER')
        else:
            raise ValidationError("No se encuentra un inicio de sesión")
        return instance

    def update(self, instance, validated_data):
        request = self.context['request']
        old_serializer_data = ParameterSerializer(instance).data
        serializer = super(ParameterSerializer, self).update(instance, validated_data)
        instance_data = ParameterSerializer(instance)
        AuditorParameter.registerEvent(request, 'general', 'Parameter', instance_data.data, instance.id,
                                       AuditorParameter.UPDATE, before_register_object=old_serializer_data,
                                       action='UPDATE_PARAMETER')
        return serializer

    class Meta:
        model = Parameter
        fields = ('year', 'name', "value",)