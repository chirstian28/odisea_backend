from django.db import models

class Status(models.Model):
    id = models.CharField(max_length=5, verbose_name='ID', primary_key=True)
    name = models.CharField(max_length=40, verbose_name='Nombre')

    def __str__(self):
        return "%s" % (self.name)

    class Meta:
        ordering = ['name']
        app_label = 'general'
        verbose_name = 'Estado'
        verbose_name_plural = 'Estados'

class Period(models.Model):
    id = models.CharField(max_length=5, verbose_name='ID', primary_key=True)
    name = models.CharField(max_length=40, verbose_name='Nombre')
    days = models.IntegerField(verbose_name='Días')

    def __str__(self):
        return "%s" % (self.name)

    class Meta:
        ordering = ['name']
        app_label = 'general'
        verbose_name = 'Periodo'
        verbose_name_plural = 'Periodos'

class Company(models.Model):
    licenser = models.OneToOneField('license.Licenser', on_delete=models.PROTECT)
    logo = models.FileField(verbose_name="Logo Empresa", null=True, blank=True)
    nit = models.CharField(max_length=40, verbose_name='NIT')
    name = models.CharField(max_length=100, verbose_name='Nombre')
    address = models.CharField(max_length=255, verbose_name='Dirección')
    compensation_entity = models.ForeignKey('entities.CompesationEntity', on_delete=models.PROTECT, verbose_name="Caja de Compensación", null=True, blank=True)
    arl = models.ForeignKey('entities.Arl', on_delete=models.PROTECT, verbose_name="ARL", null=True, blank=True)
    payment_period = models.ForeignKey('Period', on_delete=models.PROTECT, verbose_name="Periodo de Pago", null=True, blank=True)
    bank = models.ForeignKey('entities.Bank', on_delete=models.PROTECT, verbose_name="Banco", null=True, blank=True)
    bank_account = models.CharField(max_length=40, verbose_name='Cuenta Bancaria', null=True, blank=True)
    salarial_base = models.IntegerField(verbose_name="Base Salarial", null=True, blank=True)

    def __str__(self):
        return "%s" % (self.licenser.name)

    class Meta:
        app_label = 'general'
        verbose_name = 'Empresa'
        verbose_name_plural = 'Empresas'

class Parameter(models.Model):
    licenser = models.ForeignKey('license.Licenser', default=1, on_delete=models.PROTECT)
    year = models.CharField(max_length=4, verbose_name="Año en vigencia")
    name = models.CharField(max_length=30, default='', verbose_name="Nombre")
    value = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True, default=0, verbose_name="Valor")

    def __str__(self):
        return f"{self.year}" 

    class Meta:
        ordering = ["licenser", "-year"]
        unique_together = [["licenser", "year"]]
        app_label = 'general'
        verbose_name = 'Parámetro'
        verbose_name_plural = 'Parámetros'

