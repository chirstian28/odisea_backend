from rest_framework import serializers
from .models import Parameter
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from backend.auth.accounts.models import Account
from backend.core.views import GenericTableViewSet, GenericQueryViewSet, GenericModelViewSet
from backend.core.permissions import IsAdminOrReadOnly
from .serializers import ParameterSerializer


class ParameterViewSet(GenericModelViewSet):
    app_name = 'items'
    model_name = 'Provision'
    queryset = Parameter.objects.all()
    serializer_class = ParameterSerializer
    permission_classes = (IsAdminOrReadOnly, )