# -*- coding: utf-8 -*-
from backend.core.metadata import GenericTableMetadata

class EntityTableMetadata(GenericTableMetadata):

    def determine_metadata(self, request, view):
        meta_data_dict = GenericTableMetadata.determine_metadata(self, request, view)

        del meta_data_dict['buttons']['ADD']
        meta_data_dict['actions']['BASIC']['UPDATE']['disable'] = True
        meta_data_dict['actions']['BASIC']['DELETE']['disable'] = True
        meta_data_dict['title'] = 'Entidades'
        return meta_data_dict

