from django.db import models

class EntityType(models.Model):
    id = models.CharField(max_length=10, primary_key=True, verbose_name="ID")
    name = models.CharField(max_length=70, verbose_name="Nombre")

    def __str__(self):
        return f'{self.name}'

    class Meta:
        ordering = ['name']
        app_label = 'entities'
        verbose_name = 'Tipo Entidad'
        verbose_name_plural = 'Tipos de Entidades'

class EntityClass(models.Model):
    id = models.CharField(max_length=10, primary_key=True, verbose_name="ID")
    name = models.CharField(max_length=70, verbose_name="Nombre")

    def __str__(self):
        return f'{self.name}'

    class Meta:
        ordering = ['name']
        app_label = 'entities'
        verbose_name = 'Clase de Entidad'
        verbose_name_plural = 'Clases de Entidades'

class Regime(models.Model):
    id = models.CharField(max_length=10, primary_key=True, verbose_name="ID")
    name = models.CharField(max_length=70, verbose_name="Nombre")

    def __str__(self):
        return f'{self.name}'

    class Meta:
        ordering = ['name']
        app_label = 'entities'
        verbose_name = 'Regimen'
        verbose_name_plural = 'Regímenes'

class Entity(models.Model):
    id = models.CharField(max_length=10, primary_key=True, verbose_name="Codigo de la entidad")
    name = models.CharField(max_length=255, verbose_name="Nombre")
    nit = models.CharField(max_length=20, verbose_name="NIT", null=True, blank=True)
    entity_type = models.ForeignKey('EntityType', verbose_name='Tipo de Entidad', on_delete=models.PROTECT)

    def __str__(self):
        return f'{self.name}'

    class Meta:
        ordering = ['name']
        app_label = 'entities'
        verbose_name = 'Entidad'
        verbose_name_plural = 'Entidad'

class CompesationEntity(Entity):
    short_name = models.CharField(max_length=255, verbose_name="Nombre Corto")
    state = models.ForeignKey('localization.state', verbose_name='Departamento', null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return f'{self.short_name}'

    def save(self, *args, **kwargs):
        try: 
            CompesationEntity.objects.get(pk=self.id)
        except CompesationEntity.DoesNotExist:
            self.entity_type = EntityType.objects.get(pk='CCF')
        super(CompesationEntity, self).save()

    class Meta:
        ordering = ['id']
        app_label = 'entities'
        verbose_name = 'Caja de Compensación'
        verbose_name_plural = 'Cajas de Compensación'

class Eps(Entity):
    entity_class = models.ForeignKey('EntityClass', verbose_name='Clase de Entidad', on_delete=models.PROTECT)
    regime = models.ForeignKey('Regime', verbose_name='Regimen', on_delete=models.PROTECT)
    short_name = models.CharField(max_length=255, verbose_name="Nombre Corto")

    def save(self, *args, **kwargs):
        try: 
            Eps.objects.get(pk=self.id)
        except Eps.DoesNotExist:
            self.entity_type = EntityType.objects.get(pk='EPS')
        super(Eps, self).save()

    class Meta:
        ordering = ['name']
        app_label = 'entities'
        verbose_name = 'Eps'
        verbose_name_plural = 'EPS'

class Parafiscal(Entity):

    def save(self, *args, **kwargs):
        try: 
            Parafiscal.objects.get(pk=self.id)
        except Parafiscal.DoesNotExist:
            self.entity_type = EntityType.objects.get(pk='PAR')
        super(Parafiscal, self).save()

    class Meta:
        ordering = ['name']
        app_label = 'entities'
        verbose_name = 'Parafiscal'
        verbose_name_plural = 'Parafiscales'

class Taxes(Entity):

    def save(self, *args, **kwargs):
        try: 
            Taxes.objects.get(pk=self.id)
        except Taxes.DoesNotExist:
            self.entity_type = EntityType.objects.get(pk='IMP')
        super(Taxes, self).save()

    class Meta:
        ordering = ['name']
        app_label = 'entities'
        verbose_name = 'Impuestos'
        verbose_name_plural = 'Impuestos'

class Arl(Entity):

    def save(self, *args, **kwargs):
        try: 
            Arl.objects.get(pk=self.id)
        except Arl.DoesNotExist:
            self.entity_type = EntityType.objects.get(pk='ARL')
        super(Arl, self).save()

    class Meta:
        ordering = ['name']
        app_label = 'entities'
        verbose_name = 'Arl'
        verbose_name_plural = 'Arl'

class PensionFund(Entity):

    def save(self, *args, **kwargs):
        try: 
            PensionFund.objects.get(pk=self.id)
        except PensionFund.DoesNotExist:
            self.entity_type = EntityType.objects.get(pk='AFP')
        super(PensionFund, self).save()

    class Meta:
        ordering = ['id']
        app_label = 'entities'
        verbose_name = 'Fondo de Pensiones'
        verbose_name_plural = 'Fondos de Pensiones'

class Bank(Entity):

    def save(self, *args, **kwargs):
        try: 
            Bank.objects.get(pk=self.id)
        except Bank.DoesNotExist:
            self.entity_type = EntityType.objects.get(pk='FIN')
        super(Bank, self).save()

    class Meta:
        ordering = ['id']
        app_label = 'entities'
        verbose_name = 'Banco'
        verbose_name_plural = 'Bancos'
