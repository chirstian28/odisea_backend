from django.apps import AppConfig


class EntitiesConfig(AppConfig):
    name = 'backend.apps.entities'
