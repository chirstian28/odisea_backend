INSERT INTO entities_entitytype (id, name) VALUES ('EPS','Entidad Promotora de Salud');
INSERT INTO entities_entitytype (id, name) VALUES ('PAR','Parafiscales');
INSERT INTO entities_entitytype (id, name) VALUES ('IMP','Impuestos');
INSERT INTO entities_entitytype (id, name) VALUES ('ARP','Administradora de Riesgos Profesionales');
INSERT INTO entities_entitytype (id, name) VALUES ('AFP','Administradora de Fondos Pensionales');
INSERT INTO entities_entitytype (id, name) VALUES ('CCF','Cajas de Compensación Familiar');
INSERT INTO entities_entitytype (id, name) VALUES ('FIN','Financiero');
INSERT INTO entities_entitytype (id, name) VALUES ('ARL','Administradora de Riesgos Laborales');

INSERT INTO entities_entityclass (id, name) VALUES ('OR', 'ENTIDADES ORIGINALES');
INSERT INTO entities_entityclass (id, name) VALUES ('CM', 'ENTIDAD COMPLEMENTARIA MOVILIDAD');

INSERT INTO entities_regime (id, name) VALUES ('CNT', 'RÉGIMEN CONTRIBUTIVO');
INSERT INTO entities_regime (id, name) VALUES ('SBS', 'RÉGIMEN SUBSIDIADO');
INSERT INTO entities_regime (id, name) VALUES ('EPC', 'ENTIDADES PLANES COMPLEMENTARIOS DE SALUD');
INSERT INTO entities_regime (id, name) VALUES ('ERE', 'ENTIDADES RÉGIMEN DE EXCEPCIÓN');

INSERT INTO entities_entity (id, name, nit, entity_type_id) VALUES ('EPS010', 'EPS Y MEDICINA PREPAGADA SURAMERICANA S.A.','800088702-2','EPS');

INSERT INTO entities_eps (entity_ptr_id, short_name, regime_id, entity_class_id) VALUES ('EPS010', 'EPS Sura', 'CNT', 'OR');

INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('PASENA', 'SENA', 'PAR');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('PAICBF', 'INSTITUTO COLOMBIANO DE BIENESTAR FAMILIAR', 'PAR');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('PAMIED', 'MINISTERIO DE EDUCACION', 'PAR');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('PAESAP', 'ESCUELA SUPERIOR DE ADMINISTRACION PUBLICA', 'PAR');

INSERT INTO entities_parafiscal (entity_ptr_id) VALUES ('PASENA');
INSERT INTO entities_parafiscal (entity_ptr_id) VALUES ('PAICBF');
INSERT INTO entities_parafiscal (entity_ptr_id) VALUES ('PAMIED');
INSERT INTO entities_parafiscal (entity_ptr_id) VALUES ('PAESAP');

INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('DIAN', 'DIAN', 'IMP');

INSERT INTO entities_taxes (entity_ptr_id) VALUES ('DIAN');

INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('14-1', 'ASEGURADORA DE VIDA COLSEGUROS', 'ARP');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('14-5', 'COMPANIA AGRICOLA DE SEGUROS DE VIDA S.A.', 'ARP');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('14-7', 'CIA. DE SEGUROS BOLIVAR S.A.', 'ARP');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('14-8', 'COMPANIA DE SEGUROS DE VIDA AURORA', 'ARP');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('14-25', 'RIESGOS PROFESIONALES COLMENA S.A COMPANIA DE SEGUROS DE VIDA', 'ARP');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('14-4', 'SEGUROS DE VIDA COLPATRIA S.A.', 'ARP');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('14-26', 'BBVA SEGUROS DE VIDA COLOMBIA S.A', 'ARP');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('14-28', 'COMPANIA SURAMERICANA ADMINISTRADORA DE RIESGOS PROFESIONALES Y SEGUROS VIDA', 'ARP');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('14-23', 'LA PREVISORA VIDA S.A. COMPANIA DE SEGUROS', 'ARP');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('14-17', 'SEGUROS DE VIDA ALFA S.A.', 'ARP');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('14-19', 'SEGUROS DE VIDA DEL ESTADO S.A.', 'ARP');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('14-18', 'LIBERTY SEGUROS DE VIDA', 'ARP');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('14-29', 'LA EQUIDAD SEGUROS DE VIDA ORGANISMO COOPERATIVO - LA EQUIDAD VIDA', 'ARP');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('25-10', 'INSTITUTO DE SEGUROS SOCIALES I.S.S. RIESGOS PROFESIONALES', 'ARP');

INSERT INTO entities_arl (entity_ptr_id) VALUES ('14-1');
INSERT INTO entities_arl (entity_ptr_id) VALUES ('14-5');
INSERT INTO entities_arl (entity_ptr_id) VALUES ('14-7');
INSERT INTO entities_arl (entity_ptr_id) VALUES ('14-8');
INSERT INTO entities_arl (entity_ptr_id) VALUES ('14-25');
INSERT INTO entities_arl (entity_ptr_id) VALUES ('14-4');
INSERT INTO entities_arl (entity_ptr_id) VALUES ('14-26');
INSERT INTO entities_arl (entity_ptr_id) VALUES ('14-28');
INSERT INTO entities_arl (entity_ptr_id) VALUES ('14-23');
INSERT INTO entities_arl (entity_ptr_id) VALUES ('14-17');
INSERT INTO entities_arl (entity_ptr_id) VALUES ('14-19');
INSERT INTO entities_arl (entity_ptr_id) VALUES ('14-18');
INSERT INTO entities_arl (entity_ptr_id) VALUES ('14-29');
INSERT INTO entities_arl (entity_ptr_id) VALUES ('25-10');

INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('230201', 'PROTECCION', 'AFP');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('230301', 'PORVENIR', 'AFP');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('230501', 'HORIZONTE', 'AFP');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('230801', 'SANTANDER', 'AFP');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('230901', 'SKANDIA OBLIGATORIO', 'AFP');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('230904', 'SKANDIA ALTERNATIVO', 'AFP');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('231001', 'COLFONDOS', 'AFP');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('25-11', 'INSTITUTO DE SEGUROS SOCIALES I.S.S. PENSIONES', 'AFP');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('25-2', 'CAJA DE AUXILIOS Y PRESTACIONES DE LA ASOCIACION COLOMBIANA DE AVIADORES CIVILES ACDAC -CAXDAC', 'AFP');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('25-3', 'FONDO DE PREVISION SOCIAL DEL CONGRESO DE LA REPUBLICA FONPRECON', 'AFP');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('25-4', 'CAJA DE PREVISION SOCIAL DE COMUNICACIONES CAPRECOM', 'AFP');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('25-7', 'PENSIONES DE ANTIOQUIA', 'AFP');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('25-8', 'CAJA NACIONAL DE PREVISION SOCIAL CAJANAL', 'AFP');

INSERT INTO entities_pensionfund (entity_ptr_id) VALUES ('230201');
INSERT INTO entities_pensionfund (entity_ptr_id) VALUES ('230301');
INSERT INTO entities_pensionfund (entity_ptr_id) VALUES ('230501');
INSERT INTO entities_pensionfund (entity_ptr_id) VALUES ('230801');
INSERT INTO entities_pensionfund (entity_ptr_id) VALUES ('230901');
INSERT INTO entities_pensionfund (entity_ptr_id) VALUES ('230904');
INSERT INTO entities_pensionfund (entity_ptr_id) VALUES ('231001');
INSERT INTO entities_pensionfund (entity_ptr_id) VALUES ('25-11');
INSERT INTO entities_pensionfund (entity_ptr_id) VALUES ('25-2');
INSERT INTO entities_pensionfund (entity_ptr_id) VALUES ('25-3');
INSERT INTO entities_pensionfund (entity_ptr_id) VALUES ('25-4');
INSERT INTO entities_pensionfund (entity_ptr_id) VALUES ('25-7');
INSERT INTO entities_pensionfund (entity_ptr_id) VALUES ('25-8');

INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('000', 'BANCO DE LA REPÚBLICA', 'FIN');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('001', 'BANCO DE BOGOTÁ', 'FIN');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('002', 'BANCO POPULAR', 'FIN');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('006', 'BANCO CORPBANCA COLOMBIA S.A.', 'FIN');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('007', 'BANCOLOMBIA S.A.', 'FIN');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('009', 'CITIBANK COLOMBIA', 'FIN');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('010', 'BANCO GNB COLOMBIA S.A.', 'FIN');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('012', 'BANCO GNB SUDAMERIS COLOMBIA', 'FIN');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('013', 'BBVA COLOMBIA', 'FIN');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('014', 'HELM BANK', 'FIN');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('019', 'SCOTIABANK COLPATRIA', 'FIN');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('023', 'BANCO DE OCCIDENTE', 'FIN');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('031', 'BANCO DE COMERCIO EXTERIOR DE COLOMBIA S.A. (BANCOLDEX)', 'FIN');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('032', 'BANCO CAJA SOCIAL - BCSC S.A.', 'FIN');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('040', 'BANCO AGRARIO DE COLOMBIA S.A.', 'FIN');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('051', 'BANCO DAVIVIENDA S.A.', 'FIN');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('052', 'BANCO AV VILLAS', 'FIN');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('053', 'BANCO WWB S.A.', 'FIN');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('058', 'BANCO PROCREDIT', 'FIN');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('059', 'BANCAMIA', 'FIN');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('060', 'BANCO PICHINCHA S.A.', 'FIN');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('061', 'BANCOOMEVA', 'FIN');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('062', 'BANCO FALABELLA S.A.', 'FIN');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('063', 'BANCO FINANDINA S.A.', 'FIN');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('065', 'BANCO SANTANDER DE NEGOCIOS COLOMBIA S.A. - BANCO SANTANDER', 'FIN');
INSERT INTO entities_entity (id, name, entity_type_id) VALUES ('066', 'BANCO COOPERATIVO COOPCENTRAL', 'FIN');

INSERT INTO entities_bank (entity_ptr_id) VALUES ('000');
INSERT INTO entities_bank (entity_ptr_id) VALUES ('001');
INSERT INTO entities_bank (entity_ptr_id) VALUES ('002');
INSERT INTO entities_bank (entity_ptr_id) VALUES ('006');
INSERT INTO entities_bank (entity_ptr_id) VALUES ('007');
INSERT INTO entities_bank (entity_ptr_id) VALUES ('009');
INSERT INTO entities_bank (entity_ptr_id) VALUES ('010');
INSERT INTO entities_bank (entity_ptr_id) VALUES ('012');
INSERT INTO entities_bank (entity_ptr_id) VALUES ('013');
INSERT INTO entities_bank (entity_ptr_id) VALUES ('014');
INSERT INTO entities_bank (entity_ptr_id) VALUES ('019');
INSERT INTO entities_bank (entity_ptr_id) VALUES ('023');
INSERT INTO entities_bank (entity_ptr_id) VALUES ('031');
INSERT INTO entities_bank (entity_ptr_id) VALUES ('032');
INSERT INTO entities_bank (entity_ptr_id) VALUES ('040');
INSERT INTO entities_bank (entity_ptr_id) VALUES ('051');
INSERT INTO entities_bank (entity_ptr_id) VALUES ('052');
INSERT INTO entities_bank (entity_ptr_id) VALUES ('053');
INSERT INTO entities_bank (entity_ptr_id) VALUES ('058');
INSERT INTO entities_bank (entity_ptr_id) VALUES ('059');
INSERT INTO entities_bank (entity_ptr_id) VALUES ('060');
INSERT INTO entities_bank (entity_ptr_id) VALUES ('061');
INSERT INTO entities_bank (entity_ptr_id) VALUES ('062');
INSERT INTO entities_bank (entity_ptr_id) VALUES ('063');
INSERT INTO entities_bank (entity_ptr_id) VALUES ('065');
INSERT INTO entities_bank (entity_ptr_id) VALUES ('066');
