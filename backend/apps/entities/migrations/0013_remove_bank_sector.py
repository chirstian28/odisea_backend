# Generated by Django 2.2.17 on 2020-11-24 18:23

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entities', '0012_arl_bank_pensionfund_sector'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bank',
            name='sector',
        ),
    ]
