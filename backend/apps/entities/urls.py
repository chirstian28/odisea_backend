# -*- coding: utf-8 -*-
from backend.apps.entities import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'entitytable', views.EntityTableViewSet, basename='entitytable')
router.register(r'entityquery', views.EntityQueryViewSet, basename='entityquery')
router.register(r'compensationentitytable', views.CompesationEntityTableViewSet, basename='compensationentitytable')
router.register(r'compensationentityquery', views.CompesationEntityQueryViewSet, basename='compensationentityquery')
router.register(r'compensationentity', views.CompesationEntityViewSet, basename='compensationentity')
router.register(r'epstable', views.EpsTableViewSet, basename='epstable')
router.register(r'epsquery', views.EpsQueryViewSet, basename='epsquery')
router.register(r'eps', views.EpsViewSet, basename='eps')
router.register(r'parafiscaltable', views.ParafiscalTableViewSet, basename='parafiscaltable')
router.register(r'parafiscalquery', views.ParafiscalQueryViewSet, basename='parafiscalquery')
router.register(r'parafiscal', views.ParafiscalViewSet, basename='parafiscal')
router.register(r'taxestable', views.TaxesTableViewSet, basename='taxestable')
router.register(r'taxesquery', views.TaxesQueryViewSet, basename='taxesquery')
router.register(r'taxes', views.TaxesViewSet, basename='taxes')
router.register(r'arltable', views.ArlTableViewSet, basename='arltable')
router.register(r'arlquery', views.ArlQueryViewSet, basename='arlquery')
router.register(r'arl', views.ArlViewSet, basename='arl')
router.register(r'pensionfundtable', views.PensionFundTableViewSet, basename='pensionfundtable')
router.register(r'pensionfundquery', views.PensionFundQueryViewSet, basename='pensionfundquery')
router.register(r'pensionfund', views.PensionFundViewSet, basename='pensionfund')
router.register(r'banktable', views.BankTableViewSet, basename='banktable')
router.register(r'bankquery', views.BankQueryViewSet, basename='bankquery')
router.register(r'bank', views.BankViewSet, basename='bank')

urlpatterns = [

    ]

urlpatterns += router.urls
