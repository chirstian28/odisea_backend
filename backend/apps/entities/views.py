from .models import Entity, CompesationEntity, Eps, Parafiscal, Taxes, Arl, \
    PensionFund, Bank
from .serializers import EntityTableSerializer, EntityQuerySerializer, \
    CompesationEntityTableSerializer, CompesationEntityQuerySerializer, \
    CompesationEntitySerializer, EpsTableSerializer, EpsQuerySerializer, \
    EpsSerializer, ParafiscalTableSerializer, ParafiscalQuerySerializer, \
    ParafiscalSerializer, TaxesTableSerializer, TaxesQuerySerializer, \
    TaxesSerializer, ArlTableSerializer, ArlQuerySerializer, ArlSerializer, \
    PensionFundTableSerializer, PensionFundQuerySerializer, \
    PensionFundSerializer, BankTableSerializer, BankQuerySerializer, \
    BankSerializer
from .metadata import EntityTableMetadata
from .filters import EntityTableFilter, CompesationEntityTableFilter, \
    EpsTableFilter
from backend.core.views import GenericQueryViewSet, GenericModelViewSet, \
    GenericTableViewSet
from backend.core.permissions import IsAdminOrReadOnly

class EntityTableViewSet(GenericTableViewSet):
    app_name = 'entities'
    model_name = 'Entity'
    queryset = Entity.objects.all()
    serializer_class = EntityTableSerializer
    metadata_class = EntityTableMetadata
    filter_class = EntityTableFilter
    filter_licenser = False

class EntityQueryViewSet(GenericQueryViewSet):
    app_name = 'entities'
    model_name = 'Entity'
    queryset = Entity.objects.all()
    serializer_class = EntityQuerySerializer
    filter_class = EntityTableFilter
    filter_licenser = False

class CompesationEntityTableViewSet(GenericTableViewSet):
    app_name = 'entities'
    model_name = 'CompesationEntity'
    queryset = CompesationEntity.objects.all()
    serializer_class = CompesationEntityTableSerializer
    filter_class = CompesationEntityTableFilter
    filter_licenser = False
    
class CompesationEntityQueryViewSet(GenericQueryViewSet):
    app_name = 'entities'
    model_name = 'CompesationEntity'
    queryset = CompesationEntity.objects.all()
    serializer_class = CompesationEntityQuerySerializer
    filter_licenser = False

class CompesationEntityViewSet(GenericModelViewSet):
    app_name = 'entities'
    model_name = 'CompesationEntity'
    queryset = CompesationEntity.objects.all()
    serializer_class = CompesationEntitySerializer
    permission_classes = (IsAdminOrReadOnly, )
    filter_licenser = False
        
class EpsTableViewSet(GenericTableViewSet):
    app_name = 'entities'
    model_name = 'Eps'
    queryset = Eps.objects.all()
    serializer_class = EpsTableSerializer
    filter_class = EpsTableFilter
    filter_licenser = False
    
class EpsQueryViewSet(GenericQueryViewSet):
    app_name = 'entities'
    model_name = 'Eps'
    queryset = Eps.objects.all()
    serializer_class = EpsQuerySerializer
    filter_licenser = False

class EpsViewSet(GenericModelViewSet):
    app_name = 'entities'
    model_name = 'Eps'
    queryset = Eps.objects.all()
    serializer_class = EpsSerializer
    permission_classes = (IsAdminOrReadOnly, )
    filter_licenser = False
        
class ParafiscalTableViewSet(GenericTableViewSet):
    app_name = 'entities'
    model_name = 'Parafiscal'
    queryset = Parafiscal.objects.all()
    serializer_class = ParafiscalTableSerializer
    filter_licenser = False
    
class ParafiscalQueryViewSet(GenericQueryViewSet):
    app_name = 'entities'
    model_name = 'Parafiscal'
    queryset = Parafiscal.objects.all()
    serializer_class = ParafiscalQuerySerializer
    filter_licenser = False

class ParafiscalViewSet(GenericModelViewSet):
    app_name = 'entities'
    model_name = 'Parafiscal'
    queryset = Parafiscal.objects.all()
    serializer_class = ParafiscalSerializer
    permission_classes = (IsAdminOrReadOnly, )
    filter_licenser = False
        
class TaxesTableViewSet(GenericTableViewSet):
    app_name = 'entities'
    model_name = 'Taxes'
    queryset = Taxes.objects.all()
    serializer_class = TaxesTableSerializer
    filter_licenser = False
    
class TaxesQueryViewSet(GenericQueryViewSet):
    app_name = 'entities'
    model_name = 'Taxes'
    queryset = Taxes.objects.all()
    serializer_class = TaxesQuerySerializer
    filter_licenser = False

class TaxesViewSet(GenericModelViewSet):
    app_name = 'entities'
    model_name = 'Taxes'
    queryset = Taxes.objects.all()
    serializer_class = TaxesSerializer
    permission_classes = (IsAdminOrReadOnly, )
    filter_licenser = False
         
class ArlTableViewSet(GenericTableViewSet):
    app_name = 'entities'
    model_name = 'Arl'
    queryset = Arl.objects.all()
    serializer_class = ArlTableSerializer
    filter_licenser = False
    
class ArlQueryViewSet(GenericQueryViewSet):
    app_name = 'entities'
    model_name = 'Arl'
    queryset = Arl.objects.all()
    serializer_class = ArlQuerySerializer
    filter_licenser = False

class ArlViewSet(GenericModelViewSet):
    app_name = 'entities'
    model_name = 'Arl'
    queryset = Arl.objects.all()
    serializer_class = ArlSerializer
    permission_classes = (IsAdminOrReadOnly, )
    filter_licenser = False
        
class PensionFundTableViewSet(GenericTableViewSet):
    app_name = 'entities'
    model_name = 'PensionFund'
    queryset = PensionFund.objects.all()
    serializer_class = PensionFundTableSerializer
    filter_licenser = False
    
class PensionFundQueryViewSet(GenericQueryViewSet):
    app_name = 'entities'
    model_name = 'PensionFund'
    queryset = PensionFund.objects.all()
    serializer_class = PensionFundQuerySerializer
    filter_licenser = False

class PensionFundViewSet(GenericModelViewSet):
    app_name = 'entities'
    model_name = 'PensionFund'
    queryset = PensionFund.objects.all()
    serializer_class = PensionFundSerializer
    permission_classes = (IsAdminOrReadOnly, )
    filter_licenser = False
        
class BankTableViewSet(GenericTableViewSet):
    app_name = 'entities'
    model_name = 'Bank'
    queryset = Bank.objects.all()
    serializer_class = BankTableSerializer
    filter_licenser = False
    
class BankQueryViewSet(GenericQueryViewSet):
    app_name = 'entities'
    model_name = 'Bank'
    queryset = Bank.objects.all()
    serializer_class = BankQuerySerializer
    filter_licenser = False

class BankViewSet(GenericModelViewSet):
    app_name = 'entities'
    model_name = 'Bank'
    queryset = Bank.objects.all()
    serializer_class = BankSerializer
    permission_classes = (IsAdminOrReadOnly, )
    filter_licenser = False
        