# -*- coding: utf-8 -*-
from rest_framework import serializers
from backend.core.generic_serializer import GenericQuerySerializer
from .models import Entity, CompesationEntity, Eps, Parafiscal, Taxes, Arl, \
    PensionFund, Bank
from backend.components.auditor.models import AuditorParameter

class EntityTableSerializer(serializers.ModelSerializer):
    entity_type = serializers.CharField(label='Tipo Entidad')

    class Meta:
        model = Entity
        fields = ('id', 'entity_type', 'nit', 'name', )
        search_fields = ['^name', "^short_name"]
        filter_fields = ("entity_type",)

class EntityQuerySerializer(GenericQuerySerializer):

    class Meta:
        model = Entity
        fields = ('id', 'name',)

class CompesationEntityTableSerializer(serializers.ModelSerializer):
    state = serializers.CharField(label='Departamento')

    class Meta:
        model = CompesationEntity
        fields = ('id', 'nit', "short_name", 'name', "state", )
        search_fields = ['^name', "^short_name"]
        filter_fields = ("state",)
        
class CompesationEntityQuerySerializer(GenericQuerySerializer):

    class Meta:
        model = CompesationEntity
        fields = ('id', 'name',)

class CompesationEntitySerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        request = self.context['request']
        instance, instance_created = CompesationEntity.objects.get_or_create(**validated_data)
        instance_data = CompesationEntitySerializer(instance)
        AuditorParameter.registerEvent(request, 'entities', 'CompesationEntity', instance_data.data, instance.id, AuditorParameter.CREATE, action='CREATE_COMPESATIONENTITY')
        return instance

    def update(self, instance, validated_data):
        request = self.context['request']
        old_serializer_data = CompesationEntitySerializer(instance).data
        serializer = super(CompesationEntitySerializer, self).update(instance, validated_data)
        instance_data = CompesationEntitySerializer(instance)
        AuditorParameter.registerEvent(request, 'entities', 'CompesationEntity', instance_data.data, instance.id,
                                       AuditorParameter.UPDATE, before_register_object=old_serializer_data,
                                       action='UPDATE_COMPESATIONENTITY')
        return serializer

    class Meta:
        model = CompesationEntity
        fields = ('id', 'nit', "short_name", 'name', "state", )

class EpsTableSerializer(serializers.ModelSerializer):
    entity_class = serializers.CharField(label='Clase de Entidad')
    regime = serializers.CharField(label='Régimen')

    class Meta:
        model = Eps
        fields = ('id', 'nit', "short_name", 'name', "entity_class", "regime", )
        search_fields = ['^name', "^short_name"]
        filter_fields = ("entity_class", "regime", )
        
class EpsQuerySerializer(GenericQuerySerializer):

    class Meta:
        model = Eps
        fields = ('id', 'name',)

class EpsSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        request = self.context['request']
        instance, instance_created = Eps.objects.get_or_create(**validated_data)
        instance_data = EpsSerializer(instance)
        AuditorParameter.registerEvent(request, 'entities', 'Eps', instance_data.data, instance.id, AuditorParameter.CREATE, action='CREATE_EPS')
        return instance

    def update(self, instance, validated_data):
        request = self.context['request']
        old_serializer_data = EpsSerializer(instance).data
        serializer = super(EpsSerializer, self).update(instance, validated_data)
        instance_data = EpsSerializer(instance)
        AuditorParameter.registerEvent(request, 'entities', 'Eps', instance_data.data, instance.id,
                                       AuditorParameter.UPDATE, before_register_object=old_serializer_data,
                                       action='UPDATE_EPS')
        return serializer

    class Meta:
        model = Eps
        fields = ('id', 'nit', "short_name", 'name', "entity_class", "regime", )

class ParafiscalTableSerializer(serializers.ModelSerializer):

    class Meta:
        model = Parafiscal
        fields = ('id', 'nit', 'name')
        search_fields = ['^name']
        
class ParafiscalQuerySerializer(GenericQuerySerializer):

    class Meta:
        model = Parafiscal
        fields = ('id', 'name',)

class ParafiscalSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        request = self.context['request']
        instance, instance_created = Parafiscal.objects.get_or_create(**validated_data)
        instance_data = ParafiscalSerializer(instance)
        AuditorParameter.registerEvent(request, 'entities', 'Parafiscal', instance_data.data, instance.id, AuditorParameter.CREATE, action='CREATE_PARAFISCAL')
        return instance

    def update(self, instance, validated_data):
        request = self.context['request']
        old_serializer_data = ParafiscalSerializer(instance).data
        serializer = super(ParafiscalSerializer, self).update(instance, validated_data)
        instance_data = ParafiscalSerializer(instance)
        AuditorParameter.registerEvent(request, 'entities', 'Parafiscal', instance_data.data, instance.id,
                                       AuditorParameter.UPDATE, before_register_object=old_serializer_data,
                                       action='UPDATE_PARAFISCAL')
        return serializer

    class Meta:
        model = Parafiscal
        fields = ('id', 'nit', 'name')

class TaxesTableSerializer(serializers.ModelSerializer):

    class Meta:
        model = Taxes
        fields = ('id', 'nit', 'name')
        search_fields = ['^name']
        
class TaxesQuerySerializer(GenericQuerySerializer):

    class Meta:
        model = Taxes
        fields = ('id', 'name',)

class TaxesSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        request = self.context['request']
        instance, instance_created = Taxes.objects.get_or_create(**validated_data)
        instance_data = TaxesSerializer(instance)
        AuditorParameter.registerEvent(request, 'entities', 'Taxes', instance_data.data, instance.id, AuditorParameter.CREATE, action='CREATE_TAXES')
        return instance

    def update(self, instance, validated_data):
        request = self.context['request']
        old_serializer_data = TaxesSerializer(instance).data
        serializer = super(TaxesSerializer, self).update(instance, validated_data)
        instance_data = TaxesSerializer(instance)
        AuditorParameter.registerEvent(request, 'entities', 'Taxes', instance_data.data, instance.id,
                                       AuditorParameter.UPDATE, before_register_object=old_serializer_data,
                                       action='UPDATE_TAXES')
        return serializer

    class Meta:
        model = Taxes
        fields = ('id', 'nit', 'name')

class ArlTableSerializer(serializers.ModelSerializer):

    class Meta:
        model = Arl
        fields = ('id', 'nit', 'name')
        search_fields = ['^name']
        
class ArlQuerySerializer(GenericQuerySerializer):

    class Meta:
        model = Arl
        fields = ('id', 'name',)

class ArlSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        request = self.context['request']
        instance, instance_created = Arl.objects.get_or_create(**validated_data)
        instance_data = ArlSerializer(instance)
        AuditorParameter.registerEvent(request, 'entities', 'Arl', instance_data.data, instance.id, AuditorParameter.CREATE, action='CREATE_ARL')
        return instance

    def update(self, instance, validated_data):
        request = self.context['request']
        old_serializer_data = ArlSerializer(instance).data
        serializer = super(ArlSerializer, self).update(instance, validated_data)
        instance_data = ArlSerializer(instance)
        AuditorParameter.registerEvent(request, 'entities', 'Arl', instance_data.data, instance.id,
                                       AuditorParameter.UPDATE, before_register_object=old_serializer_data,
                                       action='UPDATE_ARL')
        return serializer

    class Meta:
        model = Arl
        fields = ('id', 'nit', 'name')

class PensionFundTableSerializer(serializers.ModelSerializer):

    class Meta:
        model = PensionFund
        fields = ('id', 'nit', 'name')
        search_fields = ['^name']
        
class PensionFundQuerySerializer(GenericQuerySerializer):

    class Meta:
        model = PensionFund
        fields = ('id', 'name',)

class PensionFundSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        request = self.context['request']
        instance, instance_created = PensionFund.objects.get_or_create(**validated_data)
        instance_data = PensionFundSerializer(instance)
        AuditorParameter.registerEvent(request, 'entities', 'PensionFund', instance_data.data, instance.id, AuditorParameter.CREATE, action='CREATE_PENSIONFUND')
        return instance

    def update(self, instance, validated_data):
        request = self.context['request']
        old_serializer_data = PensionFundSerializer(instance).data
        serializer = super(PensionFundSerializer, self).update(instance, validated_data)
        instance_data = PensionFundSerializer(instance)
        AuditorParameter.registerEvent(request, 'entities', 'PensionFund', instance_data.data, instance.id,
                                       AuditorParameter.UPDATE, before_register_object=old_serializer_data,
                                       action='UPDATE_PENSIONFUND')
        return serializer

    class Meta:
        model = PensionFund
        fields = ('id', 'nit', 'name')
       
class BankTableSerializer(serializers.ModelSerializer):

    class Meta:
        model = Bank
        fields = ('id', 'nit', 'name')
        search_fields = ['^name']
        
class BankQuerySerializer(GenericQuerySerializer):

    class Meta:
        model = Bank
        fields = ('id', 'name',)

class BankSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        request = self.context['request']
        instance, instance_created = Bank.objects.get_or_create(**validated_data)
        instance_data = BankSerializer(instance)
        AuditorParameter.registerEvent(request, 'entities', 'Bank', instance_data.data, instance.id, AuditorParameter.CREATE, action='CREATE_BANK')
        return instance

    def update(self, instance, validated_data):
        request = self.context['request']
        old_serializer_data = BankSerializer(instance).data
        serializer = super(BankSerializer, self).update(instance, validated_data)
        instance_data = BankSerializer(instance)
        AuditorParameter.registerEvent(request, 'entities', 'Bank', instance_data.data, instance.id,
                                       AuditorParameter.UPDATE, before_register_object=old_serializer_data,
                                       action='UPDATE_BANK')
        return serializer

    class Meta:
        model = Bank
        fields = ('id', 'nit', 'name')
 
        