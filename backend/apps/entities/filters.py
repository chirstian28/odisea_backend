import django_filters

from .models import Entity, CompesationEntity, Eps

class EntityTableFilter(django_filters.rest_framework.FilterSet):

    class Meta:
        model = Entity
        fields = ['entity_type','name']

class CompesationEntityTableFilter(django_filters.rest_framework.FilterSet):

    class Meta:
        model = CompesationEntity
        fields = ['state','name', "short_name"]

class EpsTableFilter(django_filters.rest_framework.FilterSet):

    class Meta:
        model = Eps
        fields = ["entity_class", "regime", 'name', "short_name"]
        
        