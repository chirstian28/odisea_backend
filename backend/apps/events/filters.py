import django_filters

from .models import Event

class EventTableFilter(django_filters.rest_framework.FilterSet):

    class Meta:
        model = Event
        fields = ["event_type", "employee", "register_date" ]
