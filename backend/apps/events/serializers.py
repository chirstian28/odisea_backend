from rest_framework import serializers
from .models import Event, RequestStatus
import datetime
from backend.library.datemanager.models import Holiday
from backend.components.auditor.models import AuditorParameter
from backend.core.generic_serializer import GenericQuerySerializer
from backend.core.models import ResourceHost
from backend.auth.accounts.models import Account
from rest_framework.fields import SerializerMethodField, ValidationError

class EventTableSerializer(serializers.ModelSerializer):
    event_type = serializers.CharField(label='Tipo de novedad')
    employee = serializers.CharField(label='Empleado')
    status = serializers.CharField(label='Estado de solicitud')

    class Meta:
        model = Event
        fields = ('id', 'employee', 'event_type', "register_date", "status")
        filter_fields = ("event_type", "employee", "register_date", )
        fields_actions = {
                            'employee': { 'items': { 'url': ResourceHost.getURL('BASE_URL') + '/employees/employeequery/', 
                                          'always': True,
                                    },
                                },                            
                    }
        
class EventQuerySerializer(GenericQuerySerializer):

    class Meta:
        model = Event
        fields = ('id', 'name',)
        filter_fields = ("event_type", "employee",)


class EventSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        request = self.context['request']
        actual_account = Account.getAccount(request.user)

        if actual_account is not None:
            start = validated_data['start_date']
            end = validated_data['end_date']
            count_days = 0
            count_bussiness_days = 0

            if start is not None and end is not None:
                date_list = [start + datetime.timedelta(days=d) for d in range((end - start).days + 1)] 
                holiday = Holiday()
                count_days = len(date_list)
                count_bussiness_days = 0
                for date in date_list:
                    is_business_day = holiday.is_business_day(date, True, False)
                    if is_business_day:
                        count_bussiness_days += 1

            instance, instance_created = Event.objects.get_or_create(licenser=actual_account.licenser, days=count_days, business_days=count_bussiness_days, **validated_data)
            instance_data = EventSerializer(instance)
            AuditorParameter.registerEvent(request, 'events', 'Event', instance_data.data, instance.id, AuditorParameter.CREATE, action='CREATE_EVENT')
        else:
            raise ValidationError("No se encuentra un inicio de sesión")
        return instance

    class Meta:
        model = Event
        fields = ('id', 'employee', "event_type", "register_date", 
                  "start_date", "end_date", "value", "permanent")
        fields_actions = {
                            'employee': { 'items': { 'url': ResourceHost.getURL('BASE_URL') + '/employees/employeequery/', 
                                          'always': True,
                                    },
                                },                            
                    }


class EventEmployeeSerializer(serializers.ModelSerializer):
    status = serializers.PrimaryKeyRelatedField(queryset=RequestStatus.objects.filter(id='05'))

    def update(self, instance, validated_data):
        request = self.context['request']
        old_serializer_data = EventSerializer(instance).data
        serializer = super(EventSerializer, self).update(instance, validated_data)
        instance_data = EventSerializer(instance)
        AuditorParameter.registerEvent(request, 'events', 'Event', instance_data.data, instance.id,
                                       AuditorParameter.UPDATE, before_register_object=old_serializer_data,
                                       action='UPDATE_EVENT')
        return serializer

    class Meta:
        model = Event
        fields = ('id', 'employee', "event_type", "register_date", 
                  "start_date", "end_date", "value", "permanent", "status")
        fields_actions = {
                            'employee': { 'items': { 'url': ResourceHost.getURL('BASE_URL') + '/employees/employeequery/', 
                                          'always': True,
                                    },
                                },                            
                    }

class EventAdminSerializer(serializers.ModelSerializer):
    status = serializers.PrimaryKeyRelatedField(queryset=RequestStatus.objects.filter(id__in=['01', '03']))

    def update(self, instance, validated_data):
        request = self.context['request']
        old_serializer_data = EventSerializer(instance).data
        serializer = super(EventSerializer, self).update(instance, validated_data)
        instance_data = EventSerializer(instance)
        AuditorParameter.registerEvent(request, 'events', 'Event', instance_data.data, instance.id,
                                       AuditorParameter.UPDATE, before_register_object=old_serializer_data,
                                       action='UPDATE_EVENT')
        return serializer

    class Meta:
        model = Event
        fields = ('id', 'employee', "event_type", "register_date", 
                  "start_date", "end_date", "value", "permanent", "status")
        fields_actions = {
                            'employee': { 'items': { 'url': ResourceHost.getURL('BASE_URL') + '/employees/employeequery/', 
                                          'always': True,
                                    },
                                },                            
                    }