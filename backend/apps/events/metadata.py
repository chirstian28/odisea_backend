# -*- coding: utf-8 -*-
from backend.core.metadata import GenericTableMetadata

class TestMetadata(GenericTableMetadata):

    def determine_metadata(self, request, view):
        meta_data_dict = GenericTableMetadata.determine_metadata(self, request, view)
        url_fix = str(meta_data_dict['actions']['BASIC']['UPDATE']['url'])[:-6]
        url_fix += 'eventadmin/'
        if str(request.user) == 'ertech':
            meta_data_dict['actions']['BASIC']['UPDATE']['url'] = url_fix

        # method_list = ['UPDATE', 'DELETE', 'QUERY']

        # for method in method_list:
        #     url_fix = str(meta_data_dict['actions']['BASIC'][method]['url'])[:-6]

        #     if str(request.user) == 'ertech':
        #         url_fix += 'eventadmin/'
        #     else:
        #         url_fix += 'eventemployee/'
        #     meta_data_dict['actions']['BASIC'][method]['url'] = url_fix

        return meta_data_dict
