from .models import Event
from django.db.models import Q
from backend.apps.employees.models import Employee
from backend.auth.accounts.models import Account
from .serializers import EventTableSerializer, EventQuerySerializer, \
    EventSerializer, EventEmployeeSerializer, EventAdminSerializer
from .filters import EventTableFilter
from .metadata import TestMetadata
from .permissions import IsOwnerOrReadOnly, CustomPermission
from backend.core.permissions import IsAdminOrReadOnly
from rest_framework.permissions import IsAuthenticated
from backend.core.views import GenericQueryViewSet, GenericModelViewSet, \
    GenericTableViewSet

class EventTableViewSet(GenericTableViewSet):
    app_name = 'events'
    model_name = 'Event'
    queryset = Event.objects.all()
    serializer_class = EventTableSerializer
    filter_class = EventTableFilter
    metadata_class = TestMetadata
    permission_classes = (IsAdminOrReadOnly,)

    def get_queryset(self):
        queryset = GenericTableViewSet.get_queryset(self)
        context = self.get_serializer_context()
        request = context['request']
        actual_account = Account.getAccount(request.user)

        if actual_account.type.id == 'E':
            queryset = queryset.filter(employee__account=actual_account)
        elif actual_account.type.id == 'J':
            queryset = queryset.filter(Q(employee__account=actual_account)|Q(employee__immediate_boss__account=actual_account))
        
        return queryset
    
class EventQueryViewSet(GenericQueryViewSet):
    app_name = 'events'
    model_name = 'Event'
    queryset = Event.objects.all()
    serializer_class = EventQuerySerializer

class EventViewSet(GenericModelViewSet):
    app_name = 'events'
    model_name = 'Event'
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    permission_classes = (IsAuthenticated, CustomPermission)

class EventEmployeeViewSet(GenericModelViewSet):
    app_name = 'events'
    model_name = 'Event'
    queryset = Event.objects.all()
    serializer_class = EventEmployeeSerializer
    permission_classes = (IsAdminOrReadOnly, IsOwnerOrReadOnly)

class EventAdminViewSet(GenericModelViewSet):
    app_name = 'events'
    model_name = 'Event'
    queryset = Event.objects.all()
    serializer_class = EventAdminSerializer
    permission_classes = (IsAdminOrReadOnly, IsOwnerOrReadOnly)