from backend.apps.events import views
from rest_framework import routers
from django.urls import path

router = routers.DefaultRouter()
router.register(r'eventtable', views.EventTableViewSet, basename='eventtable')
router.register(r'eventquery', views.EventQueryViewSet, basename='eventquery')
router.register(r'event', views.EventViewSet, basename='event')
router.register(r'eventemployee', views.EventEmployeeViewSet, basename='eventemployee')
router.register(r'eventadmin', views.EventAdminViewSet, basename='eventadmin')

urlpatterns = [
    #path('detail/<int:pk>/', views.ItemEventDetailView.as_view()),
]

urlpatterns += router.urls
