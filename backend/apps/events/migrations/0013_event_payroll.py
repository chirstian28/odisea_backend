# Generated by Django 2.2.17 on 2020-11-26 09:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('payroll', '0033_auto_20201126_0910'),
        ('events', '0012_remove_eventtype_item'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='payroll',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='payroll.Payroll', verbose_name='Planilla'),
        ),
    ]
