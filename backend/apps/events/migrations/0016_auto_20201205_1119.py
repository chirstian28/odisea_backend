# Generated by Django 2.2.17 on 2020-12-05 11:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0015_remove_event_paid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eventtype',
            name='id',
            field=models.CharField(max_length=5, primary_key=True, serialize=False, verbose_name='Código Novedad'),
        ),
        migrations.AlterUniqueTogether(
            name='eventtype',
            unique_together=set(),
        ),
        migrations.RemoveField(
            model_name='eventtype',
            name='code',
        ),
        migrations.RemoveField(
            model_name='eventtype',
            name='item',
        ),
        migrations.RemoveField(
            model_name='eventtype',
            name='licenser',
        ),
    ]
