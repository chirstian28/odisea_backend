from django.db import models
import datetime

class RequestStatus(models.Model):
    id = models.CharField(max_length=2, primary_key=True, verbose_name='Estado')
    name = models.CharField(max_length=40, verbose_name='Estado')

    def __str__(self):
        return f"{self.name}"

    class Meta:
        ordering = ['name']
        app_label = 'events'
        verbose_name = 'Estado de Solicitud'
        verbose_name_plural = 'Estado de Solicitudes'

class EventType(models.Model):
    id = models.CharField(max_length=5, verbose_name='Código Novedad', primary_key=True)
    name = models.CharField(max_length=40, verbose_name='Nombre')
    paid = models.BooleanField(verbose_name='Remunerada?', default=False)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        ordering = ['name']
        app_label = 'events'
        verbose_name = 'Event Type'
        verbose_name_plural = 'Events Types'

class Event(models.Model):
    licenser = models.ForeignKey('license.Licenser', on_delete=models.PROTECT)
    employee = models.ForeignKey('employees.Employee', on_delete=models.PROTECT, verbose_name='Empleado')
    event_type = models.ForeignKey('events.EventType', on_delete=models.PROTECT, verbose_name='Tipo de novedad')
    register_date = models.DateTimeField(verbose_name="Fecha de Registro")
    start_date = models.DateTimeField(null=True, verbose_name="Fecha de inicio")
    end_date = models.DateTimeField(auto_now=False, null=True, auto_now_add=False, verbose_name="Fecha de finalizacion")
    status = models.ForeignKey('events.RequestStatus', default='1', on_delete=models.PROTECT, verbose_name='Estado de solicitud')
    value = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True, verbose_name="Valor")
    days = models.IntegerField(verbose_name='Días', null=True, blank=True)
    business_days = models.IntegerField(verbose_name='Días Hábiles', null=True, blank=True)
    permanent = models.BooleanField(default=False, verbose_name='Es permamente?')
    payroll = models.ForeignKey('payroll.Payroll', on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Planilla')

    @staticmethod
    def evaluate_event(employee):
        response_dict = {'days': 0, 'deductions':0, 'accurred':0, 'event_ids':[]}
        today = datetime.date.today()
        try:
            events = Event.objects.filter(employee=employee, register_date__month=today.month, status='01')
            for event in events:
                response_dict['event_ids'].append(event.id)
                if event.value is not None or event.permanent:
                    if event.event_type.id == '00006':
                        response_dict['accurred'] += event.value
                    if event.event_type.id in ('00009', '00010'):
                        response_dict['deductions'] += event.value
                    else:
                        response_dict['accurred'] += event.value
                else:
                    if not event.event_type.paid:
                        response_dict['days'] += event.business_days
            return response_dict
        except:
            return response_dict

    @staticmethod
    def assign_payroll(list_event_ids, payroll):
        for event_id in list_event_ids:
            try:
                obj_event = Event.objects.get(id=event_id)
                obj_event.payroll = payroll
                obj_event.save()
            except:
                pass

    def __str__(self):
        return "%s" % (self.event_type.name)

    class Meta:
        ordering = ['employee', '-register_date']
        app_label = 'events'
        verbose_name = 'Novedad'
        verbose_name_plural = 'Novedades'