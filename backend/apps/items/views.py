from .models import Provision, Item
from .serializers import ProvisionTableSerializer, ProvisionQuerySerializer, \
    ProvisionSerializer, ItemTableSerializer, ItemQuerySerializer, \
    ItemSerializer
from .filters import ItemTableFilter
from backend.core.views import GenericQueryViewSet, GenericModelViewSet, \
    GenericTableViewSet
from backend.core.permissions import IsAdminOrReadOnly

class ProvisionTableViewSet(GenericTableViewSet):
    app_name = 'items'
    model_name = 'Provision'
    queryset = Provision.objects.all()
    serializer_class = ProvisionTableSerializer
    
class ProvisionQueryViewSet(GenericQueryViewSet):
    app_name = 'items'
    model_name = 'Provision'
    queryset = Provision.objects.all()
    serializer_class = ProvisionQuerySerializer

class ProvisionViewSet(GenericModelViewSet):
    app_name = 'items'
    model_name = 'Provision'
    queryset = Provision.objects.all()
    serializer_class = ProvisionSerializer
    permission_classes = (IsAdminOrReadOnly, )
        
class ItemTableViewSet(GenericTableViewSet):
    app_name = 'items'
    model_name = 'Item'
    queryset = Item.objects.all()
    serializer_class = ItemTableSerializer
    filter_class = ItemTableFilter
    
class ItemQueryViewSet(GenericQueryViewSet):
    app_name = 'items'
    model_name = 'Item'
    queryset = Item.objects.all()
    serializer_class = ItemQuerySerializer

class ItemViewSet(GenericModelViewSet):
    app_name = 'items'
    model_name = 'Item'
    queryset = Item.objects.all()
    serializer_class = ItemSerializer
    permission_classes = (IsAdminOrReadOnly, )
