from django.urls import path, include
from backend.apps.items import views
from rest_framework import routers

router = routers.DefaultRouter()

router.register(r'provisiontable', views.ProvisionTableViewSet, basename='provisiontable')
router.register(r'provisionquery', views.ProvisionQueryViewSet, basename='provisionquery')
router.register(r'provision', views.ProvisionViewSet, basename='provision')
router.register(r'itemtable', views.ItemTableViewSet, basename='itemtable')
router.register(r'itemquery', views.ItemQueryViewSet, basename='itemquery')
router.register(r'item', views.ItemViewSet, basename='item')

urlpatterns = [

]

urlpatterns += router.urls

