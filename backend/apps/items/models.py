from django.db import models
import datetime
from backend.apps.general.models import Parameter
import decimal

class Concept(models.Model):
    id = models.CharField(max_length=5, verbose_name='ID', primary_key=True)
    name = models.CharField(max_length=40, verbose_name='Nombre')

    def __str__(self):
        return f'{self.name}'
    
    class Meta:
        ordering = ['name']
        app_label = 'items'
        verbose_name = 'Concepto'
        verbose_name_plural = 'Conceptos'

class ItemType(models.Model):
    id = models.CharField(max_length=5, verbose_name='ID', primary_key=True)
    name = models.CharField(max_length=40, verbose_name='Nombre')

    def __str__(self):
        return f'{self.name}'

    class Meta:
        ordering = ['name']
        app_label = 'items'
        verbose_name = 'Tipo de Item'
        verbose_name_plural = 'Tipos de Items'

class Provision(models.Model):
    licenser = models.ForeignKey('license.Licenser', default=1, on_delete=models.PROTECT)
    code = models.CharField(max_length=5, verbose_name='Código')
    name = models.CharField(max_length=40, verbose_name='Nombre')

    def __str__(self):
        return f'{self.name}'

    class Meta:
        unique_together = [["licenser", "code",]]
        ordering = ['name']
        app_label = 'items'
        verbose_name = 'Provisión'
        verbose_name_plural = 'Provisiones'

class Item(models.Model):
    licenser = models.ForeignKey('license.Licenser', default=1, on_delete=models.PROTECT)
    name = models.CharField(max_length=30, verbose_name="Nombre")
    concept = models.ForeignKey('Concept', on_delete=models.PROTECT)
    item_type = models.ForeignKey('ItemType', on_delete=models.PROTECT)
    value = models.DecimalField(max_digits=14, decimal_places=2, null=True, blank=True, verbose_name="Valor")
    calcule_base = models.TextField(verbose_name='Fórmula Base Cálculo', null=True, blank=True)
    calcule_min = models.CharField(max_length=50, verbose_name="Mínimo", null=True, blank=True)
    calcule_max = models.CharField(max_length=50, verbose_name="Máximo", null=True, blank=True)
    source = models.ForeignKey('Provision', blank=True, null=True, on_delete=models.PROTECT, verbose_name="Fuente")
    destine = models.ForeignKey('Provision', blank=True, null=True, on_delete=models.PROTECT, verbose_name="Destino", related_name="destine_provision")
    months_payment = models.ManyToManyField('datemanager.MonthName', verbose_name="Ingrese meses de pago", blank=True)
    entity_type = models.ForeignKey('entities.EntityType', blank=True, null=True, on_delete=models.PROTECT, verbose_name="Tipo Entidad")
    event_type = models.OneToOneField('events.EventType', blank=True, null=True, on_delete=models.PROTECT, verbose_name='Tipo de Novedad')
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha y hora")

    def __str__(self):
        return "%s" % (self.name)

    def calculate(self, formula, payroll, percentage_value=None):

        if formula.find('BASICO') != -1:
            formula = formula.replace('BASICO', str(payroll.basic))
        if formula.find('SALARIO') != -1:
            formula = formula.replace('SALARIO', str(payroll.employee.salary))

        formula_replace = formula.replace(" ", "")

        str_to_list = ''

        for index, letter in enumerate(formula_replace):
            if letter == '(' or letter == ')' or letter == '+' or \
                    letter == '*' or letter == '-' or letter == '/':
                str_to_list += ','
            else:
                str_to_list += letter

        values = str_to_list.split(',')
        for value in values:
            if value != '':
                try:
                    item = Item.objects.get(name=value)
                    value_to_replace = item.calculate(item.calcule_base, payroll)
                except:
                    try:
                        obj_parameter = Parameter.objects.get(name=value)
                        value_to_replace = obj_parameter.value
                    except:
                        try:
                            isinstance(float(value), float)
                            value_to_replace = float(value)
                        except:
                            value_to_replace = 0

                formula_replace = formula_replace.replace(value, str(value_to_replace))
        try:
            result = eval(formula_replace)
            if percentage_value is not None:
                return (percentage_value / 100) * decimal.Decimal(result)
            elif self.item_type.id == 'POR' and (
                    (self.calcule_min == '') or (self.calcule_max == '')):
                return (self.value / 100) * decimal.Decimal(result)
            else:
                return result
        except:
            return 0

    class Meta:
        ordering = ['name']
        app_label = 'items'
        verbose_name = 'Item'
        verbose_name_plural = 'Items'
