INSERT INTO items_concept (id, name) VALUES ( 'DEV', 'DEVENGO');
INSERT INTO items_concept (id, name) VALUES ( 'DED', 'DEDUCCIÓN');
INSERT INTO items_concept (id, name) VALUES ( 'PRE', 'PRESTACION');
INSERT INTO items_concept (id, name) VALUES ( 'SEG', 'SEGURIDAD_SOCIAL');
INSERT INTO items_concept (id, name) VALUES ( 'PAR', 'PARAFISCAL');
INSERT INTO items_concept (id, name) VALUES ( 'CON', 'CONSIGNACION');

INSERT INTO items_itemtype (id, name) VALUES ( 'VFI', 'VALOR_FIJO');
INSERT INTO items_itemtype (id, name) VALUES ( 'POR', 'PORCENTAJE');
INSERT INTO items_itemtype (id, name) VALUES ( 'PRO', 'PROVISION');
INSERT INTO items_itemtype (id, name) VALUES ( 'VAC', 'VACACIONES');
INSERT INTO items_itemtype (id, name) VALUES ( 'ARP', 'TABLA_ARP');
