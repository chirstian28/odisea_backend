import django_filters

from .models import Item

class ItemTableFilter(django_filters.rest_framework.FilterSet):

    class Meta:
        model = Item
        fields = ('concept', "item_type","event_type",)

        