from rest_framework import serializers
from backend.core.models import ResourceHost
from backend.core.generic_serializer import GenericQuerySerializer
from backend.auth.accounts.models import Account
from rest_framework.fields import ValidationError
from .models import Provision, Item
from backend.components.auditor.models import AuditorParameter

class ProvisionTableSerializer(serializers.ModelSerializer):

    class Meta:
        model = Provision
        fields = ('id', 'code', 'name',)
        search_fields = ['^name', "$code"]
        
class ProvisionQuerySerializer(GenericQuerySerializer):

    class Meta:
        model = Provision
        fields = ('id', 'name',)

class ProvisionSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        request = self.context['request']
        actual_account = Account.getAccount(request.user)
        if actual_account is not None:
            instance, instance_created = Provision.objects.get_or_create(licenser=actual_account.licenser, **validated_data)
            instance_data = ProvisionSerializer(instance)
            AuditorParameter.registerEvent(request, 'items', 'Provision', instance_data.data, instance.id, AuditorParameter.CREATE, action='CREATE_PROVISION')
        else:
            raise ValidationError("No se encuentra un inicio de sesión")
        return instance

    def update(self, instance, validated_data):
        request = self.context['request']
        old_serializer_data = ProvisionSerializer(instance).data
        serializer = super(ProvisionSerializer, self).update(instance, validated_data)
        instance_data = ProvisionSerializer(instance)
        AuditorParameter.registerProvision(request, 'items', 'Provision', instance_data.data, instance.id,
                                       AuditorParameter.UPDATE, before_register_object=old_serializer_data,
                                       action='UPDATE_PROVISION')
        return serializer

    class Meta:
        model = Provision
        fields = ('id', 'code', "name", )

class ItemTableSerializer(serializers.ModelSerializer):

    class Meta:
        model = Item
        fields = ('id', 'name', 'concept', "item_type",)
        search_fields = ['^name', ]
        filter_fields = ('concept', "item_type","event_type",)
        
        
class ItemQuerySerializer(GenericQuerySerializer):

    class Meta:
        model = Item
        fields = ('id', 'name',)

class ItemSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        request = self.context['request']
        actual_account = Account.getAccount(request.user)
        if actual_account is not None:
            month_list = validated_data['months_payment']
            del validated_data['months_payment']
            
            instance, instance_created = Item.objects.get_or_create(licenser=actual_account.licenser, **validated_data)
            instance_data = ItemSerializer(instance)
            instance.months_payment.set(month_list) 
            AuditorParameter.registerEvent(request, 'item', 'Item', instance_data.data, instance.id, AuditorParameter.CREATE, action='CREATE_ITEM')
        else:
            raise ValidationError("No se encuentra un inicio de sesión")
        return instance

    def update(self, instance, validated_data):
        request = self.context['request']
        old_serializer_data = ItemSerializer(instance).data

        month_list = validated_data['months_payment']
        del validated_data['months_payment']
        serializer = super(ItemSerializer, self).update(instance, validated_data)
        instance_data = ItemSerializer(instance)
        instance.months_payment.set(month_list) 
        AuditorParameter.registerEvent(request, 'item', 'Item', instance_data.data, instance.id,
                                       AuditorParameter.UPDATE, before_register_object=old_serializer_data,
                                       action='UPDATE_ITEM')
        return serializer

    class Meta:
        model = Item
        fields = ('id', 'name', "concept", "item_type", "value", "calcule_base", 
                  "calcule_min", "calcule_max", "source", "destine", 
                  "entity_type", "event_type", "months_payment", 
            )
        fields_actions = {
                            'source': { 'items': { 'url': ResourceHost.getURL('BASE_URL') + '/items/provisionquery/', 
                                          'always': True,
                                    },
                                },                            
                            'destine': { 'items': { 'url': ResourceHost.getURL('BASE_URL') + '/items/provisionquery/', 
                                          'always': True,
                                    },
                                },                            
                    }

