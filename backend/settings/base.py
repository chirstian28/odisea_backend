import os
import datetime

DEVELOPER_INFO = { 'company' : "ER Technology LTDA",
                   'url' : "http://www.er-technology.net",
                   'email' : "info@er-technology.net",
                   'copyright': "© Desarrollado por ER Technology Ltda. %s" % datetime.date.today().strftime('%Y') 
            }

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

from .project import *

APP_NAME = PRODUCTION_APP_NAME
SECRET_KEY = '5j^5t(91*=+0n0l&uxg=sdm=smj7gcqx1mrvi=s0fi7^6-%q7q'
DEBUG = False
USE_HTTPS = False

CURRENCY_FORMAT = '$ #.###,00'
DATETIME_FORMAT = 'Y-m-d H:i:sO'
DATE_FORMAT = 'Y-m-d'
DATE_INPUT_FORMATS = ('%Y-%m-%d',)
FRONTEND_DATE_FORMAT = 'yyyy-MM-dd'
FRONTEND_DATETIME_FORMAT = 'yyyy-MM-dd HH:mm'

PROJECT_DIR = BASE_DIR + '/' + APP_NAME
BASE_URL = PRODUCTION_BASE_URL

SESSION_EXPIRE_AT_BROWSER_CLOSE=True
USE_I18N = True

CONNECT_HTTPS = False #True

CORS_ORIGIN_ALLOWALL = True
CORS_ALLOW_CREDENTIALS = True

CORS_ALLOW_METHODS = (
    'DELETE',
    'GET',
    'OPTIONS',
    'PATCH',
    'POST',
    'PUT',
)

CORS_ALLOW_HEADERS = (
    'accept',
    'accept-encoding',
    'authorization',
    'content-type',
    'dnt',
    'origin',
    'user-agent',
    'x-csrftoken',
    'x-requested-with',
)


if 'RDS_DB_NAME' in os.environ:
    PLATFORM = 'AWS'
else:
    PLATFORM = "LOCAL"

if CONF_ENVIRONMENT == "DEVELOPMENT":
    DEBUG = True

ALLOWED_HOSTS = ['*']

TEMP_DIR = "/tmp/"

try:
    from debug import *
except:
    pass

DDT_DEBUG = DEBUG

AUTH_PROFILE_MODULE = 'backend.auth.accounts.Account'
ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_USERNAME_REQUIRED = False

# Application definition
INSTALLED_APPS = [
    'rest_framework',
    'django_filters',
    'rest_framework.authtoken',
    'dbbackup',
    'storages',
    'mathfilters',
    'easy_pdf',
#    'dynamic_rest',
    'corsheaders',
    'django_extensions',
    'django_db_views',
]

DJANGO_MODULES_INSTALL = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
]

HEALT_CHECK_APPS = [
    'health_check',
    'health_check.db',
    'health_check.cache',
    'health_check.storage',
]

# Application definition

INSTALLED_CORE = [
    'backend.core',
]

INSTALLED_LIBRARY = [
    'backend.library.localization.apps.LibraryConfig',
    'backend.library.datemanager.apps.LibraryConfig',
    'backend.library.charts.apps.LibraryConfig',
]

INSTALLED_AUTH = [
    'backend.auth.license.apps.AuthConfig',
    'backend.auth.accounts.apps.AuthConfig',
]

INSTALLED_COMPONENTS = [
    'backend.components.auditor.apps.ComponentsConfig',
    'backend.components.backup.apps.ComponentsConfig',
    'backend.components.menu.apps.ComponentsConfig',
    'backend.components.dashboard.apps.ComponentsConfig',
    'backend.components.pushmessages.apps.ComponentsConfig',
    'backend.components.errors.apps.ComponentsConfig',
    'backend.components.filemanager.apps.ComponentsConfig',
]

INSTALLED_MODULES = [
    'backend.apps.entities.apps.EntitiesConfig',
    'backend.apps.employees.apps.EmployeesConfig',
    'backend.apps.items.apps.ItemsConfig',
    'backend.apps.payroll.apps.PayrollConfig',
    'backend.apps.events.apps.EventsConfig',
    'backend.apps.general.apps.GeneralConfig',
#    'backend.apps.dataio.apps.AppsConfig',
#    'backend.apps.collect.apps.AppsConfig',
#    'backend.apps.operative.apps.AppsConfig',
#     'backend.apps.report.apps.AppsConfig',
]

INSTALLED_APPS += DJANGO_MODULES_INSTALL
INSTALLED_APPS += HEALT_CHECK_APPS

PROJECT_COMPONENTS = INSTALLED_CORE
PROJECT_COMPONENTS += INSTALLED_LIBRARY
PROJECT_COMPONENTS += INSTALLED_AUTH
PROJECT_COMPONENTS += INSTALLED_COMPONENTS
PROJECT_COMPONENTS += INSTALLED_MODULES

INSTALLED_APPS += PROJECT_COMPONENTS

MIDDLEWARE = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'corsheaders.middleware.CorsMiddleware',
]

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': ('rest_framework.permissions.AllowAny',),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 50,
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
    ),
    'EXCEPTION_HANDLER': 'rest_framework.views.exception_handler' ,
    'DEFAULT_METADATA_CLASS': 'backend.core.metadata.GenericTableMetadata',
    'DEFAULT_FILTER_BACKENDS': ('django_filters.rest_framework.DjangoFilterBackend','rest_framework.filters.SearchFilter','rest_framework.filters.OrderingFilter',),
    'TEST_REQUEST_DEFAULT_FORMAT': 'json',
}

if CONF_ENVIRONMENT == 'PRODUCTION':
    REST_FRAMEWORK['DEFAULT_RENDERER_CLASSES'] = (
        'rest_framework.renderers.JSONRenderer',
    )
else:
    REST_FRAMEWORK['TEST_REQUEST_RENDERER_CLASSES'] = (
        'rest_framework.renderers.MultiPartRenderer',
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.TemplateHTMLRenderer'
    )

ROOT_URLCONF = 'backend.core.urls'

AUTHENTICATION_BACKENDS = (
   'django.contrib.auth.backends.ModelBackend',
)

JWT_AUTH = {
    'JWT_REFRESH_EXPIRATION_DELTA': datetime.timedelta(days=7),
    'JWT_ALLOW_REFRESH': True,
    'JWT_EXPIRATION_DELTA': datetime.timedelta(days=365),
}

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'core/templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                APP_NAME + '.core.template_processor.generic_variables',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# FILE_UPLOAD_HANDLERS = ("django_excel.ExcelMemoryFileUploadHandler",
#                         "django_excel.TemporaryExcelFileUploadHandler")

WSGI_APPLICATION = 'backend.core.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

if PLATFORM == "AWS":
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': os.environ['RDS_DB_NAME'],
            'USER': os.environ['RDS_USERNAME'],
            'PASSWORD': os.environ['RDS_PASSWORD'],
            'HOST': os.environ['RDS_HOSTNAME'],
            'PORT': os.environ['RDS_PORT'],
        }
    }
else:
    if CONF_ENVIRONMENT == "PRODUCTION":
        #DATABASES = {
        #    'default': {
        #        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        #        'NAME': 'ebdb',
        #        'USER': 'ertech',
        #        'PASSWORD': 'am3ricas',
        #        'HOST': 'aa9eq9fmd4478n.c6cn7v9bb96a.us-west-2.rds.amazonaws.com',
        #        'PORT': '5432',
        #    }
        #}
        DATABASES = {
            'default': {
                'ENGINE': 'django.db.backends.postgresql_psycopg2',
                'NAME': 'data_base_odisea',
                'USER': 'ertech',
                'PASSWORD': 'cristian123',
                'HOST': 'localhost',
                'PORT': '5432',
            }
        }
    elif CONF_ENVIRONMENT == "TESTING":
        DATABASES = {
            'default': {
                'ENGINE': 'django.db.backends.postgresql_psycopg2',
                'NAME': 'data_base_odisea',
                'USER': 'ertech',
                'PASSWORD': 'cristian123',
                'HOST': 'localhost',
                'PORT': '5432',
            }
        }
    else:
        DATABASES = {
            'default': {
                'ENGINE': 'django.db.backends.postgresql_psycopg2',
                'NAME': 'data_base_odisea',
                'USER': 'ertech',
                'PASSWORD': 'ERTECH',
                'HOST' : 'localhost',
                'PORT' : '5432',
            }
         }


# Internationalization

TIME_ZONE = 'America/Bogota'

LANGUAGE_CODE = 'es-CO'

USE_I18N = True

USE_L10N = True

USE_TZ = False

# email
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'christian.gonzalez@er-technology.net' #'soporte@er-technology.net'
EMAIL_HOST_PASSWORD = 'cristian281115' #'am3ricas'
DEFAULT_FROM_EMAIL = 'odisea@er-technology.net'

ADMINS = (
    ('ER Technology', 'soporte@er-technology.net'),
)

MANAGERS = ADMINS

APP_URL = ''

STATIC_ROOT = os.path.join(BASE_DIR, "..", "www", "static")
STATIC_URL = '/static/'
STATICFILES_DIRS = (os.path.join(BASE_DIR, 'static'),)
STATICFILES_FINDERS = [
                        'django.contrib.staticfiles.finders.FileSystemFinder',
                        'django.contrib.staticfiles.finders.AppDirectoriesFinder',
                      ]

#MEDIA FILE (user uploaded files)
MEDIA_ROOT = os.path.join(BASE_DIR, "..", "www", "media")
MEDIA_URL = '/media/'

#####################################################333
 
if CONNECT_HTTPS:
    SESSION_COOKIE_SECURE = True
    CSRF_COOKIE_SECURE = True
    SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/


LOGIN_URL = APP_URL + '/'
BASE_LOGIN_URL = BASE_URL + LOGIN_URL

WORK_FOLDER = 'work'
WORK_DIR = os.path.join(os.path.dirname(BASE_DIR),  APP_NAME + '/work/')
WORK_URL = MEDIA_URL + 'work/'
WORK_ROOT = MEDIA_ROOT + '/work/'
LOG_DIR = WORK_FOLDER + '/logs/'
LOG_ROOT = WORK_ROOT + 'logs/'
LOG_URL = WORK_URL + 'logs/'
GENERAL_LOG_FILE = LOG_ROOT + APP_NAME + '.log'
UPLOAD_ROOT= WORK_ROOT + 'upload/'
UPLOAD_URL = WORK_URL + 'upload/'
UPLOAD_DIR = WORK_FOLDER + '/upload/'

MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'

## TODO: instalacion del S3 para los archivos estaticos

WORK_URL = STATIC_URL + WORK_FOLDER + '/'
LOG_URL = WORK_URL + 'logs/'

DATE_INPUT_FORMATS = (
    '%Y-%m-%d', '%Y%m%d', '%Y/%m/%d',
)

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

if PLATFORM == "AWS":
    AWS_HEADERS = {  # see http://developer.yahoo.com/performance/rules.html#expires
            'Expires': 'Thu, 31 Dec 2099 20:00:00 GMT',
            'Cache-Control': 'max-age=94608000',
        }     

    if CONF_ENVIRONMENT == "TESTING":
        AWS_STORAGE_BUCKET_NAME = 'backend-bucket'
    else:
        AWS_STORAGE_BUCKET_NAME = 'odiseaback-bucket'
    
    AWS_ACCESS_KEY_ID = 'AKIAJSGPVYEVQ6WQMPVA'
    AWS_SECRET_ACCESS_KEY = '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'
    
    AWS_S3_UBICATION = 'us-west-2'
    AWS_S3_CUSTOM_DOMAIN = '%s.s3.%s.amazonaws.com' % (AWS_STORAGE_BUCKET_NAME, AWS_S3_UBICATION)
    STATIC_URL = "https://%s/" % AWS_S3_CUSTOM_DOMAIN
    STATICFILES_STORAGE = 'storages.backends.s3boto.S3Boto3Storage'
     
    STATICFILES_LOCATION = 'static'
    STATICFILES_STORAGE = 'backend.core.custom_storages.StaticStorage'
    STATIC_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, STATICFILES_LOCATION)
     
    MEDIAFILES_LOCATION = 'media'
    MEDIA_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, MEDIAFILES_LOCATION)
    DEFAULT_FILE_STORAGE = 'backend.core.custom_storages.PublicMediaStorage'

DBBACKUP_BACKUP_DIRECTORY = WORK_DIR + 'backup/'
DBBACKUP_DATE_FORMAT = '%Y%m%d%H%M'
DBBACKUP_POSTGRESQL_EXTENSION = 'psql'
DBBACKUP_SERVER_NAME = APP_NAME
DBBACKUP_STORAGE = 'django.core.files.storage.FileSystemStorage'

FCM_SERVER_KEY =  'AAAAzJiBwKc:APA91bF27qsRTqJ0Ge2d6I8UE03XiWo-cqwI6TpsfC_OxpyzAistbhlYByd_kxl9L1NasnV5JerDKv6A-dCgNQ_YJoQp7RBMUfvJobqRzfOVHyvqBonlHRnZe5hZ3gLLQ_jCpS_IscfQ'
FCM_LEGACY_SERVER_KEY =  'AIzaSyAi0VJcbPfUuyUEnDDQcKCMnbti5R9agkM'
FCM_API_KEY = 'AIzaSyDqqBseMmS4g2nFenbr7unP-MmsSl52ka0'
FCM_URL = 'https://fcm.googleapis.com/fcm/send'
FCM_SENDER_ID = '878731968679'

FRONTEND_APP_API_KEY = ''

# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

TMP_FOLDER = WORK_DIR + 'tmp/'

FAVICON_ROOT = MEDIA_ROOT + '/favicon.ico'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s',
        },
        'simple': {
            'format': '%(levelname)s %(message)s',
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'django.request': {
            # 'handlers': ['mail_admins'],
            'handlers': ['console'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}


MESSAGE_INFO = 'info'
MESSAGE_ERROR = 'error'
MESSAGE_WARNING = 'warning'
MESSAGE_SEVERE_WARNING = 'severeWarning'
MESSAGE_SUCCESS = 'success'

CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_CREDENTIALS = True

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

FILTER_LICENSER = True
