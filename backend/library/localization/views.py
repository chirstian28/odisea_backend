from rest_framework import viewsets
from backend.library.localization.models import Country, Region, State, City
from backend.library.localization.serializers import CountrySerializer, \
                                                     CountryTableSerializer, \
                                                     CountryQuerySerializer, \
                                                     RegionSerializer, \
                                                     RegionQuerySerializer, \
                                                     RegionTableSerializer, \
                                                     StateSerializer, \
                                                     StateTableSerializer, \
                                                     StateQuerySerializer, \
                                                     CitySerializer,\
                                                     CityTableSerializer, \
                                                     CityQuerySerializer
from backend.library.localization.filters import CountryTableFilter, \
                                                 RegionTableFilter, \
                                                 StateTableFilter, \
                                                 CityTableFilter
from backend.core.permissions import IsAdminOrReadOnly, IsAdminAndReadOnly
from backend.core.views import GenericTableViewSet, GenericModelViewSet, \
                               GenericQueryViewSet
                               
class CountryViewSet(GenericModelViewSet):
    app_name = 'localization'
    model_name = 'Country'
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
    filter_licenser = False

class CountryTableViewSet(GenericTableViewSet):
    app_name = 'localization'
    model_name = 'Country'
    queryset = Country.objects.all()
    serializer_class = CountryTableSerializer
    filter_class = CountryTableFilter
    filter_licenser = False

class CountryQueryViewSet(GenericQueryViewSet):
    app_name = 'localization'
    model_name = 'Country'
    queryset = Country.objects.all()
    serializer_class = CountryQuerySerializer
    filter_class = CountryTableFilter
    filter_licenser = False

class RegionViewSet(GenericModelViewSet):
    app_name = 'localization'
    model_name = 'Region'
    queryset = Region.objects.all()
    serializer_class = RegionSerializer
    permission_classes = (IsAdminOrReadOnly,)
    filter_licenser = False

class RegionTableViewSet(GenericTableViewSet):
    app_name = 'localization'
    model_name = 'Region'
    queryset = Region.objects.all()
    serializer_class = RegionTableSerializer
    permission_classes = (IsAdminAndReadOnly,)
    filter_class = RegionTableFilter
    filter_licenser = False

class RegionQueryViewSet(GenericQueryViewSet):
    app_name = 'localization'
    model_name = 'Region'
    queryset = Region.objects.all()
    serializer_class = RegionQuerySerializer
    permission_classes = (IsAdminAndReadOnly,)
    filter_class = RegionTableFilter
    filter_licenser = False

class StateViewSet(GenericModelViewSet):
    app_name = 'localization'
    model_name = 'State'
    queryset = State.objects.all()
    serializer_class = StateSerializer
    permission_classes = (IsAdminOrReadOnly,)
    filter_licenser = False

class StateTableViewSet(GenericTableViewSet):
    app_name = 'localization'
    model_name = 'State'
    queryset = State.objects.all()
    serializer_class = StateTableSerializer
    permission_classes = (IsAdminAndReadOnly,)
    filter_class = StateTableFilter
    filter_licenser = False

class StateQueryViewSet(GenericQueryViewSet):
    app_name = 'localization'
    model_name = 'State'
    queryset = State.objects.all()
    serializer_class = StateQuerySerializer
    permission_classes = (IsAdminAndReadOnly,)
    filter_class = StateTableFilter
    filter_licenser = False

class CityViewSet(GenericModelViewSet):
    app_name = 'localization'
    model_name = 'City'
    queryset = City.objects.all()
    serializer_class = CitySerializer
    permission_classes = (IsAdminOrReadOnly,)
    filter_licenser = False

class CityTableViewSet(GenericTableViewSet):
    app_name = 'localization'
    model_name = 'City'
    queryset = City.objects.all()
    serializer_class = CityTableSerializer
    permission_classes = (IsAdminAndReadOnly,)
    filter_class = CityTableFilter
    filter_licenser = False

class CityQueryViewSet(GenericQueryViewSet):
    app_name = 'localization'
    model_name = 'City'
    queryset = City.objects.all()
    serializer_class = CityQuerySerializer
    permission_classes = (IsAdminAndReadOnly,)
    filter_class = CityTableFilter
    filter_licenser = False
