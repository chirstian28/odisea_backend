from backend.library.localization import views
from rest_framework import routers

router = routers.DefaultRouter()

router.register(r'country', views.CountryViewSet, basename='country')
router.register(r'countrytable', views.CountryTableViewSet, basename='countrytable')
router.register(r'countryquery', views.CountryQueryViewSet, basename='countryquery')
router.register(r'region', views.RegionViewSet, basename='region')
router.register(r'regiontable', views.RegionTableViewSet, basename='regiontable')
router.register(r'regionquery', views.RegionQueryViewSet, basename='regionquery')
router.register(r'state', views.StateViewSet, basename='state')
router.register(r'statetable', views.StateTableViewSet, basename='statetable')
router.register(r'statequery', views.StateQueryViewSet, basename='statequery')
router.register(r'city', views.CityViewSet, basename='city')
router.register(r'citytable', views.CityTableViewSet, basename='citytable')
router.register(r'cityquery', views.CityQueryViewSet, basename='cityquery')

urlpatterns = router.urls


