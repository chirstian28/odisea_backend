from django.db import models

class Country(models.Model):
    id = models.CharField(max_length=5, primary_key=True, verbose_name='ID', help_text='tiny')
    name = models.CharField(max_length=60, db_index=True, verbose_name='Nombre', help_text='large')
    
    def __str__(self):
        return "%s" % (self.name)

    class Meta:
        app_label = 'localization'
        ordering = ('name',)
        verbose_name = 'País'
        verbose_name_plural = 'Paises'

class Region(models.Model):
    country = models.ForeignKey('Country', db_index=True, on_delete=models.CASCADE)
    id = models.CharField(max_length=10, primary_key=True, verbose_name='ID', help_text='tiny')
    name = models.CharField(max_length=60, db_index=True, verbose_name='Nombre', help_text='large')

    def __str__(self):
        return "%s" % self.name

    class Meta:
        app_label = 'localization'
        ordering = ('name',)
        verbose_name = 'Región'
        verbose_name_plural = 'Regiones'


class State(models.Model):
    id = models.CharField(max_length=10, primary_key=True, verbose_name='ID', help_text='tiny')
    country = models.ForeignKey('Country', db_index=True, verbose_name='País', help_text='large', on_delete=models.CASCADE)
    region = models.ForeignKey('Region', null=True, blank=True, db_index=True, verbose_name='Región', on_delete=models.CASCADE)
    name = models.CharField(max_length=60, db_index=True, verbose_name='Nombre', help_text='large')

    def __str__(self):
        return "%s" % self.name

    class Meta:
        app_label = 'localization'
        ordering = ('country', 'name',)
        verbose_name = 'Departamento'
        verbose_name_plural = 'Departamentos'


class City(models.Model):
    id = models.CharField(max_length=10, primary_key=True, verbose_name='ID', help_text='tiny')
    country = models.ForeignKey('Country', db_index=True, verbose_name='País', help_text='large', on_delete=models.CASCADE)
    state = models.ForeignKey('State', db_index=True, verbose_name='Departamento', help_text='large', on_delete=models.CASCADE)
    name = models.CharField(max_length=60, db_index=True, verbose_name='Nombre', help_text='large')

    def __str__(self):
        return "%s" % self.name

    class Meta:
        app_label = 'localization'
        ordering = ('state', 'id',)
        verbose_name = 'Ciudad'
        verbose_name_plural = 'Ciudades'
