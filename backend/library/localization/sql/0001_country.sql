INSERT INTO localization_country (id, name) VALUES ('AD', 'ANDORRA');
INSERT INTO localization_country (id, name) VALUES ('AE', 'UNITED ARAB EMIRATES');
INSERT INTO localization_country (id, name) VALUES ('AF', 'AFGHANISTAN');
INSERT INTO localization_country (id, name) VALUES ('AG', 'ANTIGUA AND BARBUDA');
INSERT INTO localization_country (id, name) VALUES ('AI', 'ANGUILLA');
INSERT INTO localization_country (id, name) VALUES ('AL', 'ALBANIA');
INSERT INTO localization_country (id, name) VALUES ('AM', 'ARMENIA');
INSERT INTO localization_country (id, name) VALUES ('AN', 'NETHERLANDS ANTILLES');
INSERT INTO localization_country (id, name) VALUES ('AO', 'ANGOLA');
INSERT INTO localization_country (id, name) VALUES ('AQ', 'ANTARCTICA');
INSERT INTO localization_country (id, name) VALUES ('AR', 'ARGENTINA');
INSERT INTO localization_country (id, name) VALUES ('AS', 'AMERICAN SAMOA');
INSERT INTO localization_country (id, name) VALUES ('AT', 'AUSTRIA');
INSERT INTO localization_country (id, name) VALUES ('AU', 'AUSTRALIA');
INSERT INTO localization_country (id, name) VALUES ('AW', 'ARUBA');
INSERT INTO localization_country (id, name) VALUES ('AX', 'ALAND ISLANDS');
INSERT INTO localization_country (id, name) VALUES ('AZ', 'AZERBAIJAN');
INSERT INTO localization_country (id, name) VALUES ('BA', 'BOSNIA AND HERZEGOVINA');
INSERT INTO localization_country (id, name) VALUES ('BB', 'BARBADOS');
INSERT INTO localization_country (id, name) VALUES ('BD', 'BANGLADESH');
INSERT INTO localization_country (id, name) VALUES ('BE', 'BELGIUM');
INSERT INTO localization_country (id, name) VALUES ('BF', 'BURKINA FASO');
INSERT INTO localization_country (id, name) VALUES ('BG', 'BULGARIA');
INSERT INTO localization_country (id, name) VALUES ('BH', 'BAHRAIN');
INSERT INTO localization_country (id, name) VALUES ('BI', 'BURUNDI');
INSERT INTO localization_country (id, name) VALUES ('BJ', 'BENIN');
INSERT INTO localization_country (id, name) VALUES ('BM', 'BERMUDA');
INSERT INTO localization_country (id, name) VALUES ('BN', 'BRUNEI DARUSSALAM');
INSERT INTO localization_country (id, name) VALUES ('BO', 'BOLIVIA');
INSERT INTO localization_country (id, name) VALUES ('BR', 'BRAZIL');
INSERT INTO localization_country (id, name) VALUES ('BS', 'BAHAMAS');
INSERT INTO localization_country (id, name) VALUES ('BT', 'BHUTAN');
INSERT INTO localization_country (id, name) VALUES ('BV', 'BOUVET ISLAND');
INSERT INTO localization_country (id, name) VALUES ('BW', 'BOTSWANA');
INSERT INTO localization_country (id, name) VALUES ('BY', 'BELARUS');
INSERT INTO localization_country (id, name) VALUES ('BZ', 'BELIZE');
INSERT INTO localization_country (id, name) VALUES ('CA', 'CANADA');
INSERT INTO localization_country (id, name) VALUES ('CC', 'COCOS (KEELING) ISLANDS');
INSERT INTO localization_country (id, name) VALUES ('CD', 'DEMOCRATIC REPUBLIC OF THE CONGO');
INSERT INTO localization_country (id, name) VALUES ('CF', 'CENTRAL AFRICAN REPUBLIC');
INSERT INTO localization_country (id, name) VALUES ('CG', 'CONGO');
INSERT INTO localization_country (id, name) VALUES ('CH', 'SWITZERLAND');
INSERT INTO localization_country (id, name) VALUES ('CI', 'COTE D''IVOIRE (IVORY COAST)');
INSERT INTO localization_country (id, name) VALUES ('CK', 'COOK ISLANDS');
INSERT INTO localization_country (id, name) VALUES ('CL', 'CHILE');
INSERT INTO localization_country (id, name) VALUES ('CM', 'CAMEROON');
INSERT INTO localization_country (id, name) VALUES ('CN', 'CHINA');
INSERT INTO localization_country (id, name) VALUES ('CO', 'COLOMBIA');
INSERT INTO localization_country (id, name) VALUES ('CR', 'COSTA RICA');
INSERT INTO localization_country (id, name) VALUES ('CS', 'SERBIA AND MONTENEGRO');
INSERT INTO localization_country (id, name) VALUES ('CU', 'CUBA');
INSERT INTO localization_country (id, name) VALUES ('CV', 'CAPE VERDE');
INSERT INTO localization_country (id, name) VALUES ('CX', 'CHRISTMAS ISLAND');
INSERT INTO localization_country (id, name) VALUES ('CY', 'CYPRUS');
INSERT INTO localization_country (id, name) VALUES ('CZ', 'CZECH REPUBLIC');
INSERT INTO localization_country (id, name) VALUES ('DE', 'GERMANY');
INSERT INTO localization_country (id, name) VALUES ('DJ', 'DJIBOUTI');
INSERT INTO localization_country (id, name) VALUES ('DK', 'DENMARK');
INSERT INTO localization_country (id, name) VALUES ('DM', 'DOMINICA');
INSERT INTO localization_country (id, name) VALUES ('DO', 'DOMINICAN REPUBLIC');
INSERT INTO localization_country (id, name) VALUES ('DZ', 'ALGERIA');
INSERT INTO localization_country (id, name) VALUES ('EC', 'ECUADOR');
INSERT INTO localization_country (id, name) VALUES ('EE', 'ESTONIA');
INSERT INTO localization_country (id, name) VALUES ('EG', 'EGYPT');
INSERT INTO localization_country (id, name) VALUES ('EH', 'WESTERN SAHARA');
INSERT INTO localization_country (id, name) VALUES ('ER', 'ERITREA');
INSERT INTO localization_country (id, name) VALUES ('ES', 'SPAIN');
INSERT INTO localization_country (id, name) VALUES ('ET', 'ETHIOPIA');
INSERT INTO localization_country (id, name) VALUES ('FI', 'FINLAND');
INSERT INTO localization_country (id, name) VALUES ('FJ', 'FIJI');
INSERT INTO localization_country (id, name) VALUES ('FK', 'FALKLAND ISLANDS (MALVINAS)');
INSERT INTO localization_country (id, name) VALUES ('FM', 'FEDERATED STATES OF MICRONESIA');
INSERT INTO localization_country (id, name) VALUES ('FO', 'FAROE ISLANDS');
INSERT INTO localization_country (id, name) VALUES ('FR', 'FRANCE');
INSERT INTO localization_country (id, name) VALUES ('FX', 'FRANCE, METROPOLITAN');
INSERT INTO localization_country (id, name) VALUES ('GA', 'GABON');
INSERT INTO localization_country (id, name) VALUES ('GB', 'GREAT BRITAIN (UK)');
INSERT INTO localization_country (id, name) VALUES ('GD', 'GRENADA');
INSERT INTO localization_country (id, name) VALUES ('GE', 'GEORGIA');
INSERT INTO localization_country (id, name) VALUES ('GF', 'FRENCH GUIANA');
INSERT INTO localization_country (id, name) VALUES ('GH', 'GHANA');
INSERT INTO localization_country (id, name) VALUES ('GI', 'GIBRALTAR');
INSERT INTO localization_country (id, name) VALUES ('GL', 'GREENLAND');
INSERT INTO localization_country (id, name) VALUES ('GM', 'GAMBIA');
INSERT INTO localization_country (id, name) VALUES ('GN', 'GUINEA');
INSERT INTO localization_country (id, name) VALUES ('GP', 'GUADELOUPE');
INSERT INTO localization_country (id, name) VALUES ('GQ', 'EQUATORIAL GUINEA');
INSERT INTO localization_country (id, name) VALUES ('GR', 'GREECE');
INSERT INTO localization_country (id, name) VALUES ('GS', 'S. GEORGIA AND S. SANDWICH ISLANDS');
INSERT INTO localization_country (id, name) VALUES ('GT', 'GUATEMALA');
INSERT INTO localization_country (id, name) VALUES ('GU', 'GUAM');
INSERT INTO localization_country (id, name) VALUES ('GW', 'GUINEA-BISSAU');
INSERT INTO localization_country (id, name) VALUES ('GY', 'GUYANA');
INSERT INTO localization_country (id, name) VALUES ('HK', 'HONG KONG');
INSERT INTO localization_country (id, name) VALUES ('HM', 'HEARD ISLAND AND MCDONALD ISLANDS');
INSERT INTO localization_country (id, name) VALUES ('HN', 'HONDURAS');
INSERT INTO localization_country (id, name) VALUES ('HR', 'CROATIA (HRVATSKA)');
INSERT INTO localization_country (id, name) VALUES ('HT', 'HAITI');
INSERT INTO localization_country (id, name) VALUES ('HU', 'HUNGARY');
INSERT INTO localization_country (id, name) VALUES ('ID', 'INDONESIA');
INSERT INTO localization_country (id, name) VALUES ('IE', 'IRELAND');
INSERT INTO localization_country (id, name) VALUES ('IL', 'ISRAEL');
INSERT INTO localization_country (id, name) VALUES ('IN', 'INDIA');
INSERT INTO localization_country (id, name) VALUES ('IO', 'BRITISH INDIAN OCEAN TERRITORY');
INSERT INTO localization_country (id, name) VALUES ('IQ', 'IRAQ');
INSERT INTO localization_country (id, name) VALUES ('IR', 'IRAN');
INSERT INTO localization_country (id, name) VALUES ('IS', 'ICELAND');
INSERT INTO localization_country (id, name) VALUES ('IT', 'ITALY');
INSERT INTO localization_country (id, name) VALUES ('JM', 'JAMAICA');
INSERT INTO localization_country (id, name) VALUES ('JO', 'JORDAN');
INSERT INTO localization_country (id, name) VALUES ('JP', 'JAPAN');
INSERT INTO localization_country (id, name) VALUES ('KE', 'KENYA');
INSERT INTO localization_country (id, name) VALUES ('KG', 'KYRGYZSTAN');
INSERT INTO localization_country (id, name) VALUES ('KH', 'CAMBODIA');
INSERT INTO localization_country (id, name) VALUES ('KI', 'KIRIBATI');
INSERT INTO localization_country (id, name) VALUES ('KM', 'COMOROS');
INSERT INTO localization_country (id, name) VALUES ('KN', 'SAINT KITTS AND NEVIS');
INSERT INTO localization_country (id, name) VALUES ('KP', 'KOREA (NORTH)');
INSERT INTO localization_country (id, name) VALUES ('KR', 'KOREA (SOUTH)');
INSERT INTO localization_country (id, name) VALUES ('KW', 'KUWAIT');
INSERT INTO localization_country (id, name) VALUES ('KY', 'CAYMAN ISLANDS');
INSERT INTO localization_country (id, name) VALUES ('KZ', 'KAZAKHSTAN');
INSERT INTO localization_country (id, name) VALUES ('LA', 'LAOS');
INSERT INTO localization_country (id, name) VALUES ('LB', 'LEBANON');
INSERT INTO localization_country (id, name) VALUES ('LC', 'SAINT LUCIA');
INSERT INTO localization_country (id, name) VALUES ('LI', 'LIECHTENSTEIN');
INSERT INTO localization_country (id, name) VALUES ('LK', 'SRI LANKA');
INSERT INTO localization_country (id, name) VALUES ('LR', 'LIBERIA');
INSERT INTO localization_country (id, name) VALUES ('LS', 'LESOTHO');
INSERT INTO localization_country (id, name) VALUES ('LT', 'LITHUANIA');
INSERT INTO localization_country (id, name) VALUES ('LU', 'LUXEMBOURG');
INSERT INTO localization_country (id, name) VALUES ('LV', 'LATVIA');
INSERT INTO localization_country (id, name) VALUES ('LY', 'LIBYA');
INSERT INTO localization_country (id, name) VALUES ('MA', 'MOROCCO');
INSERT INTO localization_country (id, name) VALUES ('MC', 'MONACO');
INSERT INTO localization_country (id, name) VALUES ('MD', 'MOLDOVA');
INSERT INTO localization_country (id, name) VALUES ('MG', 'MADAGASCAR');
INSERT INTO localization_country (id, name) VALUES ('MH', 'MARSHALL ISLANDS');
INSERT INTO localization_country (id, name) VALUES ('MK', 'MACEDONIA');
INSERT INTO localization_country (id, name) VALUES ('ML', 'MALI');
INSERT INTO localization_country (id, name) VALUES ('MM', 'MYANMAR');
INSERT INTO localization_country (id, name) VALUES ('MN', 'MONGOLIA');
INSERT INTO localization_country (id, name) VALUES ('MO', 'MACAO');
INSERT INTO localization_country (id, name) VALUES ('MP', 'NORTHERN MARIANA ISLANDS');
INSERT INTO localization_country (id, name) VALUES ('MQ', 'MARTINIQUE');
INSERT INTO localization_country (id, name) VALUES ('MR', 'MAURITANIA');
INSERT INTO localization_country (id, name) VALUES ('MS', 'MONTSERRAT');
INSERT INTO localization_country (id, name) VALUES ('MT', 'MALTA');
INSERT INTO localization_country (id, name) VALUES ('MU', 'MAURITIUS');
INSERT INTO localization_country (id, name) VALUES ('MV', 'MALDIVES');
INSERT INTO localization_country (id, name) VALUES ('MW', 'MALAWI');
INSERT INTO localization_country (id, name) VALUES ('MX', 'MEXICO');
INSERT INTO localization_country (id, name) VALUES ('MY', 'MALAYSIA');
INSERT INTO localization_country (id, name) VALUES ('MZ', 'MOZAMBIQUE');
INSERT INTO localization_country (id, name) VALUES ('NA', 'NAMIBIA');
INSERT INTO localization_country (id, name) VALUES ('NC', 'NEW CALEDONIA');
INSERT INTO localization_country (id, name) VALUES ('NE', 'NIGER');
INSERT INTO localization_country (id, name) VALUES ('NF', 'NORFOLK ISLAND');
INSERT INTO localization_country (id, name) VALUES ('NG', 'NIGERIA');
INSERT INTO localization_country (id, name) VALUES ('NI', 'NICARAGUA');
INSERT INTO localization_country (id, name) VALUES ('NL', 'NETHERLANDS');
INSERT INTO localization_country (id, name) VALUES ('NO', 'NORWAY');
INSERT INTO localization_country (id, name) VALUES ('NP', 'NEPAL');
INSERT INTO localization_country (id, name) VALUES ('NR', 'NAURU');
INSERT INTO localization_country (id, name) VALUES ('NU', 'NIUE');
INSERT INTO localization_country (id, name) VALUES ('NZ', 'NEW ZEALAND (AOTEAROA)');
INSERT INTO localization_country (id, name) VALUES ('OM', 'OMAN');
INSERT INTO localization_country (id, name) VALUES ('PA', 'PANAMA');
INSERT INTO localization_country (id, name) VALUES ('PE', 'PERU');
INSERT INTO localization_country (id, name) VALUES ('PF', 'FRENCH POLYNESIA');
INSERT INTO localization_country (id, name) VALUES ('PG', 'PAPUA NEW GUINEA');
INSERT INTO localization_country (id, name) VALUES ('PH', 'PHILIPPINES');
INSERT INTO localization_country (id, name) VALUES ('PK', 'PAKISTAN');
INSERT INTO localization_country (id, name) VALUES ('PL', 'POLAND');
INSERT INTO localization_country (id, name) VALUES ('PM', 'SAINT PIERRE AND MIQUELON');
INSERT INTO localization_country (id, name) VALUES ('PN', 'PITCAIRN');
INSERT INTO localization_country (id, name) VALUES ('PR', 'PUERTO RICO');
INSERT INTO localization_country (id, name) VALUES ('PS', 'PALESTINIAN TERRITORY');
INSERT INTO localization_country (id, name) VALUES ('PT', 'PORTUGAL');
INSERT INTO localization_country (id, name) VALUES ('PW', 'PALAU');
INSERT INTO localization_country (id, name) VALUES ('PY', 'PARAGUAY');
INSERT INTO localization_country (id, name) VALUES ('QA', 'QATAR');
INSERT INTO localization_country (id, name) VALUES ('RE', 'REUNION');
INSERT INTO localization_country (id, name) VALUES ('RO', 'ROMANIA');
INSERT INTO localization_country (id, name) VALUES ('RU', 'RUSSIAN FEDERATION');
INSERT INTO localization_country (id, name) VALUES ('RW', 'RWANDA');
INSERT INTO localization_country (id, name) VALUES ('SA', 'SAUDI ARABIA');
INSERT INTO localization_country (id, name) VALUES ('SB', 'SOLOMON ISLANDS');
INSERT INTO localization_country (id, name) VALUES ('SC', 'SEYCHELLES');
INSERT INTO localization_country (id, name) VALUES ('SD', 'SUDAN');
INSERT INTO localization_country (id, name) VALUES ('SE', 'SWEDEN');
INSERT INTO localization_country (id, name) VALUES ('SG', 'SINGAPORE');
INSERT INTO localization_country (id, name) VALUES ('SH', 'SAINT HELENA');
INSERT INTO localization_country (id, name) VALUES ('SI', 'SLOVENIA');
INSERT INTO localization_country (id, name) VALUES ('SJ', 'SVALBARD AND JAN MAYEN');
INSERT INTO localization_country (id, name) VALUES ('SK', 'SLOVAKIA');
INSERT INTO localization_country (id, name) VALUES ('SL', 'SIERRA LEONE');
INSERT INTO localization_country (id, name) VALUES ('SM', 'SAN MARINO');
INSERT INTO localization_country (id, name) VALUES ('SN', 'SENEGAL');
INSERT INTO localization_country (id, name) VALUES ('SO', 'SOMALIA');
INSERT INTO localization_country (id, name) VALUES ('SR', 'SURIname');
INSERT INTO localization_country (id, name) VALUES ('ST', 'SAO TOME AND PRINCIPE');
INSERT INTO localization_country (id, name) VALUES ('SU', 'USSR (FORMER)');
INSERT INTO localization_country (id, name) VALUES ('SV', 'EL SALVADOR');
INSERT INTO localization_country (id, name) VALUES ('SY', 'SYRIA');
INSERT INTO localization_country (id, name) VALUES ('SZ', 'SWAZILAND');
INSERT INTO localization_country (id, name) VALUES ('TC', 'TURKS AND CAICOS ISLANDS');
INSERT INTO localization_country (id, name) VALUES ('TD', 'CHAD');
INSERT INTO localization_country (id, name) VALUES ('TF', 'FRENCH SOUTHERN TERRITORIES');
INSERT INTO localization_country (id, name) VALUES ('TG', 'TOGO');
INSERT INTO localization_country (id, name) VALUES ('TH', 'THAILAND');
INSERT INTO localization_country (id, name) VALUES ('TJ', 'TAJIKISTAN');
INSERT INTO localization_country (id, name) VALUES ('TK', 'TOKELAU');
INSERT INTO localization_country (id, name) VALUES ('TL', 'TIMOR-LESTE');
INSERT INTO localization_country (id, name) VALUES ('TM', 'TURKMENISTAN');
INSERT INTO localization_country (id, name) VALUES ('TN', 'TUNISIA');
INSERT INTO localization_country (id, name) VALUES ('TO', 'TONGA');
INSERT INTO localization_country (id, name) VALUES ('TP', 'EAST TIMOR');
INSERT INTO localization_country (id, name) VALUES ('TR', 'TURKEY');
INSERT INTO localization_country (id, name) VALUES ('TT', 'TRINIDAD AND TOBAGO');
INSERT INTO localization_country (id, name) VALUES ('TV', 'TUVALU');
INSERT INTO localization_country (id, name) VALUES ('TW', 'TAIWAN');
INSERT INTO localization_country (id, name) VALUES ('TZ', 'TANZANIA');
INSERT INTO localization_country (id, name) VALUES ('UA', 'UKRAINE');
INSERT INTO localization_country (id, name) VALUES ('UG', 'UGANDA');
INSERT INTO localization_country (id, name) VALUES ('UK', 'UNITED KINGDOM');
INSERT INTO localization_country (id, name) VALUES ('UM', 'UNITED STATES MINOR OUTLYING ISLANDS');
INSERT INTO localization_country (id, name) VALUES ('US', 'UNITED STATES');
INSERT INTO localization_country (id, name) VALUES ('UY', 'URUGUAY');
INSERT INTO localization_country (id, name) VALUES ('UZ', 'UZBEKISTAN');
INSERT INTO localization_country (id, name) VALUES ('VA', 'VATICAN CITY STATE (HOLY SEE)');
INSERT INTO localization_country (id, name) VALUES ('VC', 'SAINT VINCENT AND THE GRENADINES');
INSERT INTO localization_country (id, name) VALUES ('VE', 'VENEZUELA');
INSERT INTO localization_country (id, name) VALUES ('VG', 'VIRGIN ISLANDS (BRITISH)');
INSERT INTO localization_country (id, name) VALUES ('VI', 'VIRGIN ISLANDS (U.S.)');
INSERT INTO localization_country (id, name) VALUES ('VN', 'VIET NAM');
INSERT INTO localization_country (id, name) VALUES ('VU', 'VANUATU');
INSERT INTO localization_country (id, name) VALUES ('WF', 'WALLIS AND FUTUNA');
INSERT INTO localization_country (id, name) VALUES ('WS', 'SAMOA');
INSERT INTO localization_country (id, name) VALUES ('YE', 'YEMEN');
INSERT INTO localization_country (id, name) VALUES ('YT', 'MAYOTTE');
INSERT INTO localization_country (id, name) VALUES ('YU', 'YUGOSLAVIA (FORMER)');
INSERT INTO localization_country (id, name) VALUES ('ZA', 'SOUTH AFRICA');
INSERT INTO localization_country (id, name) VALUES ('ZM', 'ZAMBIA');
INSERT INTO localization_country (id, name) VALUES ('ZR', 'ZAIRE (FORMER)');
INSERT INTO localization_country (id, name) VALUES ('ZW', 'ZIMBABWE');

