import django_filters

from backend.library.localization.models import City, State, Country, Region

class CityTableFilter(django_filters.rest_framework.FilterSet):

    class Meta:
        model = City
        fields = ['name', 'country', 'state']

class RegionTableFilter(django_filters.rest_framework.FilterSet):

    class Meta:
        model = Region
        fields = ['country','name']

class StateTableFilter(django_filters.rest_framework.FilterSet):

    class Meta:
        model = State
        fields = ['country','name']

class CountryTableFilter(django_filters.rest_framework.FilterSet):

    class Meta:
        model = Country
        fields = ['name']
