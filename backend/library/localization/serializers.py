# -*- coding: utf-8 -*-
from rest_framework import serializers
from backend.library.localization.models import Country, Region, State, City
from backend.core.models import ResourceHost

class CountrySerializer(serializers.ModelSerializer):

    class Meta:
        model = Country
        fields = ("id", "name")

class CountryTableSerializer(serializers.ModelSerializer):

    class Meta:
        model = Country
        fields = ("id", "name",)
        search_fields = ['^name', ]

class CountryQuerySerializer(serializers.ModelSerializer):

    class Meta:
        model = Country
        fields = ("id", "name")

class RegionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Region
        fields = ("id", "name", "country", )

class RegionTableSerializer(serializers.ModelSerializer):
    country = serializers.CharField()
    
    class Meta:
        model = Region
        filter_fields = ("country",)
        fields = ("id", "name","country",)
        search_fields = ['^name', ]

class RegionQuerySerializer(serializers.ModelSerializer):

    class Meta:
        model = Region
        fields = ("id", "name")

class StateSerializer(serializers.ModelSerializer):

    class Meta:
        model = State
        fields = ("id", "name", "country", "region", )
        fields_actions = {
                            'country': { 'changes': ['region'] },
                            'region': { 'fill': { 'url': ResourceHost.getBase() + '/localization/regionquery/', 
                                                 'parameters': ['country'],
                                             },
                            },
        }                            

class StateTableSerializer(serializers.ModelSerializer):
    country = serializers.CharField()
    
    class Meta:
        model = State
        filter_fields = ("country",)
        fields = ("id", "name","country",)
        search_fields = ['^name', ]

class StateQuerySerializer(serializers.ModelSerializer):

    class Meta:
        model = State
        filter_fields = ("country",)
        fields = ("id", "name")

class CityTableSerializer(serializers.ModelSerializer):
    country = serializers.CharField()
    state = serializers.CharField()

    class Meta:
        model = City
        fields = ("id", "name", "state","country", )
        filter_fields = ("state","country",)
        search_fields = ['^name', ]

class CityQuerySerializer(serializers.ModelSerializer):

    class Meta:
        model = City
        fields = ("id", "name")
        filter_fields = ("state","country",)

class CitySerializer(serializers.ModelSerializer):

    class Meta:
        model = City
        fields = ("id", "name", "country", "state", )
        fields_actions = {
                            'country': { 'changes': ['state'] },
                            'state': { 'fill': { 'url': ResourceHost.getBase() + '/localization/statequery/', 
                                                 'parameters': ['country'],
                                             },
                            },
        }                            

