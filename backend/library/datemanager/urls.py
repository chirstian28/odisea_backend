from backend.library.datemanager import views, converters
from rest_framework import routers
from django.urls import path, re_path, register_converter

register_converter(converters.ISODateConverter, 'isodate')

router = routers.DefaultRouter()

router.register(r'monthnametable', views.MonthNameTableViewSet, basename='monthnametable')
router.register(r'daynametable', views.DayNameTableViewSet, basename='daynametable')
router.register(r'holidaytable', views.HolidayTableViewSet, basename='holidaytable')
router.register(r'holiday', views.HolidayViewSet, basename='holiday')
 
urlpatterns = [
            path('isbusinessday/<isodate:date>/', views.DateIsBusinessDayView.as_view()),
            path('nextbusinessday/<isodate:date>/', views.NextBusinessDayView.as_view()),
            path('previousbusinessday/<isodate:date>/', views.PreviousBusinessDayView.as_view()),
            path('addbusinessday/<isodate:date>/<int:days>/', views.AddBusinessDayView.as_view()),
            path('subtractbusinessday/<isodate:date>/<int:days>/', views.SubtractBusinessDayView.as_view()),
            path('monthlastdate/<isodate:date>/', views.MonthLastDateView.as_view()),
            path('monthlastdatenumber/<int:year>/<int:month>/', views.MonthLastDateNumberView.as_view()),
            path('daterange/<isodate:from_date>/<isodate:to_date>/', views.DateRangeView.as_view()),
            path('getholidaylist/<isodate:from_date>/<isodate:to_date>/', views.getHolidayListView.as_view()),
            path('translatemonthname/<str:month_text>/', views.TranslateMonthNameView.as_view()),
            path('verbosemonth/<int:month_number>/', views.VerboseMonthView.as_view()),
            path('verboseday/<isodate:date>/', views.VerboseDayView.as_view()),
            path('calendarmatrix/<int:year>/<int:month>/', views.CalendarMatrixView.as_view()),
            path('getmountlist/', views.MountListView.as_view()),
            re_path(r'^verbosenumberday/(?P<day_number>[0-6])/$', views.VerboseNumberDayView.as_view()),
]

urlpatterns += router.urls


