INSERT INTO datemanager_monthname (id, name, short_name) VALUES (1, 'Enero', 'jan');
INSERT INTO datemanager_monthname (id, name, short_name) VALUES (2, 'Febrero', 'feb');
INSERT INTO datemanager_monthname (id, name, short_name) VALUES (3, 'Marzo', 'mar');
INSERT INTO datemanager_monthname (id, name, short_name) VALUES (4, 'Abril', 'apr');
INSERT INTO datemanager_monthname (id, name, short_name) VALUES (5, 'Mayo', 'may');
INSERT INTO datemanager_monthname (id, name, short_name) VALUES (6, 'Junio', 'jun');
INSERT INTO datemanager_monthname (id, name, short_name) VALUES (7, 'Julio', 'jul');
INSERT INTO datemanager_monthname (id, name, short_name) VALUES (8, 'Agosto', 'aug');
INSERT INTO datemanager_monthname (id, name, short_name) VALUES (9, 'Septiembre', 'sep');
INSERT INTO datemanager_monthname (id, name, short_name) VALUES (10, 'Octubre', 'oct');
INSERT INTO datemanager_monthname (id, name, short_name) VALUES (11, 'Noviembre', 'nov');
INSERT INTO datemanager_monthname (id, name, short_name) VALUES (12, 'Diciembre', 'dec');

