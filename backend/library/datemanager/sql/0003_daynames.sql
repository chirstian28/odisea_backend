INSERT INTO datemanager_dayname (id, name, short_name) VALUES (0, 'Lunes', 'mon');
INSERT INTO datemanager_dayname (id, name, short_name) VALUES (1, 'Martes', 'tue');
INSERT INTO datemanager_dayname (id, name, short_name) VALUES (2, 'Miercoles', 'wed');
INSERT INTO datemanager_dayname (id, name, short_name) VALUES (3, 'Jueves', 'thu');
INSERT INTO datemanager_dayname (id, name, short_name) VALUES (4, 'Viernes', 'fri');
INSERT INTO datemanager_dayname (id, name, short_name) VALUES (5, 'Sábado', 'sat');
INSERT INTO datemanager_dayname (id, name, short_name) VALUES (6, 'Domingo', 'sun');

