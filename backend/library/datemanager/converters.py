import datetime

class ISODateConverter:
    regex = '[0-9]{4}-[0-9]{2}-[0-9]{2}'

    def to_python(self, value):
        try:
            date = datetime.datetime.strptime(value, '%Y-%m-%d').date()
        except:
            date = None
        return date

    def to_url(self, value):
        return value.strftime('%Y-%m-%d') % value

class CODateConverter:
    regex = '[0-9]{2}-[0-9]{2}-[0-9]{4}'

    def to_python(self, value):
        try:
            date = datetime.datetime.strptime(value, '%d-%m-%Y').date()
        except:
            date = None
        return date

    def to_url(self, value):
        return value.strftime('%d-%m-%Y') % value


class NOSeparatorConverter:
    regex = '[1-2][0-9]{7}'

    def to_python(self, value):
        try:
            date = datetime.datetime.strptime(value, '%Y%m%d').date()
        except:
            date = None
        return date

    def to_url(self, value):
        return value.strftime('%Y%m%d') % value

class NOSeparatorAAConverter:
    regex = '[0-9]{6}'

    def to_python(self, value):
        try:
            date = datetime.datetime.strptime(value, '%y%m%d').date()
        except:
            date = None
        return date

    def to_url(self, value):
        return value.strftime('%y%m%d') % value
