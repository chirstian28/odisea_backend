from django.db import models
import datetime
 
class MonthName(models.Model):
    id = models.IntegerField(primary_key=True, verbose_name='ID')
    name = models.CharField(max_length=10, verbose_name='Nombre')
    short_name = models.CharField(max_length=3, verbose_name='Nombre Corto')
     
    def __str__(self):
        return u"%s" % (self.name)
         
    class Meta:
        ordering = ['id']
        app_label = 'datemanager'
        verbose_name = 'Mes'
        verbose_name_plural = 'Meses'

class DayName(models.Model):
    id = models.IntegerField(primary_key=True, verbose_name='ID')
    name = models.CharField(max_length=10, verbose_name='Nombre')
    short_name = models.CharField(max_length=3, verbose_name='Nombre Corto')
     
    def __str__(self):
        return u"%s" % (self.name)
         
    class Meta:
        ordering = ['id']
        app_label = 'datemanager'
        verbose_name = 'Día'
        verbose_name_plural = 'Días'
 
class Holiday(models.Model):
    day = models.DateField(primary_key=True, verbose_name='Fecha')
    name = models.CharField(max_length=200,null=True,blank=True, verbose_name='Nombre Festivo')
 
    def __str__(self):
        return self.name + " " + str(self.day)
     
    def is_business_day(date, saturday_too=False, getdict=True):
        data_result = {
                    'date': date,
                    'result': True
                  }
        
        if date.weekday() == 6:
            data_result['result'] = False
        elif not saturday_too:
            if date.weekday() == 5:
                data_result['result'] = False
        else:
            holiday_list = Holiday.objects.filter(day=date)
            if len(holiday_list) > 0:
                data_result['result'] = False
        if getdict:
            return data_result
        else:
            return data_result['result']
    
    is_business_day = staticmethod(is_business_day)
     
    def next_business_day(date, saturday_too=False, getdict=True):
        data_result = {
                    'date': date,
                    'next_date': None
                  }

        next_date = date + datetime.timedelta(days=1)
        while not Holiday.is_business_day(next_date, saturday_too, getdict=False):
            next_date += datetime.timedelta(days=1)
        data_result['next_date'] = next_date
        if getdict:
            return data_result
        else:
            return data_result['next_date']
     
    next_business_day = staticmethod(next_business_day)

    def get_holidays(from_date, to_date, getdict=True):
        data_result = {
            'end_date': from_date,
            'start_date': to_date,
            'holidays': []
        }

        data_result['holidays'] = Holiday.objects.filter(day__range=(from_date, to_date)).values_list('day', flat=True)
        if getdict:
            return data_result
        else:
            return data_result['holidays']

    get_holidays = staticmethod(get_holidays)

    def get_sundays(from_date, to_date, getdict=True):
        data_result = {
            'end_date': from_date,
            'start_date': to_date,
            'sundays': []
        }
        date = from_date
        while date <= to_date:
            if date.weekday() == 6:
                data_result['sundays'].append(date)
            date += datetime.timedelta(days=1)
        if getdict:
            return data_result
        else:
            return data_result['sundays']

    get_sundays = staticmethod(get_sundays)

    def previous_business_day(date, saturday_too=False, getdict=True):
        data_result = {
                    'date': date,
                    'previous_date': None
                  }
        previous_date = date - datetime.timedelta(days=1)
        while not Holiday.is_business_day(previous_date, saturday_too, getdict=False):
            previous_date -= datetime.timedelta(days=1)
        data_result['previous_date'] = previous_date
        if getdict:
            return data_result
        else:
            return data_result['previous_date']
     
    previous_business_day = staticmethod(previous_business_day)
 
    def add_business_day(date, days, saturday_too=False, getdict=True):
        data_result = {
                    'date': date,
                    'new_date': None
                  }
        next_date = date
        for i in range(0, days):
            next_date = Holiday.next_business_day(next_date, saturday_too, getdict=False)
        data_result['new_date'] = next_date
        if getdict:
            return data_result
        else:
            return data_result['new_date']
     
    add_business_day = staticmethod(add_business_day)
 
    def substract_business_day(date, days, saturday_too=False, getdict=True):
        data_result = {
                    'date': date,
                    'new_date': None
                  }
        previous_date = date
        for i in range(0, days):
            previous_date = Holiday.previous_business_day(previous_date, saturday_too, getdict=False)
        data_result['new_date'] = previous_date
        if getdict:
            return data_result
        else:
            return data_result['new_date']
     
    substract_business_day = staticmethod(substract_business_day)
     
    def month_last_date(date, getdict=True):
        data_result = {
                    'date': date,
                    'last_date': None
                  }
        repeat = True
        day = 31
        last_date = None
        while repeat:
            try:
                repeat = False
                last_date = datetime.date(date.year, date.month, day)
            except:
                day -= 1
                repeat = True
        data_result['last_date'] = last_date
        if getdict:
            return data_result
        else:
            return data_result['last_date']
         
    month_last_date = staticmethod(month_last_date)
 
    def month_last_date_number(month, year=None, getdict=True):
        if year is None:
            year = datetime.date.today().year
        data_result = {
                    'year': year,
                    'month': month,
                    'last_date': None
                  }
            
        date = datetime.date(year,month,1)
        data_result['last_date'] = Holiday.month_last_date(date=date, getdict=False)
        if getdict:
            return data_result
        else:
            return data_result['last_date']
         
    month_last_date_number = staticmethod(month_last_date_number)
 
    def dateRange(from_date, to_date, getdict=True):
        date = from_date
        data_result = {
                    'from_date': from_date,
                    'to_date': to_date,
                    'date_list': []
                  }
        
        while date <= to_date:
            if getdict:
                element = {'date': date}
            else:
                element = date
            data_result['date_list'].append(element)
            date += datetime.timedelta(days=1)
            
        if getdict:
            return data_result
        else:
            return data_result['date_list']

    dateRange = staticmethod(dateRange)
     
    def translate_text_name (month_text, getdict=True):
        data_result = {
                    'month': month_text,
                    'month_number': 0,
                  }
        month_text = month_text.upper()
        if month_text in ['JAN', 'JANUARY', 'ENERO', 'ENE']:
            data_result['month_number'] = 1
        if month_text in ['FEB', 'FEBRUARY', 'FEBRERO']:
            data_result['month_number'] =  2
        if month_text in ['MAR', 'MARCH', 'MARZO']:
            data_result['month_number'] =  3
        if month_text in ['APR', 'APRIL', 'ABRIL', 'ABR']:
            data_result['month_number'] =  4
        if month_text in ['MAY', 'MAYO']:
            data_result['month_number'] =  5
        if month_text in ['JUN', 'JUNE', 'JUNIO']:
            data_result['month_number'] =  6
        if month_text in ['JUL', 'JULY', 'JULIO']:
            data_result['month_number'] =  7
        if month_text in ['AUG', 'AUGUST', 'AGOSTO', 'AGO']:
            data_result['month_number'] =  8
        if month_text in ['SEP', 'SEPTEMBER', 'SEPTIEMBRE']:
            data_result['month_number'] =  9
        if month_text in ['OCT', 'OCTOBER', 'OCTUBRE']:
            data_result['month_number'] =  10
        if month_text in ['NOV', 'NOVEMBER', 'NOVIEMBRE']:
            data_result['month_number'] =  11
        if month_text in ['DEC', 'DECEMBER', 'DICIEMBRE', 'DIC']:
            data_result['month_number'] = 12
        data_result['month_name'] = Holiday.verbose_month(data_result['month_number'])
        if getdict:
            return data_result
        else:
            return data_result['month_number']
         
    translate_text_name = staticmethod(translate_text_name)

    def verbose_month(month_number, getdict=True):
        data_result = {
                    'month_number': month_number,
                    'month': '',
                  }
        if month_number == 1:
            data_result['month'] = 'Enero'
        if month_number == 2:
            data_result['month'] = 'Febrero'
        if month_number == 3:
            data_result['month'] = 'Marzo'
        if month_number == 4:
            data_result['month'] = 'Abril'
        if month_number == 5:
            data_result['month'] = 'Mayo'
        if month_number == 6:
            data_result['month'] = 'Junio'
        if month_number == 7:
            data_result['month'] = 'Julio'
        if month_number == 8:
            data_result['month'] = 'Agosto'
        if month_number == 9:
            data_result['month'] = 'Septiembre'
        if month_number == 10:
            data_result['month'] = 'Octubre'
        if month_number == 11:
            data_result['month'] = 'Noviembre'
        if month_number == 12:
            data_result['month'] = 'Diciembre'
        if getdict:
            return data_result
        else:
            return data_result['month']
 
    verbose_month = staticmethod(verbose_month)
     
    def verbose_day(date, getdict=True):
        data_result = {
                    'date': date,
                    'day_name': '',
                  }

        day_number = date.weekday()
        data_result['day'] = day_number
        data_result['day_name'] =  Holiday.verbose_number_day(day_number, getdict)
        if getdict:
            return data_result
        else:
            return data_result['day_name']
     
    verbose_day = staticmethod(verbose_day)
     
    def verbose_number_day(day_number, getdict=True):
        data_result = {
                    'day_number': day_number,
                    'day_name': '',
                  }

        if day_number == 0:
            data_result['day_name'] = u'Lunes'
        elif day_number == 1:
            data_result['day_name'] = u'Martes'
        elif day_number == 2:
            data_result['day_name'] = u'Miércoles'
        elif day_number == 3:
            data_result['day_name'] = u'Jueves'
        elif day_number == 4:
            data_result['day_name'] = u'Viernes'
        elif day_number == 5:
            data_result['day_name'] = u'Sábado'
        elif day_number == 6:
            data_result['day_name'] = u'Domingo'
        else:
            data_result['day_name'] = u''
        if getdict:
            return data_result
        else:
            return data_result['day_name']
     
    verbose_number_day = staticmethod(verbose_number_day)
     
    def getMountList():
        result = []
        for i in range(1, 13):
            result.append(Holiday.verbose_month(i))
        return result
     
    getMountList = staticmethod(getMountList)
             
    def CalendarMatrix(month, year, getdict=True):
        data_result = {
           'year': year,
           'month': month,
           'calendar': None
         }

        import calendar
        c = calendar.Calendar()
        data_result['calendar'] = c.monthdayscalendar (year,month)
        if getdict:
            return data_result
        else:
            return data_result['calendar']
     
    CalendarMatrix = staticmethod(CalendarMatrix)
 
    class Meta:
        app_label = 'datemanager'
        ordering = ('day',)
        verbose_name = 'Festivo'
        verbose_name_plural = 'Festivos'

