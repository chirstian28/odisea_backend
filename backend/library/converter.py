# -*- coding: utf-8 -*-
#!/usr/bin/env python

import os
import sys
import shutil
from chardet.universaldetector import UniversalDetector

def try_encoding(full_path):
    # gather the encodings you think that the file may be
    # encoded inside a tuple
    encodings = ['UTF-8', 'ISO-8859-1', 'WINDOWS-1252', 'WINDOWS-1253', 'ASCII',
                 'CP850', 'WINDOWS-1250', 'WINDOWS-1251', 'WINDOWS-1254', 'WINDOWS-1255',
                 'WINDOWS-1256', 'WINDOWS-1257', 'WINDOWS-1258', 'ISO-8859-7',
                 'MACGREEK', 'LATIN1']

    confidence = 0
    try:
        f = open(full_path, 'r')
        data = f.read()
    except Exception:
        print("file not found: %s" % full_path)
        sys.exit(1)

    for enc in encodings:
        try:
            data.decode(enc)
            confidence = 1
            break
        except Exception:
            confidence = 0
    
    f.close()
    return confidence, enc

def guess_encoding(full_path):
    detector = UniversalDetector()
    fp = open(full_path)
    count = 0
    for line in fp:
        count += 1
        detector.feed(line)
        if detector.done:
            break
        if count > 1000:
            break
    fp.close()
    detector.close()
    #print(detector.result["encoding"].upper())
    #print(detector.result["confidence"])
    return detector.result["confidence"], detector.result["encoding"].upper()

def set_enconding_to_utf8(path, filename, encoding):
    backup_filename = filename + ".bak"
    shutil.move(path + "/" + filename, path + "/" + backup_filename)
    shutil.copy(path + "/" + backup_filename, path + "/" + filename)
    #cmd = "/usr/bin/iconv -f %s -t %s -o %s %s" % (encoding, "UTF-8", path + "/" +  filename, path + "/" +  backup_filename)
    #print(cmd)
    #os.system(cmd)
    return backup_filename
            
def convert_to_utf8(full_path, only_guess=True):
    filename = os.path.basename(full_path)
    path = os.path.dirname(full_path)
    if only_guess:
        confidence, encoding = guess_encoding(full_path)
        bak_filename = set_enconding_to_utf8(path, filename, encoding)
    else:
        confidence, encoding = try_encoding(full_path) # this method is brute force
        print ("confidence: %s, encoding: %s" % (confidence, encoding) )
        if confidence == 1:
            if encoding != "UTF-8":
                bak_filename = set_enconding_to_utf8(path, filename, encoding)
        else:
            confidence, encoding = guess_encoding(full_path)
            bak_filename = set_enconding_to_utf8(path, filename, encoding)
        
    return confidence, encoding, bak_filename

#if __name__ == '__main__':
#    convert_to_utf8(sys.argv[1])
