# -*- coding:utf-8 -*-
import os
import logging as _logging 
from django.conf import settings

def getLoggingPath (companent_name, logging_filename):
    logging_dir = settings.LOG_ROOT + companent_name + '/' 
    logging_filepath = logging_dir + logging_filename
    try:
        os.makedirs(logging_dir)
    except OSError:
        pass
    format_desc='%(asctime)s %(levelname)s %(message)s'
    _logging.basicConfig(level=_logging.DEBUG,
          format=format_desc,
          filename=logging_filepath,
          filemode='a')
    
    logger = _logging.getLogger('SOAP_MAIN_LOG_'+companent_name.upper())
    hdlr = _logging.FileHandler(logging_filepath)
    formatter = _logging.Formatter(format_desc)
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(_logging.DEBUG)
    
    return logger, hdlr


    
