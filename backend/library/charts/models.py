from django.db import models

class ChartClass(models.Model):

    graph_width = 1000
    graph_space = 50
    graph_height = 400
    graph_colors = [
                '#008ffb',
                '#00e396',
                '#feb019',
                '#ff4560',
                '#775dd0',
                '#1215EE',
                '#1ba41e',
                '#e85919',
                '#FF0000',
                '#e022f2',
                '#3c4b4e',
                '#0ecbed',
        ]        

    @staticmethod
    def generate2DimensionsChart(chart_options, quantity=1):
          
        data = {}
        if 'key' in chart_options.keys():
            data['key'] = chart_options['key']

        if 'type' not in chart_options.keys():
            data['type'] = 'line'
        else:
            data['type'] = chart_options['type']

        data['options'] = {'dataLabels': {
                                        'enabled': False
                                    },
                            'grid': {
                                'row': {
                                'colors': ['#f3f3f3', 'transparent'], 
                                'opacity': 0.5
                                },
                            },
                            'fill': {
                                'opacity': 1
                            },
                    }
        data['options']['chart'] = {
                                        'width': int(ChartClass.graph_width / quantity) - (ChartClass.graph_space * (quantity - 1) ),
                                        'height': ChartClass.graph_height,
                                        'type': data['type'],
                                        'zoom': {
                                                    'enabled': False
                                            }
                                }
        if 'stacked' in chart_options.keys() and chart_options['stacked']:
            data['options']['chart']['stacked'] = True

        if data['type'] == 'bar' and 'horizontal' in chart_options.keys() and chart_options['horizontal']:
            data['options']['plotOptions'] = {
                                                'bar': {
                                                    'horizontal': True,
                                                },
                                        }
        if 'legend' in chart_options.keys() and chart_options['legend']:
            data['options']['legend'] = chart_options['legend']

        if 'curve' in chart_options.keys():
            data['options']['stroke'] = {
                                        'curve': chart_options['curve']
                                    }
        if 'title' in chart_options.keys():
            data['options']['title'] = {
                                        'text': chart_options['title'],
                                        'align': 'left'
                                    }
        if data['type'] in ('line', 'bar',):
            data['options']['xaxis'] = {
                                        'categories': chart_options['categories'],
                                    }
            if 'x_label' in chart_options.keys():
                data['options']['xaxis']['title'] = {
                                            'text': chart_options['x_label'],
                                        }
            if 'export_data' in chart_options.keys():
                data['options']['xaxis']['export_url'] = chart_options['export_data']

        if 'y_label' in chart_options.keys():
            data['options']['yaxis'] = [{ 'title': {
                                                'text': chart_options['y_label'],
                                            },
                                    }]
        data['width'] = int(ChartClass.graph_width / quantity) - (ChartClass.graph_space * (quantity - 1) )
        data['height'] = ChartClass.graph_height
        data['series'] = chart_options['series']

        if 'colors' in chart_options.keys():
            data['options']['colors'] = chart_options['colors']
        else:
            data['options']['colors'] = ChartClass.graph_colors

        return data
        
    def generatePieChart(chart_options, quantity=1):
        data = {}
        if 'key' in chart_options.keys():
            data['key'] = chart_options['key']
        data['type'] = 'pie'
        
        data['width'] = int(ChartClass.graph_width / quantity) - (ChartClass.graph_space * (quantity - 1) )
        data['height'] = ChartClass.graph_height
        data['options'] = {'responsive': [{'breakpoint': 480,
                                           'options': {
                                                        'chart': {
                                                                    'width': 200,
                                                                 },
                                                        'legend': {
                                                            'position': 'bottom'
                                                        }
                                                }
                                    }]
                        }
        data['options']['chart'] = {
                                        'width': int(ChartClass.graph_width / quantity) - (ChartClass.graph_space * (quantity - 1) ),
                                        'type': data['type'],
                                        'zoom': {
                                                    'enabled': False
                                            }
                                }
        if 'title' in chart_options.keys():
            data['options']['title'] = {
                                        'text': chart_options['title'],
                                        'align': 'left'
                                    }


        data['series'] = chart_options['series']
        data['options']['labels'] = chart_options['labels']
        
        if 'export_data' in chart_options.keys():
            data['options']['export_url'] = chart_options['export_data']

        if 'colors' in chart_options.keys() and chart_options['colors'] is not None:
            data['options']['colors'] = chart_options['colors']
        else:
            data['options']['colors'] = ChartClass.graph_colors
        return data

    class Meta:
        abstract = True
        app_label = 'charts'
        verbose_name = 'Gráfico'
        verbose_name_plural = 'Gráficos'

# https://apexcharts.com/react-chart-demos/

