from django.apps import AppConfig

class LibraryConfig(AppConfig):
    name = 'backend.library.charts'
