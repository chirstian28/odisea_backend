# -*- coding: utf-8 -*-

import os

from django.http import Http404, HttpResponse
from django.conf import settings
from django.core.paginator import Paginator
import datetime
from django.core import serializers
from xml.dom import minidom
import csv
import logging
from django.core.serializers.json import DjangoJSONEncoder

def createDataList(data_list, page_number, title, verbose_title, page_quantity=20, page_orphans=3):
    element = {}
    results_paginator_data_list = Paginator(data_list, page_quantity, orphans=page_orphans)

    if page_number > results_paginator_data_list.num_pages:
        page_number = results_paginator_data_list.num_pages

    min_page = page_number - page_quantity
    max_page = page_number + page_quantity

    if min_page < 1:
        min_page = 1
    if max_page > results_paginator_data_list.num_pages:
        max_page = results_paginator_data_list.num_pages

    page = results_paginator_data_list.page(page_number)

    total = results_paginator_data_list.count

    if len(data_list) > 0:
        element['data_list'] = page.object_list
        element['has_next_page'] = page.has_next()
        element['has_previous_page'] = page.has_previous()
        element['has_first_page'] = page_number != 1
        element['has_last_page'] = page_number != results_paginator_data_list.num_pages
        element['page_range'] = range(min_page, max_page + 1)
        element['page_number'] = page_number
        element['page_initial_counter'] = (page_number - 1) * page_quantity
        element['total'] = total
        element['quantity'] = len(page.object_list)

    element['verbose_title'] = verbose_title
    element['title'] = title
    return element

def getPaginatorVars(request, count):
    resp = {}
    try:
        page = int(request.POST['page']) # get the requested page 
        limit = int(request.POST['rows']) # get how many rows we want to have into the grid 
        sidx = request.POST['sidx'] # get index row - i.e. user click to sort 
        sord = request.POST['sord'] # get the direction
        search = False
        
        if request.POST.has_key("_search") and request.POST["_search"] == "true":
            search = True
            search_field = request.POST["searchField"]
            search_operation = jgOperationTranslator(request.POST["searchOper"])
            search_string = request.POST["searchString"]
        if not sidx:
            sidx = None
        if count > 0:
            total_pages = int(round(float(count) / float(limit)))
        else:
            total_pages = 1
        if page > total_pages:
            page = total_pages
        if page <= 0:
            page = 1
        start = limit * page - limit
        if start < 0:
            start = 0
        if sord == "desc":
            direction = "-"
        else:
            direction = ""
        resp['page'] = page
        resp['limit'] = limit
        resp['sidx'] = sidx
        resp['sord'] = sord
        resp['search'] = search
        resp['count'] = count
        resp['total_pages'] = total_pages
        resp['start'] = start
        resp['direction'] = direction
        if search:
            resp['search_field'] = search_field 
            resp['search_operation'] = search_operation 
            resp['search_string'] = search_string  
    except (ValueError):
        raise Http404          
    
    return resp

def convertDividedStrToDateTime(date_str, time_str, date_separator="-", time_separator=":"):
    date = date_str.split(date_separator)
    time_str = time_str.replace('T', '')
    time = time_str.split(time_separator)
    if len(time) == 3:
        return datetime.datetime(int(date[0]), int(date[1]), int(date[2]), int(time[0]), int(time[1]), int(time[2]))
    elif len(time) == 2:
        return datetime.datetime(int(date[0]), int(date[1]), int(date[2]), int(time[0]), int(time[1]))
    
def convertStrToDateTime(datetime_str, date_time_separator=" ", date_separator="-", time_separator=":"):
    datetime_splitted = datetime_str.split(date_time_separator)
    date_splitted = datetime_splitted[0].split(date_separator)
    time_splitted = datetime_splitted[1].split(time_separator)
    return datetime.datetime(int(date_splitted[0]), int(date_splitted[1]), int(date_splitted[2]), int(time_splitted[0]), int(time_splitted[1]), int(time_splitted[2]))

def convertStrToDate(date_str, date_separator="-"):
    date_splitted = date_str.split(date_separator)
    return datetime.date(int(date_splitted[0]), int(date_splitted[1]), int(date_splitted[2]))

def convertStrToTime(time_str, time_separator=":"):
    time_str = time_str.replace('T', '') 
    time_splitted = time_str.split(time_separator)
    if len(time_splitted) == 3:
        return datetime.time(int(time_splitted[0]), int(time_splitted[1]), int(time_splitted[2]))
    elif len(time_splitted) == 2:
        return datetime.time(int(time_splitted[0]), int(time_splitted[1]))

def pluralize(word):
    word_length = len(word)
    if word_length > 0:
        if word[word_length - 1] == "s":
            return word + "es"
        else:
            return word + "s"
    else:
        return word
    
def convertListIdToCSV(data_list):
    csv = ""
    for item in data_list:
        if csv == "":
            csv += str(item.id) 
        else:
            csv += "," + str(item.id)
    return csv

def csvToVector(data_str):
    vector = data_str.split(",")
    for i in range(len(vector)):
        vector[i] = vector[i].replace(" ", "")
    return vector

 
xhrOk = 'OK'
xhrErr = 'error'
xhrNew = 'new'
xhrUpdate = 'updated'

def createxml(object_list, fields_csv, prefix=None, with_object_xml=True, status=None):
    fields = []
    for f in fields_csv.split(','):
        fields.append(f.replace(' ', ''))
    if with_object_xml:
        object_str = serializers.serialize('xml', object_list, fields=tuple(fields)).replace('<?xml version="1.0" encoding="utf-8"?>', '')

    if not status:
        status = xhrOk

    # metadata
    meta_doc = minidom.Document()
    root_node = meta_doc.createElement('metadata')
    status_node = meta_doc.createElement('status') 
    status_node.appendChild(meta_doc.createTextNode(status))
    fields_node = meta_doc.createElement('ordered_fields')
    fields_node.appendChild(meta_doc.createTextNode(fields_csv))
    if prefix:
        table_node = meta_doc.createElement('prefix')
        table_node.appendChild(meta_doc.createTextNode(prefix))
    
    root_node.appendChild(status_node)
    root_node.appendChild(fields_node)
    if prefix:
        root_node.appendChild(table_node)
    meta_doc.appendChild(root_node)
    
    # response
    resp = '<?xml version="1.0" encoding="utf-8"?>'
    resp += '<response>'
    resp += meta_doc.toxml(encoding="utf-8").replace('<?xml version="1.0" encoding="utf-8"?>', '')
    if with_object_xml:
        resp += object_str
    resp += '</response>'
    
    return resp

def handle_uploaded_file(f, path, create_dir=False):
    destination, filename = create_file(f.name, path, create_dir=create_dir)
    for chunk in f.chunks():
        destination.write(chunk)
    destination.flush()
    return destination, filename

def create_file(filename, path, create_dir=False):
    if path[-1] != "/":
        path += "/"
    if create_dir and not os.path.isdir(path):
        os.system('mkdir -p ' + path + ' \n')
        os.system('chmod 777 ' + path + ' \n')
    filename = filename.replace(' ', '_')
    file_path = path + filename
    new_file_path = file_path
    while os.path.exists(new_file_path):
        path = new_file_path[0:new_file_path.rfind(".")]
        extension = new_file_path[new_file_path.rfind(".") + 1:]
        new_file_path = path + '_.' + extension
    fp = open(new_file_path, "wb+")
    return fp, new_file_path.split('/')[-1]
    
def encodingConverter(file_path, from_code="ISO-8859-1", to_code="UTF-8"):
    if to_code == "UTF-8":
        string = 'python ' + settings.APP_ROOT + '/library/converter.py ' + file_path
        os.system(string)
    else:
        os.system("iconv --from-code=" + from_code + " --to-code=" + to_code + " " + file_path + " > " + file_path + ".tmp")
        os.system("mv " + file_path + ".tmp " + file_path)
   
def jgOperationTranslator(operation): # Add other operations
    #['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc']
    #['equal','not equal', 'less', 'less or equal','greater','greater or equal', 'begins with','does not begin with','is in','is not in','ends with','does not end with','contains','does not contain'] 
    if operation == "eq":
        op = "="
    elif operation == "ne":
        op = "!="
    elif operation == "lt":
        op = "<"
    elif operation == "le":
        op = "<="
    elif operation == "gt":
        op = ">"
    elif operation == "ge":
        op = ">="
    elif operation == "in":
        op = "IN"
        
    return op

def check_any_permission_required(user, permissions):
    for perm in permissions:
        if user.has_perm(perm):
            return True
    return False

def check_all_permission_required(user, permissions):
    for perm in permissions:
        if not user.has_perm(perm):
            return False
    return True
        
def write_as_csv(data_list, filename, delimiter=',', quotechar='"'):
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(mimetype='text/csv')
    response['Content-Disposition'] = 'attachment; filename=%s' % (filename)
    writer = csv.writer(response, delimiter=delimiter, quotechar=quotechar)
    for row in data_list:
        row_ascii = vector_to_latin1(row)
        writer.writerow(row_ascii)
    return response

def vector_to_latin1(vector):
    vector_ascii = []
    for item in vector:
        if type(item) == unicode:
            new_item = item.encode("iso-8859-1", "replace")
            #new_item = unicodedata.normalize('NFKD', item).encode('LATIN-1', 'ignore') #item.encode("ascii", "replace")
        else:
            new_item = item
        vector_ascii.append(new_item)
    return vector_ascii
        
def list_of_dic_to_vector(listOfDic, key):
    vector = []
    for item in listOfDic:
        vector.append(item[key])
    return vector

def create_logger(log_filename, log_path, level=logging.INFO, data_format='%(asctime)s %(levelname)s %(message)s', create_dir=False):
    if log_path[-1] != "/":
        log_path += "/"
    if not os.path.isdir(log_path):
        os.system('mkdir -p ' + log_path + ' \n')
        os.system('chmod 777 ' + log_path + ' \n')
    logger = logging.getLogger(log_filename)
    hdlr = logging.FileHandler(log_path + log_filename)
    formatter = logging.Formatter(data_format)
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(level)
    return hdlr

def create_canonical_name(msg):
    return "_".join(msg.lower().split()).encode("ascii", "ignore") 

def to_unicode(obj, encoding='utf-8'):
    if isinstance(obj, basestring):
        if not isinstance(obj, unicode):
            obj = unicode(obj, encoding)
    return obj

def message_to_dict(request):
    import simplejson as json
    message = ''
    message_dict = None
    if 'message' in request.GET:
        message = request.GET['message']
    elif 'message' in request.POST:
        message = request.POST['message']
    if message != '':
        message_dict = json.loads(message)
        
    return message_dict

def letterXLS(column_number):
    result = ''
    last_letter_number = ( (column_number - 1) % 26 )
    
    ord_A = ord('A')
    ord_result = ord_A + last_letter_number
    result += chr(ord_result)
    
    return result

def get_message(request):
    message = '{}'
    if request.method == 'GET' and 'message' in request.GET:
        message = request.GET['message']
    elif request.method == 'POST' and 'message' in request.POST:
        message = request.POST['message']

    return message


def FCMsendData(data_str):
    try:
        import pycurl
        header_list = ["Authorization: key=" + settings.FCM_LEGACY_SERVER_KEY, "Content-Type:application/json"]
        c = pycurl.Curl()
        c.setopt(c.URL, settings.FCM_URL)
        c.setopt(pycurl.POST,1)
        c.setopt(pycurl.HEADER,1)
        c.setopt(c.HTTPHEADER, header_list)
        c.setopt(c.POSTFIELDS, data_str)
        c.perform()
        result = c.getinfo(c.RESPONSE_CODE)
        c.close()
        return result
    except:
        return FCMsendCURLData(data_str)

def FCMsendCURLData(data_str):
    import os
    command_str = 'curl --header "Authorization: key='
    command_str += settings.FCM_LEGACY_SERVER_KEY
    command_str += '" --header "Content-Type:application/json" '
    command_str += settings.FCM_URL
    command_str += ' -d "'
    command_str += data_str.replace('"', '\\"')
    command_str += '"'
    print (command_str)
    os.system(command_str)
    return 200

class JsonEncode(DjangoJSONEncoder):

    def default(self, o):
        from django_google_maps.fields import GeoPt
        if isinstance(o, GeoPt):
            data = {
                'lon': o.lon,
                'lat': o.lat
            }
            return data
        return DjangoJSONEncoder.default(self, o)

def JsonDumps(data_dict):
    import json
    data =  json.dumps(data_dict, cls=JsonEncode)
    return data

def JsonLoads(data_str):
    import json
    data_dict =  json.loads(data_str)
    return data_dict

def firebase_decode (input_str):
    result = input_str
    if input_str is not None:
        result = result.replace(u"%24", u'$')
        result = result.replace(u"%2F", u'/')
        result = result.replace(u"%5B", u'[')
        result = result.replace(u"%5D", u']')
        result = result.replace(u"%23", u'#')
        result = result.replace(u"%2E", u'.')
        result = result.replace(u"%25", u'%')
    return result

def firebase_encode (input_str):
    result = input_str
    if input_str is not None:
        result = result.replace(u"%", u'%25')
        result = result.replace(u".", u'%2E')
        result = result.replace(u"#", u'%23')
        result = result.replace(u"$", u'%24')
        result = result.replace(u"/", u'%2F')
        result = result.replace(u"[", u'%5B')
        result = result.replace(u"]", u'%5D')
    return result

def get_rest_url(rest_url):
    import pycurl
    from io import BytesIO
    import json

    buffer = BytesIO()
    c = pycurl.Curl()
    c.setopt(c.URL, rest_url)
    c.setopt(c.WRITEDATA, buffer)
    c.perform()
    c.close()

    body = buffer.getvalue().decode('utf-8')
    try:
        data_dict = json.loads(body)
    except:
        return None
    return data_dict

def send_rest_request(rest_url, data_dict, token=None, method='get'):
    import requests
    import json

    if token is not None:
        headers = {'Authorization': 'Bearer ' + token, "Content-Type": "application/json"}
    else:
        headers = None

    if method == 'get':
        response = requests.get(rest_url, data=json.dumps(data_dict), headers=headers)
    elif method == 'post':
        response = requests.post(rest_url, data=json.dumps(data_dict), headers=headers)
    if method == 'put':
        response = requests.put(rest_url, data=json.dumps(data_dict), headers=headers)
    if method == 'patch':
        response = requests.patch(rest_url, data=json.dumps(data_dict), headers=headers)
    if method == 'delete':
        response = requests.delete(rest_url, data=json.dumps(data_dict), headers=headers)

    return response


    
