# -*- coding: utf-8 -*-
from backend.core.metadata import GenericMetadata

class SetPasswordMetadata(GenericMetadata):

    def determine_metadata(self, request, view):
        meta_data_dict = GenericMetadata.determine_metadata(self, request, view)

        meta_data_dict['buttons']['SAVE']['title'] = 'Asignar Contraseña'
        meta_data_dict['buttons']['SAVE']['method'] = 'PUT'
                
        meta_data_dict['buttons']['EXIT']['url'] = '/accounts/accounttable/'
        meta_data_dict['buttons']['EXIT']['icon'] = 'cancel'  
        meta_data_dict['type'] = 'FORM'
        meta_data_dict['kind'] = 'SETPASSWORD'
        try:
            meta_data_dict['title'] = view.title
        except:
            pass
        return meta_data_dict

class ChangePasswordMetadata(GenericMetadata):

    def determine_metadata(self, request, view):
        meta_data_dict = GenericMetadata.determine_metadata(self, request, view)
        meta_data_dict['buttons']['SAVE']['title'] = 'Cambiar Contraseña'
        meta_data_dict['buttons']['SAVE']['method'] = 'PUT'
        meta_data_dict['buttons']['SAVE']['kind'] = 'UPDATE'
        meta_data_dict['buttons']['EXIT']['title'] = 'Cancelar'
        meta_data_dict['kind'] = 'FORM'

        try:
            meta_data_dict['title'] = view.title
        except:
            pass
        return meta_data_dict
