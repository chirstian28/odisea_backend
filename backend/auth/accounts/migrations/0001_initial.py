# Generated by Django 2.2.15 on 2020-08-05 09:18

from django.conf import settings
import django.contrib.auth.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0011_update_proxy_permissions'),
        ('license', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('user_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('identification', models.CharField(db_index=True, max_length=22, verbose_name='Identificación')),
                ('complete_name', models.CharField(help_text='large', max_length=200, verbose_name='Nombre Completo')),
                ('cellphone_number', models.CharField(blank=True, max_length=15, null=True, verbose_name='Celular')),
                ('licenser', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='license.Licenser', verbose_name='Propietario')),
            ],
            options={
                'verbose_name': 'Cuenta',
                'verbose_name_plural': 'Cuentas',
            },
            bases=('auth.user',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='AccountType',
            fields=[
                ('id', models.CharField(max_length=5, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=60)),
                ('disable_components', models.TextField(blank=True, null=True)),
            ],
            options={
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='AccountToken',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('token', models.TextField(db_index=True, verbose_name='Token:')),
                ('registerid', models.TextField(db_index=True, verbose_name='Register id:')),
                ('active', models.BooleanField(default=True)),
                ('account', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.Account')),
                ('licenser', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='license.Licenser')),
            ],
            options={
                'verbose_name': 'Token Cuenta',
                'verbose_name_plural': 'Tokens Cuentas',
            },
        ),
        migrations.AddField(
            model_name='account',
            name='type',
            field=models.ForeignKey(blank=True, default='A', null=True, on_delete=django.db.models.deletion.SET_NULL, to='accounts.AccountType', verbose_name='Tipo Cuenta'),
        ),
    ]
