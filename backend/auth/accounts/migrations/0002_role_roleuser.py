# Generated by Django 2.2.15 on 2020-11-12 16:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('license', '0001_initial'),
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Role',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=60)),
            ],
            options={
                'verbose_name': 'Registro de Rol',
            },
        ),
        migrations.CreateModel(
            name='RoleUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('account', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='accounts.Account', verbose_name='Cuenta')),
                ('licenser', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='license.Licenser', verbose_name='Propietario')),
                ('role', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='accounts.Role', verbose_name='Rol')),
            ],
            options={
                'verbose_name': 'Asignar Rol a cuenta',
            },
        ),
    ]
