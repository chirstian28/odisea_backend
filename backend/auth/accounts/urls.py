from backend.auth.accounts import views
from rest_framework import routers
from django.urls import path, include

router = routers.DefaultRouter()

router.register(r'account', views.AccountViewSet, basename='account')
router.register(r'accounttable', views.AccountTableViewSet, basename='accounttable')
router.register(r'accountquery', views.AccountQueryViewSet, basename='accountquery')

urlpatterns = [
            path('setpassword/', views.SetPasswordView.as_view()),
            path('setpassword/<int:account_id>/', views.SetPasswordView.as_view()),

            path('changepassword/', views.ChangePasswordView.as_view()),
            path('changecondition/', views.ChangeConditionView.as_view()),
            
            path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
            path('profile/', views.ProfileView.as_view()),
            path('login/', views.LoginView.as_view()),
            path('tokenrefresh/', views.RefreshTokenView.as_view()),
            path('tokenverify/', views.VerifyTokenView.as_view()),
            path('logout/', views.LogoutView.as_view()),

            path('accounttypefilter/', views.AccountTypeFilterView.as_view()),
]

urlpatterns += router.urls

