import django_filters

from backend.auth.accounts.models import Account

class AccountTableFilter(django_filters.rest_framework.FilterSet):

    class Meta:
        model = Account
        fields = ['username','type', "groups", "is_active"]
