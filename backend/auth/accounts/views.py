from backend.auth.license.models import LicenseLimit
from backend.components.auditor.models import AuditorParameter
from backend.auth.accounts.models import Account, AccountToken, AccountType
from backend.auth.accounts.serializers import AccountTableSerializer, \
                                         AccountSerializer, LoginSerializer, \
                                         VerifyTokenSerializer, \
                                         RefreshTokenSerializer,  \
                                         ChangePasswordSerializer, \
                                         ChangeConditionSerializer, \
                                         SetPasswordSerializer, \
                                         AccountQuerySerializer, \
                                         CustomJWTSerializer
from backend.auth.accounts.metadata import ChangePasswordMetadata, \
                                           SetPasswordMetadata
from backend.core.permissions import IsAdminAndReadOnly
from backend.core.views import GenericModelViewSet, GenericTableViewSet, \
    GenericQueryViewSet
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib import auth
from rest_framework_jwt.views import ObtainJSONWebToken, VerifyJSONWebToken, \
                                     RefreshJSONWebToken
from django.contrib.auth.models import User
from rest_framework.permissions import IsAuthenticated
from django.conf import settings

class LoginView(ObtainJSONWebToken):
    serializer_class = LoginSerializer

    def post(self, request, *args, **kwargs):
        licenserid = request.data['licenserid']
        username = request.data['username']
        error = None

        if error is None:
            license_validation = LicenseLimit.checkLicense(check_type='T')
            if not license_validation['have_license']:
                error = 'Error de validación de licencia: '
                error += f'Límite de tiempo límite de uso ({license_validation["license_limit"].strftime("%Y-%m-%d")})'

        if error is None:
            response = CustomJWTSerializer.post(self, request.data, request, *args, **kwargs)
            if response.status_code == status.HTTP_200_OK and 'username' in request.data.keys():
                try:
                    user = User.objects.get(username=username, is_active=True)
                    request.user = user
                    account = Account.getAccount(user)
                    if account is not None:
                        licenser = account.licenser
                        if account.licenser.id != licenserid:
                            error = 'Error de Validación de Usuario'
                            user = None
                            request.user = None
                except User.DoesNotExist:
                    user = None
                    request.user = None
                    error = 'Error de Validación de Usuario'
                    
                registerid = None
                if 'registerid' in request.data.keys():
                    registerid = request.data['registerid']
            else:
                try:
                    error = response.data['ERROR']
                except:
                    error = 'Error de Validación de Usuario'

        if error is None:
            try:
                account_token = AccountToken.objects.get(licenser=licenser, account=account, active=True)
            except AccountToken.DoesNotExist:
                account_token = AccountToken()
                account_token.licenser = licenser
                account_token.account = account
                account_token.creation_username = request.data['username']
            account_token.token = response.data['token']
            account_token.registerid = registerid
            account_token.update_username = request.data['username']
            account_token.save()
            response.data['licenser'] = account.licenser.id
            response.data['message'] = 'Sesión iniciada correctamente'
            response.data['level'] = settings.MESSAGE_SUCCESS
            AuditorParameter.registerEvent(request, 'accounts', 'AccountToken', None, request.data['username'],
                                           AuditorParameter.LOGIN,
                                           action='LOGIN_ACCOUNT')
        else:
            return Response(data={'message': error, 'level': settings.MESSAGE_ERROR}, status=status.HTTP_400_BAD_REQUEST)
        return response

class LogoutView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        account = Account.getAccount(user=request.user)
        data = {}
        if account is not None:
            account.Logout()
            auth.logout(request)
            AccountToken.objects.filter(account=account, active=True).update(active=False)
            AuditorParameter.registerEvent(request, 'accounts', 'AccountToken', None, account.id,
                                           AuditorParameter.LOGOUT,
                                           action='LOGOUT_ACCOUNT')
            
        data['message'] = 'Sesión terminada correctamente'
        data['level'] = settings.MESSAGE_SUCCESS
        
        return Response(data)

class AccountTableViewSet(GenericTableViewSet):
    app_name = 'accounts'
    model_name = 'Account'
    queryset = Account.objects.filter(is_active=True)
    serializer_class = AccountTableSerializer
    permission_classes = (IsAdminAndReadOnly,IsAuthenticated,)
    filter_licenser = True

    def get_queryset(self):
        queryset = GenericTableViewSet.get_queryset(self)
        
        context = self.get_serializer_context()
        request = context['request']
        actual_account = Account.getAccount(request.user)
        AuditorParameter.registerEvent(self.request, 'accounts', 'Account', None, None, AuditorParameter.QUERY, action='QUERY_ACCOUNT' )
        
        if actual_account is not None:
            queryset = queryset.filter(type__hierarchy__gte=actual_account.type.hierarchy)
        else:
            queryset = queryset.none()
        return queryset

class AccountQueryViewSet(GenericQueryViewSet):
    app_name = 'accounts'
    model_name = 'Account'
    queryset = Account.objects.filter(is_active=True)
    serializer_class = AccountQuerySerializer
    permission_classes = (IsAdminAndReadOnly,)
    filter_licenser = True

class AccountViewSet(GenericModelViewSet):
    app_name = 'accounts'
    model_name = 'Account'
    queryset = Account.objects.filter(is_active=True)
    serializer_class = AccountSerializer
    permission_classes = [IsAuthenticated]
    filter_licenser = True
    
    def perform_destroy(self, instance):
        old_serializer_data = self.serializer_class(instance).data
        try:
            AuditorParameter.registerEvent(self.request, self.app_name, self.model_name, None, instance.id, AuditorParameter.DELETE, before_register_object=old_serializer_data, action='DELETE_' + self.model_name.upper() )
        except:
            pass
        instance.is_active = False
        instance.save()
    
class SetPasswordView(APIView):
    serializer_class = SetPasswordSerializer
    metadata_class = SetPasswordMetadata

    def get(self, request, account_id=None):
        return Response(status=status.HTTP_202_ACCEPTED)
        
    def put(self, request, account_id=None):
        account = None
        new_password = None
        confirmpassword = None
        error = None
        actual_account = Account.getAccount(request.user)
        
        if account_id is not None:
            try:
                account = Account.objects.get(licenser=actual_account.licenser, pk=account_id)
            except Account.DoesNotExist:
                pass
        else:
            error = "No se envió el ID de la Cuenta"
            
        if not account.is_active:
            error = "La cuenta no está activa"
            
        if error is None:
            if 'password' in request.data.keys() and request.data['password'] != '':
                new_password = request.data['password']
                
            if 'confirmpassword' in request.data.keys() and request.data['confirmpassword'] != '':
                confirmpassword = request.data['confirmpassword']
            
            if new_password != confirmpassword:
                error = "La Nueva contraseña debe ser igual a la confirmación"
        
        if error is None:
            account.set_password(new_password)
            account.save()

            AuditorParameter.registerEvent(request, 'accounts', 'Account', None, account.id,
                                           AuditorParameter.UPDATE, action='SET_PASSWORD')
            return Response(status=status.HTTP_202_ACCEPTED)
        return Response(data={'ERROR': error}, status=status.HTTP_400_BAD_REQUEST)

class ChangePasswordView(APIView):
    serializer_class = ChangePasswordSerializer
    metadata_class = ChangePasswordMetadata
    title = 'Cambiar Contraseña'
    permission_classes = (IsAuthenticated,)

    def put(self, request, format=None):
        current_password = None
        new_password = None
        error = ''
        
        if 'currentpassword' in request.data.keys() and request.data['currentpassword'] != '':
            current_password = request.data['currentpassword']
        if 'password' in request.data.keys() and request.data['password'] != '':
            new_password = request.data['password']
        if 'confirmpassword' in request.data.keys() and request.data['confirmpassword'] != '':
            confirmpassword = request.data['confirmpassword']

        if current_password is not None and new_password is not None and confirmpassword is not None:
            if new_password != confirmpassword:
                error = 'La nueva contraseña no coincide con la confirmación'
            elif new_password == current_password:
                error = 'La nueva contraseña es igual que la anterior'
            else:
                auth_user = auth.authenticate(username=request.user.username,
                                              password=current_password)

                if auth_user is not None:
                    if auth_user.is_active:
                        auth_user.set_password(new_password)
                        auth_user.save()

                        auth.logout(request)
                        
                        AuditorParameter.registerEvent(request, 'accounts', 'Account', None, auth_user.id,
                                                       AuditorParameter.UPDATE, action='CHANGE_PASSWORD')
                        data = {"SUCCESS": 'La contraseña fue actualizada exitosamente, se recomienda cerrar la sesión y volver a iniciarla con la nueva contraseña.'}
                        return Response(data=data, status=status.HTTP_202_ACCEPTED)
                    else:
                        error = 'Cuenta Inactiva'
                else:
                    error = 'La contraseña actual es incorrecta.'
        return Response(data={'ERROR': error}, status=status.HTTP_400_BAD_REQUEST)

class ChangeConditionView(APIView):
    serializer_class = ChangeConditionSerializer
    
    def put(self, request, format=None):
        account = None
        isactive = False
        error = ''
        actual_account = Account.getAccount(request.user)

        if 'accountid' in request.data.keys() and request.data['accountid'] != '':
            accountid = request.data['accountid']
            try:
                account = Account.objects.get(licenser=actual_account.licenser, pk=accountid)
            except Account.DoesNotExist:
                pass
        if 'isactive' in request.data.keys() and request.data['isactive'].lower() == 'true':
            isactive = True

        if account is not None:
            result = account.updateCondition(isactive)
            if result:
                AuditorParameter.registerEvent(request, 'accounts', 'Account', None, account.id, AuditorParameter.UPDATE, action='CHANGE_CONDITION' )
                return Response(status=status.HTTP_202_ACCEPTED)
            else:
                error = 'Cuenta no existe.'
        return Response(data={'ERROR': error}, status=status.HTTP_400_BAD_REQUEST)

class ProfileView(APIView):
    
    def get(self, request, format=None):
        account = Account.getAccount(request.user)
        if account is not None:
            data = account.getProfile()
            return Response(data)
        error = 'Account does not exist'
        return Response(data={'ERROR': error}, status=status.HTTP_400_BAD_REQUEST)


class VerifyTokenView(VerifyJSONWebToken):

    serializer_class = VerifyTokenSerializer
    #permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        response = VerifyJSONWebToken.post(self, request, *args, **kwargs)
        error = None
        if response.status_code == status.HTTP_200_OK:
            try:
                account_token = AccountToken.objects.get(token=request.data['token'],active=True)
            except AccountToken.DoesNotExist:
                error = 'No existe Cuenta registrada con el token'
            if error is None:
                account = account_token.account
                request.user = account.getUser()
                AuditorParameter.registerEvent(request, 'accounts', 'AccountToken', None, account.id, AuditorParameter.QUERY,
                                               action='VERIFY_TOKEN')
            if error is not None:
                response_dict = {'level': settings.MESSAGE_ERROR, 'message': error}
                return Response(data=response_dict, status=status.HTTP_400_BAD_REQUEST)
        return response

class RefreshTokenView(RefreshJSONWebToken):
    serializer_class = RefreshTokenSerializer

    def post(self, request, *args, **kwargs):
        response = RefreshJSONWebToken.post(self, request, *args, **kwargs)
        error = None
        if response.status_code == status.HTTP_200_OK:

            try:
                account_token = AccountToken.objects.get(token=request.data['token'],active=True)
            except AccountToken.DoesNotExist:
                error = 'No existe Cuenta registrada con el token'
            if error is None:
                account = account_token.account
                AuditorParameter.registerEvent(request, 'accounts', 'AccountToken', None, account_token.id, AuditorParameter.UPDATE,
                                               action='REFRESH_TOKEN')
            if error is None:
                account_token.token = response.data['token']
                account_token.update_username = account.username
                account_token.save()

            if error is not None:
                response_dict = {'level': settings.MESSAGE_ERROR, 'message': error}
                return Response(data=response_dict, status=status.HTTP_400_BAD_REQUEST)

        return response

class AccountTypeFilterView(APIView):

    def get(self, request):
        data = []
        account_type_list = AccountType.objects.all()
        for account_type in account_type_list:
            data.append({ 'key': account_type.id, "text": account_type.name })

        return Response(data=data)
