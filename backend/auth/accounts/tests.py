from django.test import TestCase
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User
from rest_framework.test import APIRequestFactory
from rest_framework.authtoken.views import ObtainAuthToken
import json
from backend.auth.accounts.views import ChangePasswordView
from backend.auth.accounts.serializers import AccountSerializer

# Create your tests here.
def testAddAccount():
    data_dict = {
            "licenser": "ertech",
            "user": {
                "username": "ertech7",
                "first_name": "Administrador",
                "last_name": "ERTECH3",
                "email": "info@er-technology.net",
                "is_staff": True,
                "is_active": True,
                "is_superuser": True,
                "account": 1
            },
            "identification": "900.162.974-8",
            "complete_name": "ER Technology LTDA 7",
            "type": "A",
            "condition": "I",
            "cellphone_number": "",
            "extension": 8890,
            "voicemail": False,
            "secret": 8890,
            "nat": False,
            "additional_info": "",
            "timer": 0,
            "call_record": True,
            "other_can_spy": False,
            "locked": False,
            "time_to_locked": 0,
            "simultaneous_calls": 0,
            "findme": "3731733",
            "lock_internal_calls": False,
            "lock_inbound_calls": False,
            "lock_other_groups_call": False,
            "last_unlocked": None,
            "max_unlock": 0,
            "last_sip_login": None,
            "last_sip_logout": None,
        }
    
    serializer = AccountSerializer(data=data_dict)
    if serializer.is_valid():
        account = serializer.save()

def test_login():
    data_dict = {
        'username': 'ertech',
        'password': 'ERTECH',
    }

    factory = APIRequestFactory()
    view = ObtainAuthToken.as_view()
    request = factory.post('/api-token-auth/', json.dumps(data_dict), content_type='application/json')
    response = view(request) 
    print (response.status_code)
    print ( response.data )
    
def test_changepassword():
    user = User.objects.get(username = 'ertech')
    
    data_dict = {
        'current_password': 'ERTECH2',
        'password': 'ERTECH',
    }

    factory = APIRequestFactory()
    view = ChangePasswordView.as_view()
    request = factory.put('/accouns/changepassword/', json.dumps(data_dict), content_type='application/json')
    response = view(request) 
    print (response.status_code)
    print ( response.data )
            
        
            
