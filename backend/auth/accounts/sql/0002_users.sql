INSERT INTO auth_group ( name ) VALUES ( 'ADMIN' );
INSERT INTO auth_group ( name ) VALUES ( 'EMPLEADOS' );

INSERT INTO auth_user (username, first_name, last_name, email, password, is_staff, is_active, is_superuser, last_login, date_joined) VALUES ('ertech', 'Administrador Backend', 'ERTECH', 'soporte@er-technology.net', 'pbkdf2_sha256$20000$eRHlmdSTnOVk$+4wE3evpQD3W+Eo2Xp+blmdwtoPDC0BjT0H/EcWIwDY=', TRUE, TRUE, TRUE, current_timestamp, current_timestamp);
INSERT INTO auth_user (username, first_name, last_name, email, password, is_staff, is_active, is_superuser, last_login, date_joined) VALUES ('admin', 'Administrador', 'ERTECH', 'info@er-technology.net', 'pbkdf2_sha256$30000$CpQKqUQerdAa$SWI83cAE5/YYuZIZ7VZUxHUiVcmrkWd921MzybvAsOk=', TRUE, TRUE, TRUE, current_timestamp, current_timestamp);
INSERT INTO auth_user (username, first_name, last_name, email, password, is_staff, is_active, is_superuser, last_login, date_joined) VALUES ('soporte', 'Soporte', 'ERTECH', 'soporte@er-technology.net', 'pbkdf2_sha256$30000$JHyxlnTgCJEi$4IzjqPqLgVggejcDXCe8kxrPxy6DCSU7RW9RNs/L8MU=', FALSE, TRUE, FALSE, current_timestamp, current_timestamp);

INSERT INTO auth_user_groups (user_id, group_id) VALUES ( (SELECT id FROM auth_user WHERE username = 'ertech'), (SELECT id FROM auth_group WHERE name = 'ADMIN') );
INSERT INTO auth_user_groups (user_id, group_id) VALUES ( (SELECT id FROM auth_user WHERE username = 'admin'), (SELECT id FROM auth_group WHERE name = 'ADMIN') );
INSERT INTO auth_user_groups (user_id, group_id) VALUES ( (SELECT id FROM auth_user WHERE username = 'soporte'), (SELECT id FROM auth_group WHERE name = 'EMPLEADOS') );
