# -*- coding: utf-8 -*-
from backend.auth.license.models import LicenseLimit
from rest_framework import serializers
from backend.auth.accounts.models import Account
from backend.components.auditor.models import AuditorParameter
from rest_framework_jwt.serializers import JSONWebTokenSerializer, VerifyJSONWebTokenSerializer, \
    RefreshJSONWebTokenSerializer
from backend.core.models import ResourceHost
from backend.core.generic_serializer import GenericQuerySerializer
import datetime 

class LoginSerializer(JSONWebTokenSerializer):
    licenserid = serializers.CharField(required=True)
    registerid = serializers.CharField(required=False)

class CustomJWTSerializer(JSONWebTokenSerializer):
    
    def post(self, request_data, request, *args, **kwargs):
        from rest_framework_jwt.views import jwt_response_payload_handler
        from rest_framework.response import Response
        from rest_framework_jwt.settings import api_settings
        from rest_framework import status

        serializer = self.get_serializer(data=request_data)

        if serializer.is_valid():
            user = serializer.object.get('user') or request.user
            token = serializer.object.get('token')
            response_data = jwt_response_payload_handler(token, user, request)
            response = Response(response_data)
            if api_settings.JWT_AUTH_COOKIE:
                expiration = (datetime.datetime.utcnow() +
                              api_settings.JWT_EXPIRATION_DELTA)
                response.set_cookie(api_settings.JWT_AUTH_COOKIE,
                                    token,
                                    expires=expiration,
                                    httponly=True)
            return response
        try:
            error_msg = ', '.join([f'{k} {v[0]}' for k,v in serializer.errors.items()])
        except:
            error_msg = 'Hay errores en la solicitud'
        
        return Response({'ERROR': error_msg}, status=status.HTTP_400_BAD_REQUEST)
    
class AccountSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(label='Nombres', help_text='large')
    last_name = serializers.CharField(label='Apellidos', help_text='large')
    email = serializers.EmailField(label='Email', help_text='large')
    username = serializers.CharField(label='Usuario', help_text='noupdate')
    
    def validate(self, attrs):
        license_validation = LicenseLimit.checkLicense()
        if not license_validation['have_license']:
            error = 'Error de validación de licencia: '
            if license_validation['license_type'] == 'U':
                error += f'Límite de usuarios ({license_validation["license_limit"]})'
            elif license_validation['license_type'] == 'T':
                error += f'Límite de tiempo límite de uso ({license_validation["license_limit"].strftime("%Y-%m-%d")})'
            raise serializers.ValidationError({
                        'username': [error]
                            })

        return super().validate(attrs)

    def create(self, validated_data):
        request = self.context['request']
        actual_account = Account.getAccount(request.user)

        validated_data['is_superuser'] = False
        validated_data['is_staff'] = False
        validated_data['is_active'] = True
        account, account_created = Account.objects.get_or_create(licenser=actual_account.licenser, **validated_data)
        AuditorParameter.registerEvent(request, 'accounts', 'Account', validated_data, account.id, AuditorParameter.CREATE,
                                       action='CREATE_ACCOUNT')

        return account

    def update(self, instance, validated_data):
        request = self.context['request']
        old_serializer = AccountSerializer(instance)
        serializer = super(AccountSerializer, self).update(instance, validated_data)
        instance.update_username = request.user.username
        instance.save()
        instance_data = AccountSerializer(instance)
        AuditorParameter.registerEvent(request, 'accounts', 'Account', instance_data.data, instance.id, AuditorParameter.UPDATE, before_register_object=old_serializer.data, action='UPDATE_ACCOUNT' )
        return serializer
    
    class Meta:
        model = Account
        fields = ("id", 'username', "identification", 'first_name', 'last_name', 'email', "phone_number", "type",  )
        description = 'Cuenta de acceso a la plataforma.'

class AccountTableSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(help_text='small')
    username = serializers.CharField(label='Usuario')
    complete_name = serializers.CharField(label='Nombre Completo',help_text='large')
    email = serializers.CharField(label='Email',help_text='large')
    type = serializers.CharField(label='Tipo Cuenta')

    class Meta:
        model = Account
        verbose_name = 'Cuentas'
        fields = ("id", 'username', "complete_name", "email", "type", )
        filter_fields = ("type",)
        search_fields = ['$first_name', '$last_name', '^username']
        ordering_fields = ("id", 'username',)
        basic_actions = {'SETPASSWORD': {
                                    'order': 5,
                                    'title': 'Asignar Contraseña',
                                    'kind': 'SETPASSWORD',
                                    'url': '/accounts/setpassword/',
                                    'disable': False,
                                    'icon': 'PasswordField',
                                    'backurl': '/accounts/accounttable/',
                                    'method': 'GET',
                                }
            }
        description = "Cuentas de acceso a la plataforma.  Su impacto es global, no se requiere tener una extensión el en sistema para tener acceso a la plataforma."
        
class AccountQuerySerializer(GenericQuerySerializer):

    class Meta:
        model = Account
        verbose_name = 'Cuentas'
        fields = ('id', 'name',)
        filter_fields = ("type",)

class SetPasswordSerializer(serializers.Serializer):
    password = serializers.CharField(label='Nueva Contraseña', help_text='password',required=True)
    confirmpassword = serializers.CharField(label='Confirmar Contraseña',help_text='password',required=True)

    class Meta:
        verbose_name = 'Asignar Contraseña'
        fields = ('password', 'confirmpassword', )

class ChangePasswordSerializer(serializers.Serializer):
    currentpassword = serializers.CharField(label='Contraseña Actual',help_text='password')
    password = serializers.CharField(label='Nueva Contraseña',help_text='password')
    confirmpassword = serializers.CharField(label='Confirmar Contraseña',help_text='password')

    class Meta:
        verbose_name = 'Cambiar Contraseña'
        fields = ('currentpassword', 'password', 'confirmpassword', )

class ChangeConditionSerializer(serializers.Serializer):
    accountid = serializers.CharField()
    isactive = serializers.BooleanField()

class VerifyTokenSerializer(VerifyJSONWebTokenSerializer):
    pass

class RefreshTokenSerializer(RefreshJSONWebTokenSerializer):
    pass
