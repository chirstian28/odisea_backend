from django.contrib.auth.models import User
from django.conf import settings
from django.db import models
from django.db.models import indexes

class AccountType(models.Model):
    id = models.CharField(max_length=5, primary_key=True)
    name = models.CharField(max_length=60)
    hierarchy = models.IntegerField(verbose_name='Jerarquía de tipos de cuentas', default= 0, help_text='help:La jerarquía más alta es 0')
    disable_components = models.TextField(null=True, blank=True)

    def __str__(self):
        return "%s" % (self.name)

    class Meta:
        ordering = ['name']
        app_label = 'accounts'
        verbose_name = 'Tipo de Cuenta'
        verbose_name_plural = 'Tipos de Cuentas'

class UserDate(models.Model):
    creation_username = models.CharField(verbose_name='Usuario creación', max_length=100, null=True, blank=True)
    creation_date_time = models.DateTimeField(verbose_name='Fecha y hora creación', auto_now_add=True, null=True, blank=True)
    update_username = models.CharField(verbose_name='Usuario Actualización', max_length=100, null=True, blank=True)
    update_date_time = models.DateTimeField(verbose_name='Fecha Actualización', auto_now=True, null=True, blank=True)
     
    class Meta:
        abstract = True
          
class Account(User, UserDate):
    licenser = models.ForeignKey('license.Licenser', verbose_name="Cliente", on_delete=models.PROTECT)
    identification = models.CharField(max_length=22, verbose_name="Identificación", db_index=True)
    type = models.ForeignKey("AccountType", default= '', verbose_name="Tipo Cuenta", on_delete=models.PROTECT)
    phone_number = models.CharField(max_length=15, blank=True, null=True, verbose_name="Telefono")

    def valideComponent(self, component):
        if self.type is not None and self.type.disable_components is not None:
            if component in self.type.disable_components.split(','):
                return False
        return True 

    @property
    def complete_name(self):
        result = self.first_name
        if self.last_name:
            result += ' ' + self.last_name
        return result

    def getAccount(user):
        try:
            account = Account.objects.get(pk=user.id)
        except:
            account = None
        return account
    
    getAccount = staticmethod(getAccount)
    
    def getProfile(self):
        data = {}
        data['username'] = self.username
        data['complete_name'] = self.complete_name
        data['account_type'] = self.type.name
        return data

    def __str__(self):
        return "%s" % (self.username)

    def Logout(self):
        from backend.core.models import TemporalData
        TemporalData.objects.filter(account=self).delete()
        AccountToken.objects.filter(account=self).delete()

    class Meta:
        ordering = ['licenser', 'username']
        app_label = 'accounts'
        verbose_name = 'Cuenta'
        verbose_name_plural = 'Cuentas'
        indexes = [
            models.Index(fields=['licenser', 'type'], name='account_type'),
        ]


class AccountToken(models.Model):
    licenser = models.ForeignKey('license.Licenser', on_delete=models.PROTECT)
    account = models.ForeignKey('Account', on_delete=models.CASCADE)
    token =  models.TextField(verbose_name="Token:", db_index=True)
    registerid = models.TextField(verbose_name="Register id:", db_index=True, null=True, blank=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return "%s" % (self.token)

    def getAccount(token):
        try:
            account_token = AccountToken.objects.get(token=token)
            account = account_token.account
        except AccountToken.DoesNotExist:
            account = None

        return account

    getAccount = staticmethod(getAccount)

    def Validate(token):
        validate_response = '1'
        error_code = ''
        try:
            AccountToken.objects.get(token=token, active=True)
        except AccountToken.DoesNotExist:
            validate_response = '0'
            error_code = 'AM00014'

        return validate_response, error_code

    Validate = staticmethod(Validate)

    class Meta:
        app_label = 'accounts'
        verbose_name = 'Token Cuenta'
        verbose_name_plural = 'Tokens Cuentas'
