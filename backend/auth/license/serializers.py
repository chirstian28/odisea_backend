from rest_framework import serializers
from backend.auth.license.models import LicenseLimit, Licenser

class LicenseLimitSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = LicenseLimit
        fields = ("id", "environment", "by_user_quantity", "by_date")

class LicenserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Licenser
        fields = ("id", "name", "email", "address", "phone")
