# -*- coding: utf-8 -*-
from django.db import models
import os
from django.conf import settings
from backend.library.sendmail import send_mail
from backend.core.models import ResourceHost, Environment

class LicenseLimit(models.Model):
    environment = models.CharField(verbose_name='Entorno', max_length=100)
    by_user_quantity = models.IntegerField(verbose_name='Límite por Cantidad', null=True,blank=True)
    by_date = models.DateField(verbose_name='Límite por fecha', null=True,blank=True)
    
    def checkLicense(user_quantity=None, check_type=None):
        from backend.auth.accounts.models import Account
        import datetime
        result = {}
        result['have_license'] = True
        result['license_type'] = ''
        result['license_limit'] = None
        
        try:
            license_limit = LicenseLimit.objects.get(environment=Environment.getEnvironmentName())
        except LicenseLimit.DoesNotExist:
            return result
        
        if check_type is None or check_type == 'T':
            today = datetime.date.today()
            if license_limit.by_date and today > license_limit.by_date + datetime.timedelta(days=30):
                result['have_license'] = False
                result['license_type'] = 'T'
                result['license_limit'] = license_limit.by_date
                return result
        
        if check_type is None or check_type == 'U':
            if user_quantity is None:
                user_quantity = Account.objects.filter(is_active=True).count()
            if license_limit.by_user_quantity and license_limit.by_user_quantity + 2 < user_quantity:
                result['have_license'] = False
                result['license_type'] = 'U'
                result['license_limit'] = license_limit.by_user_quantity
                return result
        
        return result

    checkLicense = staticmethod(checkLicense)

    def __str__(self):
        return str(self.licenser)
        
    class Meta:
        app_label = 'license'
        verbose_name = 'License Limit'
        verbose_name_plural = 'Licenses Limit'

def upload_logo_to(instance, filename):
    from django.utils.timezone import now
    filename_tuple = os.path.splitext(filename)
    return 'license/%d_%s%s' % (
        instance.id,
        now().strftime("%Y%m%d%H%M%S"),
        filename_tuple[1].lower(),
    )

class Licenser(models.Model):
    id = models.CharField(max_length=10, primary_key=True)
    name = models.CharField(max_length=60, verbose_name='Nombre', unique=True)
    logo = models.ImageField(null=True, upload_to=upload_logo_to, blank=True, verbose_name='Logo')
    email = models.CharField(max_length=60, null=True, blank=True, verbose_name='Email')
    admin_email = models.EmailField(max_length=60, null=True, blank=True, verbose_name='Email Administrador')
    phone = models.CharField(max_length=60, null=True, blank=True, verbose_name='Teléfono')
    address = models.TextField(null=True,blank=True, verbose_name='Dirección')
    city = models.ForeignKey('localization.City',null=True,blank=True,verbose_name='Ciudad', on_delete=models.SET_NULL)

    def get_logo_url(self):
        from django.core.files.storage import default_storage as storage
        if not self.logo.name:
            return ""
        if storage.exists(self.logo.name):
            base_url = ''
            if settings.STORAGE_TYPE != "S3":
                base_url = ResourceHost.getURL('BASE_URL')
            return base_url + storage.url(self.logo.name)
        return ""

    def getInfo(self):
        from backend.auth.license.serializers import LicenserSerializer
        data =  LicenserSerializer(self).data
        logo_url = self.get_logo_url()
        if logo_url:
            data['logo'] = self.get_logo_url()
        else:
            data['logo'] = ''
            if settings.PLATFORM != 'AWS':
                data['logo'] += ResourceHost.getURL('BASE_URL')
            data['logo'] += settings.MEDIA_URL + 'logo.png'
        
        return data

    def sendLogEmail(self, filename, process, date):
        email_dict = {}
        try:
            email_dict['REPLY_TO'] = settings.ADMINS[0]
        except:
            pass
        email_dict['TO_EMAIL'] = []
        for admin in settings.ADMINS:
            email_dict['TO_EMAIL'].append(admin[0] + '<' + admin[1] + '>')
        if self.admin_email is not None:
            email_dict['TO_EMAIL'].append(self.id + '<' + self.admin_email + '>')
    
        email_dict['SUBJECT'] = settings.COMERCIAL_APP_NAME.replace(' ', '_') 
        email_dict['SUBJECT'] += " ENV: " + Environment.getEnvironmentName()
        email_dict['SUBJECT'] += " " + process + " " + self.id
        
        email_dict['VERBOSE_TITLE'] = 'Reporte de Proceso %s del %s Entorno %s' % (process, date.strftime('%Y-%m-%d'), Environment.getEnvironmentName()) 
        
        email_dict['DATA'] = {}
        mailfile = open(filename, "r")
        email_dict['DATA']['BODY'] = str(date) + '<br />'
        
        lines = mailfile.readlines()
        for line in lines:
            email_dict['DATA']['BODY'] += line
            email_dict['DATA']['BODY'] += '<br />'
        mailfile.close()
        
        email_sent = send_mail(email_dict)
        if not email_sent:
            print  ('No se pudo enviar el correo')

    def __str__(self):
        return u"%s" % (self.name)

    class Meta:
        ordering = ['name']
        app_label = 'license'
        verbose_name = 'Licenciador'
