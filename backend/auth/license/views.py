from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from backend.auth.license.models import LicenseLimit, Licenser
from backend.auth.license.serializers import LicenseLimitSerializer, \
                                             LicenserSerializer
from backend.core.views import GenericModelViewSet

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

@csrf_exempt
def check_license(request, user_quantity=None, check_type=None, format=None):
    if request.method == 'GET':
        data_result = LicenseLimit.checkLicense(user_quantity, check_type)
        return JSONResponse(data_result)

class LicenseLimitViewSet(GenericModelViewSet):
    app_name = 'license'
    model_name = 'LicenseLimit'
    queryset = LicenseLimit.objects.all()
    serializer_class = LicenseLimitSerializer
    filter_licenser = False

class LicenseViewSet(GenericModelViewSet):
    app_name = 'license'
    model_name = 'Licenser'
    queryset = Licenser.objects.all()
    serializer_class = LicenserSerializer
    filter_licenser = False

