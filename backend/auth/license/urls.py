from backend.auth.license import views
from django.urls import path
from rest_framework import routers

router = routers.DefaultRouter()

router.register(r'license', views.LicenseViewSet)
router.register(r'licenselimit', views.LicenseLimitViewSet)

urlpatterns = [
    path('check/', views.check_license),
    path('check/<int:user_quantity>/', views.check_license),
    path('check/<int:user_quantity>/<str:check_type>/', views.check_license),
]

urlpatterns += router.urls
