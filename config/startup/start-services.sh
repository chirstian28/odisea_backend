#!/bin/bash

/usr/local/bin/gunicorn --access-logfile /var/log/gunicorn-acces.log --error-log /var/log/gunicorn-error.log --workers 3 --bind unix:/tmp/backend.sock backend.core.wsgi:application --daemon

nginx -g "daemon off;"
