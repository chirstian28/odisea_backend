FROM python:3.8-slim-buster

WORKDIR /usr/src/app

# Install os packages
RUN apt-get update -y
RUN apt-get upgrade -y
RUN apt-get install -y

RUN apt-get install -y locales
RUN sed -i -e 's/# es_CO.UTF-8 UTF-8/es_CO.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen
RUN apt-get install -y apt-utils
RUN apt-get -y install binutils libproj-dev gdal-bin
RUN apt-get install -y nginx gcc vim libcurl4-openssl-dev libgmp-dev \
    libmpfr-dev libmpc-dev libssl-dev libexpat1-dev gettext libz-dev

RUN apt-get install -y wget cron nano vim
RUN apt-get install -y libpq-dev postgresql-client python-psycopg2
# Copy files
COPY requirements.txt .
COPY . /var/www/html/backend
COPY www /var/www/html/www
COPY config/startup/start-services.sh /usr/src/app
COPY config/startup/prepare-app.sh /usr/src/app

# Install python packages
RUN pip install --no-cache-dir -r requirements.txt

ENV PYTHONPATH "${PYTHONPATH}:/var/www/html/backend"

# Copy webapp nginx config
COPY config/nginx/backend.config.80 /etc/nginx/sites-available/backend
RUN ln -s /etc/nginx/sites-available/backend /etc/nginx/sites-enabled/
RUN rm /etc/nginx/sites-enabled/default

# ssl
#COPY certs/domain.crt /etc/ssl/certs
#COPY certs/domain.key /etc/ssl/private

# Prepare App
RUN /usr/src/app/prepare-app.sh

EXPOSE 80

CMD /usr/src/app/start-services.sh


